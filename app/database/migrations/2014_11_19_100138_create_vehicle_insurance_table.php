<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVehicleInsuranceTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('vehicle_insurance', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('bs_client_id');
			$table->integer('indi_client_id');
			$table->string('car_registration', 255);
			$table->string('engine_number', 255);
			$table->date('insurance_start_date');
			$table->date('insurance_end_date');
			$table->string('courtesy_vehicle', 255);
			$table->date('manufacturing_year');
			$table->integer('cubic_capacity');
			$table->integer('vehicle_value');
			$table->integer('radio_cassette');
			$table->integer('anti_theft');
			$table->integer('windscreen');
			$table->string('chasis_number', 255);
			$table->string('premium', 255);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('vehicle_insurance');
	}

}
