<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateClientsIndividualTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('clients_individual', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('client_id');
			$table->string('first_name', 255);
			$table->string('second_name', 255);
			$table->string('email', 255);
			$table->integer('phone_number');
			$table->string('postal_address', 255);
			$table->string('location', 255);
			$table->string('status', 45);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('clients_individual');
	}

}
