<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateClientsBusinessTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('clients_business', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('client_id');
			$table->string('business_name', 255);
			$table->string('contact_person', 255);
			$table->string('email', 255);
			$table->integer('business_phone');
			$table->string('location', 255);
			$table->string('postal_address', 255);
			$table->string('status', 45);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('clients_business');
	}

}
