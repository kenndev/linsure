<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMedicalInsuranceTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('medical_insurance', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('bs_client_id');
			$table->integer('indi_client_id');
			$table->date('date_of_birth');
			$table->string('company_covering', 255);
			$table->string('impatient', 255);
			$table->string('outpatient', 255);
			$table->string('dental', 255);
			$table->string('optica', 255);
			$table->string('last_expense', 255);
			$table->string('personal_account', 255);
			$table->string('maternity', 255);
			$table->date('date_from');
			$table->date('date_to');
			$table->string('insurance_number', 255);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('medical_insurance');
	}

}
