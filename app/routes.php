<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the Closure to execute when that URI is requested.
  |
 */

Route::get('/', function() {
    return Redirect::to('users/logout');
});
//
Route::get('login', function() {
    return Redirect::to('users/logout');
});
// Confide routes
Route::get('users/create', 'UsersController@create');
Route::post('users', 'UsersController@store');
Route::get('users/login', 'UsersController@login');
Route::post('users/login', 'UsersController@doLogin');
Route::get('users/confirm/{code}', 'UsersController@confirm');
Route::get('users/forgot_password', 'UsersController@forgotPassword');
Route::post('users/forgot_password', 'UsersController@doForgotPassword');
Route::get('users/reset_password/{token}', 'UsersController@resetPassword');
Route::post('users/reset_password', 'UsersController@doResetPassword');
Route::get('users/logout', 'UsersController@logout');


Route::get('admin/dashboard', 'AdminController@dashboard');
Route::post('admin/todo', 'AdminController@To_Do');
Route::get('admin/deletetodo/{id}', 'AdminController@Delete_to_to');

//settings routes
Route::get('admin/settings', 'AdminController@settings');
Route::post('admin/settingsadd', 'AdminController@addsettings');
Route::post('admin/updatesettings', 'AdminController@updatesettings');

Route::get('admin/clients', 'AdminController@clients_add');
Route::post('admin/useradd', 'AdminController@addclientindi');
Route::post('admin/useraddbs', 'AdminController@addclientbs');

//view clients
Route::get('admin/viewClients', 'AdminController@view_clients');
Route::get('admin/viewClientsDroped', 'AdminController@view_clients_droped');
Route::put('admin/dropclient/{id}/{typeId}', array('as' => 'drop_client', 'uses' => 'AdminController@drop_Client'));
Route::put('admin/publishClient/{id}/{typeId}', array('as' => 'publishClient', 'uses' => 'AdminController@publishClient'));
Route::put('admin/deleteClient/{id}/{typeId}', array('as' => 'drop_client', 'uses' => 'AdminController@deleteClient'));

//Create Insurance policys
Route::get('admin/newInsurance', 'AdminController@new_insurance');

//add insurance to database for vehicle insurance
Route::post('admin/insureadd', 'AdminController@vehicle_insure_add');

//create policy for client
Route::put('admin/policy_Add/{id}/{typeId}', array('as' => 'policy_Add', 'uses' => 'AdminController@policyadd'));

//create vehicle insurance array('as'=>'update_document_details','uses'=>'AuthorsController@update_document_details')
Route::get('admin/vehicleInsurance/{id}/{typeId}', array('as' => 'admin/vehicleInsurance', 'uses' => 'AdminController@vehicle_insurance'));

//create medicle insurance
Route::get('admin/medicleInsurance/{id}/{typeId}', 'AdminController@med_insurance');

//create fire and bulgary
Route::get('admin/fireInsurance/{id}/{typeId}', 'AdminController@fire_insurance');

//view published vehicle insurance policys
Route::get('admin/viewVehicleInsurance', 'AdminController@view_vehicle_insurance');

//view medical insurance policys
Route::get('admin/viewMedicleInsurance', 'AdminController@view_medical_insurance');

//view dropped medical insurance policys
Route::get('admin/viewDroppedMedicalInsurance', 'AdminController@view_dropped_medical_insurance');

//Unpublish/ drop medical insurance
Route::put('admin/dropMedicalInsurance/{id}', 'AdminController@drop_medical_insurance');

//Publish medical insurance
Route::put('admin/publishMedicalInsurance/{id}', 'AdminController@publish_medical_insurance');

//View Medical details
Route::get('admin/medicalinsuarancedetails/{id}', 'AdminController@medical_insurance_details');

//View Medical details pdf
Route::get('admin/medicalinsuarancedetailspdf/{id}', 'AdminController@medical_insurance_details_pdf');


//view unpublished vehicle insurance policys
Route::get('admin/viewVehicleInsuranceDroped', 'AdminController@view_vehicle_insurance_droped');

//Unpublish vehicle insurance policys
Route::put('admin/dropInsurance/{id}/{typeId}', array('as' => 'dropInsurance', 'uses' => 'AdminController@drop_Insurance'));

//publish vehicle insurance policys
Route::put('admin/publishedInsurance/{id}/{typeId}', array('as' => 'publishedInsurance', 'uses' => 'AdminController@published_insurance'));

//View risk note html
Route::get('admin/riskNoteView/{id}/{typeId}', 'AdminController@risknote_View');

//risk note pdf
Route::get('admin/riskNote/{id}/{typeId}', 'AdminController@risknote');

//Select a claim
Route::put('admin/claimchoose/{id}/{typeId}', array('as' => 'claimChoose', 'uses' => 'AdminController@claim_choose'));

//vehicle insurance claim view form
Route::get('admin/claimVehicle/{id}/{typeId}', 'AdminController@claim_Vehicle');

//post vehicle partial damage
Route::post('admin/partialdamage', 'AdminController@partial_Damage');

//theft claim form
Route::get('admin/theftClaim/{id}/{typeId}', 'AdminController@theft_Claim');

//post theft claim to database
Route::post('admin/theftClaimPost', 'AdminController@theft_claim_post');

//windscreen claim form
Route::get('admin/windscreenClaim/{id}/{typeId}', 'AdminController@windscreen_Claim');

//post windscreen claim to database
Route::post('admin/windscreenClaimPost', 'AdminController@windscreen_claim_post');

//radiocassett claim form
Route::get('admin/radiocasetteClaim/{id}/{typeId}', 'AdminController@radiocassette_Claim');

//post radiocassett claim to database
Route::post('admin/radiocassetteClaimPost', 'AdminController@radiocassette_claim_post');

//select claim view
Route::get('admin/selectclaimview', 'AdminController@select_claim_view');

//view partial damage claims
Route::get('admin/partialDamageView', 'AdminController@view_Partialdamage');

//view partial damage details report
Route::get('admin/partialDamageDetails/{id}/{typeId}', 'AdminController@partial_damage_details');

//view theft claims
Route::get('admin/theftclaimView', 'AdminController@view_theft_claim');

//view theft details report
Route::get('admin/theftclaimViewDetails/{id}/{typeId}', 'AdminController@view_theft_claim_details');

//view windscreen claims
Route::get('admin/windscreenclaimView', 'AdminController@view_windscreen_claim');

//view windscreen details report
Route::get('admin/windscreenclaimViewDetails/{id}/{typeId}', 'AdminController@view_windscreen_claim_details');

//view radio claims
Route::get('admin/radioclaimView', 'AdminController@view_radio_claim');

//view radio details report
Route::get('admin/radioclaimViewDetails/{id}/{typeId}', 'AdminController@view_radio_claim_details');

//medicine insurance post to database
Route::post('admin/medicinepost', 'AdminController@medicine_post');

//Mail view
Route::get('admin/mailview', 'AdminController@mail_view');

//send email
Route::post('admin/sendemail', 'AdminController@send_email');


/**
 * 
 * Exporting data to excell
 */
//export clients to excell view
Route::get('admin/exportclientsexcel', 'AdminController@export_clients_excel');

//export individual clients
Route::get('admin/exportindiclients', 'AdminController@export_indi_clients');

//export business clients
Route::get('admin/exportbsclients', 'AdminController@export_bs_clients');

//export insurance to excel view
Route::get('admin/exportinsexcelview', 'AdminController@export_ins_excel_view');

//export individual insurance to excel
Route::get('admin/exportinsexcel', 'AdminController@export_ins_excel');

//export business insurance to excel
Route::get('admin/exportbsexcel', 'AdminController@export_bs_excel');

//export medicine insurance to excel
Route::get('admin/exportmedexcel', 'AdminController@export_med_excel');

//export claims view
Route::get('admin/exportclaims', 'AdminController@export_claims');

/**
 * renew insurance
 */
Route::get('admin/renewins/{id}', 'AdminController@renew_ins');
Route::get('admin/renewinspage/{id}', 'AdminController@renew_insurancepage');

/**
 * Accounts module
 */
Route::get('admin/invoiceClient', 'AdminController@invoice_client');

//invoice medicle insurance
Route::get('admin/invoiceMedClient', 'AdminController@invoice_medicle_client');

//invoice template processing
Route::put('admin/invoice/{id}/{typeId}/{insID}', 'AdminController@invoice_template');

//view invoice template
Route::put('admin/viewinvoice/{id}/{typeId}/{insID}', 'AdminController@view_invoice_template');

//update invoice details
Route::post('admin/updateinvoice', 'AdminController@update_invoice');

//edit invoice
Route::put('admin/invoiceEdit/{id}', 'AdminController@invoice_edit');


/**
 * Credit note module
 */
//credit note cancel date choose view 
Route::get('admin/creditNoteDateissue/{id}/{typeId}', 'AdminController@creditnotedateissue_View');

//credit note doesnt exist view view 
Route::get('admin/creditNoteDoesntExist', 'AdminController@creditnote_doesntexist');

//credit note view 
Route::get('admin/creditNoteView/{id}/{typeId}', 'AdminController@creditnote_View');

//credit note pdf
Route::get('admin/creditnotepdf/{id}/{typeId}', 'AdminController@creditnote_pdf');

//credit note post to database
Route::post('admin/creditadd', 'AdminController@credit_add_post');

//credit note cancel date doesnt exist medicine ins
Route::get('admin/creditNoteDateissueMed/{id}', 'AdminController@creditnotedateissue_med_View');

//credit note medicine isurance view 
Route::get('admin/creditNoteViewMed/{id}', 'AdminController@creditnote_med_View');

//credit note medicine isurance pdf
Route::get('admin/creditnotepdfMed/{id}', 'AdminController@creditnote_med_pdf');

//credit note medicine isurance post to database
Route::post('admin/creditaddMed', 'AdminController@credit_add_med_post');
