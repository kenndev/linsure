<?php

class AdminController extends BaseController {

    protected $mainlayout = null;
   

    public function __construct() {
        $this->beforeFilter('csrf', array('on' => 'post')); //protecting our post form from being accessed/used from somewhere else
        //$this->beforeFilter('auth',array('only'=>array('dashboard','service'))); //we se the auth filter, which checks if the current user is logged in
        $this->beforeFilter('auth');
    }

    private function _setupLayout() {
        if (!is_null($this->mainlayout)) {
            $this->layout = View::make($this->mainlayout);
        }
    }

    public function dashboard() {
        $todo = DB::table('to_do')
                ->paginate(5);

        $clients_indi = DB::table('clients_individual')
                ->count($columns = '*');
        $clients_bs = DB::table('clients_business')
                ->count($columns = '*');
        $vehicle_insurance = DB::table('vehicle_insurance_indi')
                ->count($columns = '*');
        $vehicle_insurance_bs = DB::table('vehicle_insurance_bs')
                ->count($columns = '*');
        $total_motor_ins = $vehicle_insurance + $vehicle_insurance_bs;
        $medical_insurance_indi = DB::table('medical_insurance_indi')
                ->count($columns = '*');
        $medical_insurance_bs = DB::table('medical_insurance_bs')
                ->count($columns = '*');
        $total_med_ins = $medical_insurance_indi + $medical_insurance_bs;

//        Excel::batch('app/storage/uploads', function($rows, $file) {
//
//            // Explain the reader how it should interpret each row,
//            // for every file inside the batch
//            $rows->each(function($row) {
//
//                // Example: dump the firstname
//                var_dump($row->location_of_accident);
//            });
//        });
//        Excel::load('Filename (4).xls', function($reader) {
//
//            // Getting all results
//            $results = $reader->get();
//
//            // ->all() is a wrapper for ->get() and will work the same
//            $results = $reader->all();
//
//            for ($i = 0, $c = count($results); $i < $c; ++$i) {
//                $results[$i] = (array) $results[$i];
//            }
//
//            echo '<pre>';
//            var_dump($results);
//            echo '</pre>';
//            exit();
//        });
//        $this->getpastdates();
        $this->mainlayout = 'layouts.test';
        $this->_setupLayout();
        $this->layout->title = "Dashboard";
        $this->layout->content = View::make('admin.index')->with(array('clientsindi' => $clients_indi, 'clientsbs' => $clients_bs, 'totalvehicleins' => $total_motor_ins, 'totalmedicalins' => $total_med_ins, 'todo' => $todo));
    }

    /**
     * To do action
     */
    //post to-do
    public function To_Do() {
        $rules = array(
            'to_do' => 'required'
        );

        $validator = Validator::make(Input::all(), $rules);
        if ($validator->passes()) {
            $todoadd = new ToDO();
            $todoadd->title = Input::get('to_do');
            $todoadd->save();
            return Redirect::to('admin/dashboard')->with('message', 'To do action added successfully');
        } else {
            return Redirect::to('admin/dashboard')
                            ->withErrors($validator)
                            ->withInput(Input::all())
                            ->with('errorMessage', 'To do action was NOT added successfully');
        }
    }
    public function Delete_to_to($id){
        $deleted_todo = DB::table('to_do')
                
                ->delete($id);
                
        if ($deleted_todo) {

            return Redirect::action('AdminController@dashboard')->with('message', 'To do action successfully deleted');
        } else {
            return Redirect::action('AdminController@dashboard')->with('errorMessage', 'To do action not successfully deleted');
        }
    }

    /*     * *
     * settings fumctionality
     * 
     */

    public function settings() {
        $settings_view = DB::table('settings')
                ->get();


        $this->mainlayout = 'layouts.test';
        $this->_setupLayout();
        $this->layout->title = "Linsure Insurance";
        $this->layout->content = View::make('admin.settingView')->with(array('settingsRow' => $settings_view));
    }

    public function addsettings() {
        $rules = array(
            'phcf_lvy' => 'required',
            'training_levy' => 'required',
            'rate' => 'required',
            'stamp_duty' => 'required',
            'political_violence' => 'required',
            'riot' => 'required'
        );

        $validator = Validator::make(Input::all(), $rules);
        if ($validator->passes()) {
            $add_settings = new Mysettings();
            $add_settings->phcf_levy = Input::get('phcf_lvy');
            $add_settings->training_levy = Input::get('training_levy');
            $add_settings->rate = Input::get('rate');
            $add_settings->stamp_duty = Input::get('stamp_duty');
            $add_settings->political_violence = Input::get('political_violence');
            $add_settings->riot = Input::get('riot');
            $add_settings->save();
            return Redirect::to('admin/settings')->with('message', 'Settings successfully added');
        } else {
            return Redirect::to('admin/settings')
                            ->withErrors($validator)
                            ->withInput(Input::all())
                            ->with('errorMessage', 'Settings not successfully added');
        }
    }

    public function updatesettings() {
        $phcf_lvy = Input::get('phcf_lvy');
        $training_levy = Input::get('training_levy');
        $rate = Input::get('rate');
        $stamp_duty = Input::get('stamp_duty');
        $political_violence = Input::get('political_violence');
        $riot = Input::get('riot');
        $id_update = Input::get('id_update');


        $updating = DB::table('settings')
                ->where('id', $id_update)
                ->update(array('phcf_levy' => $phcf_lvy, 'training_levy' => $training_levy, 'rate' => $rate, 'stamp_duty' => $stamp_duty, 'political_violence' => $political_violence, 'riot' => $riot));
        if ($updating) {

            return Redirect::action('AdminController@settings')->with('message', 'Successfully Updated');
        } else {
            return Redirect::action('AdminController@settings')->with('errorMessage', 'Not Successfully Updated');
        }
    }

    /**
     * Clients functionality
     */
    public function clients_add() {
        $this->mainlayout = 'layouts.test';
        $this->_setupLayout();
        $this->layout->title = "Linsure Insurance";
        $this->layout->content = View::make('admin.clients');
    }

    public function addclientindi() {

        $rules = array(
            'first_name' => 'required',
            'second_name' => 'required',
            'email' => 'required|email|unique:clients_individual',
            'phone_number' => 'required|numeric|unique:clients_individual',
            'postal_address' => 'required',
            'location' => 'required'
        );
        $random = uniqid();
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->passes()) {
            $add_user = new Myindiclient();
            $add_user->first_name = Input::get('first_name');
            $add_user->second_name = Input::get('second_name');
            $add_user->email = Input::get('email');
            $add_user->phone_number = Input::get('phone_number');
            $add_user->postal_address = Input::get('postal_address');
            $add_user->location = Input::get('location');
            $add_user->status = 1;
            $add_user->client_id = $random;
            $add_user->save();
            return Redirect::to('admin/clients')->with('message', 'Individual client was Added Successfully');
        } else {
            return Redirect::to('admin/clients')
                            ->withErrors($validator)
                            ->withInput(Input::all())
                            ->with('errorMessage', 'Individual client was Not added Successfully');
        }
    }

    public function addclientbs() {
        $rules = array(
            'business_name' => 'required',
            'contact_person' => 'required',
            'business_email' => 'required|email|unique:clients_business',
            'business_phone' => 'required|numeric|unique:clients_business',
            'business_postal_address' => 'required',
            'business_location' => 'required'
        );
        $random = uniqid();
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->passes()) {
            $add_user = new Mybsclient();
            $add_user->business_name = Input::get('business_name');
            $add_user->contact_person = Input::get('contact_person');
            $add_user->business_email = Input::get('business_email');
            $add_user->business_phone = Input::get('business_phone');
            $add_user->postal_address = Input::get('business_postal_address');
            $add_user->location = Input::get('business_location');
            $add_user->status = 1;
            $add_user->client_id = $random;
            $add_user->save();
            return Redirect::to('admin/clients')->with('message', 'Business client was Added Successfully');
        } else {
            return Redirect::to('admin/clients')
                            ->withErrors($validator)
                            ->withInput(Input::all())
                            ->with('errorMessage', 'Business client was Not added Successfully, Check the business client tab for more informed errors');
        }
    }

    public function view_clients() {
        $clients_individual = DB::table('clients_individual')
                ->where('status', 1)
                ->get();

        $clients_bs = DB::table('clients_business')
                ->where('status', 1)
                ->get();

        $this->mainlayout = 'layouts.test';
        $this->_setupLayout();
        $this->layout->title = "Linsure Insurance";
        $this->layout->content = View::make('admin.view')->with(array('client_individuals' => $clients_individual, 'client_business' => $clients_bs));
    }

    public function view_clients_droped() {
        $clients_individual = DB::table('clients_individual')
                ->where('status', 0)
                ->get();

        $clients_bs = DB::table('clients_business')
                ->where('status', 0)
                ->get();

        $this->mainlayout = 'layouts.test';
        $this->_setupLayout();
        $this->layout->title = "Linsure Insurance";
        $this->layout->content = View::make('admin.view_droped')->with(array('client_individuals' => $clients_individual, 'client_business' => $clients_bs));
    }

    public function drop_Client($id, $typeID) {
        if ($typeID == 5) {
            $updating = DB::table('clients_individual')
                    ->where('id', $id)
                    ->update(array('status' => 0));
            if ($updating) {

                return Redirect::action('AdminController@view_clients')->with('message', 'Individual client was successfully droped');
            } else {
                return Redirect::action('AdminController@view_clients')->with('errorMessage', 'Individual client was not successfully droped');
            }
        } elseif ($typeID == 10) {
            $updating = DB::table('clients_business')
                    ->where('id', $id)
                    ->update(array('status' => 0));
            if ($updating) {

                return Redirect::action('AdminController@view_clients')->with('message', 'Business client was successfully droped');
            } else {
                return Redirect::action('AdminController@view_clients')->with('errorMessage', 'Business client not was successfully droped');
            }
        }
    }

    public function publishClient($id, $typeID) {
        if ($typeID == 5) {
            $updating = DB::table('clients_individual')
                    ->where('id', $id)
                    ->update(array('status' => 1));
            if ($updating) {

                return Redirect::action('AdminController@view_clients_droped')->with('message', 'Individual client was successfully published');
            } else {
                return Redirect::action('AdminController@view_clients_droped')->with('errorMessage', 'Individual client was not successfully published');
            }
        } elseif ($typeID == 10) {
            $updating = DB::table('clients_business')
                    ->where('id', $id)
                    ->update(array('status' => 1));
            if ($updating) {

                return Redirect::action('AdminController@view_clients_droped')->with('message', 'Business client was successfully published');
            } else {
                return Redirect::action('AdminController@view_clients_droped')->with('errorMessage', 'Business client was not successfully published');
            }
        }
    }

    public function deleteClient($id, $typeID) {
        if ($typeID == 5) {
            $updating = DB::table('clients_individual')
                    ->where('id', $id)
                    ->delete();

            if ($updating) {

                return Redirect::action('AdminController@view_clients_droped')->with('message', 'Individual was client successfully deleted');
            } else {
                return Redirect::action('AdminController@view_clients_droped')->with('errorMessage', 'Individual client was not successfully deleted');
            }
        } elseif ($typeID == 10) {
            $updating = DB::table('clients_business')
                    ->where('id', $id)
                    ->delete();
            if ($updating) {

                return Redirect::action('AdminController@view_clients_droped')->with('message', 'Business client was successfully deleted');
            } else {
                return Redirect::action('AdminController@view_clients_droped')->with('errorMessage', 'Business client was not successfully deleted');
            }
        }
    }

    /**
     * 
     * @param type $id
     * @param type $typeID
     * 
     * insurance module starts here
     */
    public function new_insurance() {
        $clients_individual = DB::table('clients_individual')
                ->where('status', 1)
                ->get();

        $clients_bs = DB::table('clients_business')
                ->where('status', 1)
                ->get();

        $this->mainlayout = 'layouts.test';
        $this->_setupLayout();
        $this->layout->title = "Linsure Insurance";
        $this->layout->content = View::make('admin.viewInsurance')->with(array('client_individuals' => $clients_individual, 'client_business' => $clients_bs));
    }

    public function policyadd($id, $typeID) {


        $this->mainlayout = 'layouts.test';
        $this->_setupLayout();
        $this->layout->title = "Linsure Insurance";
        $this->layout->content = View::make('admin.selectpolicy')->with(array('client_id' => $id, 'client_type' => $typeID));
    }

    public function vehicle_insurance($id, $typeID) {
        $settings_view = DB::table('settings')
                ->get();

        $this->mainlayout = 'layouts.test';
        $this->_setupLayout();
        $this->layout->title = "Linsure Insurance";
        $this->layout->content = View::make('admin.policy')->with(array('client_id' => $id, 'client_type' => $typeID, 'settings_stuff' => $settings_view));
    }

    public function med_insurance($id, $typeID) {

        $this->mainlayout = 'layouts.test';
        $this->_setupLayout();
        $this->layout->title = "Linsure Insurance";
        $this->layout->content = View::make('admin.medicine')->with(array('client_id' => $id, 'client_type' => $typeID));
    }

    public function fire_insurance($id, $typeID) {

        $this->mainlayout = 'layouts.test';
        $this->_setupLayout();
        $this->layout->title = "Linsure Insurance";
        $this->layout->content = View::make('admin.fires')->with(array('client_id' => $id, 'client_type' => $typeID));
    }

    //post vehicle insurance to database

    public function vehicle_insure_add() {
        $carvalue = Input::get('vehicle_value');
        $radio_cassette = Input::get('radio_cassette');
        $anti_theft = Input::get('anti_theft');
        $wind_screen = Input::get('windscreen');
        $levys = Input::get('levies');
        $stampDuty = Input::get('stamp_duty');
        $checkbx1 = Input::get('checkbox1');
        $checkbx2 = Input::get('checkbox2');

        $premium = (Input::get('rate') / 100) * $carvalue;
        $total_phcflevys = (Input::get('phcf_levy') / 100) * $premium;
        $total_traininglevy = (Input::get('training_levy') / 100) * $premium;

        $total_levy = $total_phcflevys + $total_traininglevy;

        $politicalTotal = ($checkbx1 / 100) * $premium;
        $riottotal = ($checkbx2 / 100) * $premium;
        $total_premium = $premium + $radio_cassette + $anti_theft + $wind_screen + $total_levy + (($stampDuty / 100) * $premium) + $politicalTotal + $riottotal;

        $rules = array(
            'car_registration' => 'required',
            'engine_number' => 'required',
            'insurance_start_date' => 'required',
            'insurance_end_date' => 'required',
            'courtesy_vehicle' => 'required',
            'manufacturing_year' => 'required',
            'cubic_capacity' => 'required',
            'vehicle_value' => 'required',
            'chasis_number' => 'required'
        );
        $random = uniqid();
        $validator = Validator::make(Input::all(), $rules);

        if (Input::get('type_of_client') == 5) {
            $add_vehicleins = new MyvehicleInsurance();
        } elseif (Input::get('type_of_client') == 10) {
            $add_vehicleins = new MyvehicleInsuranceBs();
        }

        if ($validator->passes()) {

            $add_vehicleins->car_registration = Input::get('car_registration');
            $add_vehicleins->engine_number = Input::get('engine_number');
            $add_vehicleins->insurance_start_date = Input::get('insurance_start_date');
            $add_vehicleins->insurance_end_date = Input::get('insurance_end_date');
            $add_vehicleins->courtesy_vehicle = Input::get('courtesy_vehicle');
            $add_vehicleins->manufacturing_year = Input::get('manufacturing_year');
            $add_vehicleins->cubic_capacity = Input::get('cubic_capacity');
            $add_vehicleins->vehicle_value = Input::get('vehicle_value');
            $add_vehicleins->radio_cassette = Input::get('radio_cassette');
            $add_vehicleins->anti_theft = Input::get('anti_theft');
            $add_vehicleins->windscreen = Input::get('windscreen');
            $add_vehicleins->chasis_number = Input::get('chasis_number');
            $add_vehicleins->status = 1;
            $add_vehicleins->insurance_id = $random;
            if (Input::get('type_of_client') == 5) {
                $add_vehicleins->indi_client_id = Input::get('id');
            } elseif (Input::get('type_of_client') == 10) {
                $add_vehicleins->bs_client_id = Input::get('id');
            }
            $add_vehicleins->phcflevy = $total_phcflevys;
            $add_vehicleins->traininglevy = $total_traininglevy;
            $add_vehicleins->premium = $premium;
            $add_vehicleins->total_premium = $total_premium;
            $add_vehicleins->save();
            if (Input::get('type_of_client') == 5) {
                $update = DB::table('clients_individual')
                        ->where('id', Input::get('id'))
                        ->update(array('vehicle_status' => 1));
            } elseif (Input::get('type_of_client') == 10) {
                $update = DB::table('clients_business')
                        ->where('id', Input::get('id'))
                        ->update(array('vehicle_status' => 1));
            }
            return Redirect::to('admin/vehicleInsurance/' . Input::get('id') . '/' . Input::get('type_of_client'))->with('message', 'Isuarance cover was made Successfully');
        } else {
            return Redirect::to('admin/vehicleInsurance/' . Input::get('id') . '/' . Input::get('type_of_client'))
                            ->withErrors($validator)
                            ->withInput(Input::all())
                            ->with(array('errorMessage' => 'Insurance cover was not made Successfully'));
        }
    }

    public function medicine_post() {

        $rules = array(
            'date_of_birth' => 'required',
            'company_covering' => 'required',
            'date_from' => 'required',
            'date_to' => 'required'
        );
        $random = uniqid();
        $validator = Validator::make(Input::all(), $rules);

        if (Input::get('client_type') == 5) {
            $add_medsins = new MymedicalInsurance();
        } elseif (Input::get('client_type') == 10) {
            $add_medsins = new MymedicalInsurancebs();
        }
        if (strcmp(Input::get('optionsRadios'), 'impatient') == 0) {
            $impatient = 1;
            $outpatient = 0;
        } elseif (strcmp(Input::get('optionsRadios'), 'outpatient') == 0) {
            $impatient = 0;
            $outpatient = 1;
        }

        if (strcmp(Input::get('dental'), 'dental') == 0) {
            $dental = 1;
        } else {
            $dental = 0;
        }
        if (strcmp(Input::get('optica'), 'optica') == 0) {
            $optica = 1;
        } else {
            $optica = 0;
        }
        if (strcmp(Input::get('personal_account'), 'personal_account') == 0) {
            $personal_account = 1;
        } else {
            $personal_account = 0;
        }
        if (strcmp(Input::get('maternity'), 'maternity') == 0) {
            $maternity = 1;
        } else {
            $maternity = 0;
        }

        if ($validator->passes()) {

            $add_medsins->date_of_birth = Input::get('date_of_birth');
            $add_medsins->company_covering = Input::get('company_covering');
            $add_medsins->impatient = $impatient;
            $add_medsins->outpatient = $outpatient;
            $add_medsins->dental = $dental;
            $add_medsins->optica = $optica;
            $add_medsins->last_expense = Input::get('last_expense');
            $add_medsins->personal_account = $personal_account;
            $add_medsins->maternity = $maternity;
            $add_medsins->date_from = Input::get('date_from');
            $add_medsins->date_to = Input::get('date_to');
            $add_medsins->insurance_number = $random;
            if (Input::get('client_type') == 5) {
                $add_medsins->indi_client_id = Input::get('id');
            } elseif (Input::get('client_type') == 10) {
                $add_medsins->bs_client_id = Input::get('id');
            }

            $add_medsins->save();
            if (Input::get('client_type') == 5) {
                $update = DB::table('clients_individual')
                        ->where('id', Input::get('id'))
                        ->update(array('med_status' => 1));
            } elseif (Input::get('client_type') == 10) {
                $update = DB::table('clients_business')
                        ->where('id', Input::get('id'))
                        ->update(array('med_status' => 1));
            }
            return Redirect::to('admin/medicleInsurance/' . Input::get('id') . '/' . Input::get('client_type'))->with('message', 'Isuarance cover was made Successfully');
        } else {
            return Redirect::to('admin/medicleInsurance/' . Input::get('id') . '/' . Input::get('client_type'))
                            ->withErrors($validator)
                            ->withInput(Input::all())
                            ->with(array('errorMessage' => 'Insurance cover was not made Successfully'));
        }
    }

    public function view_vehicle_insurance() {
        $insurance_individual = DB::table('vehicle_insurance_indi')
                ->join('clients_individual', 'vehicle_insurance_indi.indi_client_id', '=', 'clients_individual.id')
                ->where('vehicle_insurance_indi.status', 1)
                ->get($columns = array('vehicle_insurance_indi.id',
            'vehicle_insurance_indi.car_registration',
            'vehicle_insurance_indi.insurance_id',
            'clients_individual.first_name',
            'clients_individual.second_name',
            'clients_individual.email',
            'clients_individual.phone_number'));

        $insurance_bs = DB::table('vehicle_insurance_bs')
                ->join('clients_business', 'vehicle_insurance_bs.bs_client_id', '=', 'clients_business.id')
                ->where('vehicle_insurance_bs.status', 1)
                ->get($columns = array('vehicle_insurance_bs.id',
            'vehicle_insurance_bs.car_registration',
            'vehicle_insurance_bs.insurance_id',
            'clients_business.business_name',
            'clients_business.contact_person',
            'clients_business.business_email',
            'clients_business.business_phone'));
        $this->mainlayout = 'layouts.test';
        $this->_setupLayout();
        $this->layout->title = "Linsure Insurance";
        $this->layout->content = View::make('admin.view_vehicle_insurance')->with(array('insurance_individual' => $insurance_individual, 'insurance_business' => $insurance_bs));
    }

    public function view_vehicle_insurance_droped() {
        $insurance_individual = DB::table('vehicle_insurance_indi')
                ->join('clients_individual', 'vehicle_insurance_indi.indi_client_id', '=', 'clients_individual.id')
                ->where('vehicle_insurance_indi.status', 0)
                ->get($columns = array('vehicle_insurance_indi.id',
            'vehicle_insurance_indi.car_registration',
            'clients_individual.first_name',
            'clients_individual.second_name',
            'clients_individual.email',
            'clients_individual.phone_number'));

        $insurance_bs = DB::table('vehicle_insurance_bs')
                ->join('clients_business', 'vehicle_insurance_bs.bs_client_id', '=', 'clients_business.id')
                ->where('vehicle_insurance_bs.status', 0)
                ->get($columns = array('vehicle_insurance_bs.id',
            'vehicle_insurance_bs.car_registration',
            'clients_business.business_name',
            'clients_business.contact_person',
            'clients_business.business_email',
            'clients_business.business_phone'));
        $this->mainlayout = 'layouts.test';
        $this->_setupLayout();
        $this->layout->title = "Linsure Insurance";
        $this->layout->content = View::make('admin.viewVehicle_insurance_droped')->with(array('insurance_individual' => $insurance_individual, 'insurance_business' => $insurance_bs));
    }

    //unpublish insurance vehicle cover
    public function drop_Insurance($id, $type_client) {
        if ($type_client == 5) {
            $updating = DB::table('vehicle_insurance_indi')
                    ->where('id', $id)
                    ->update(array('status' => 0));
            if ($updating) {

                return Redirect::action('AdminController@view_vehicle_insurance')->with('message', 'Insurance cover was successfully Unpublished');
            } else {
                return Redirect::action('AdminController@view_vehicle_insurance')->with('errorMessage', 'Insurance cover was not successfully Unpublished');
            }
        } elseif ($type_client == 10) {
            $updating = DB::table('vehicle_insurance_bs')
                    ->where('id', $id)
                    ->update(array('status' => 0));
            if ($updating) {

                return Redirect::action('AdminController@view_vehicle_insurance')->with('message', 'Insurance cover was successfully Unpublished');
            } else {
                return Redirect::action('AdminController@view_vehicle_insurance')->with('errorMessage', 'Insurance cover was not successfully Unpublished');
            }
        }
    }

    //publish insurance vehicle cover
    public function published_insurance($id, $type_client) {
        if ($type_client == 5) {
            $updating = DB::table('vehicle_insurance_indi')
                    ->where('id', $id)
                    ->update(array('status' => 1));
            if ($updating) {

                return Redirect::action('AdminController@view_vehicle_insurance_droped')->with('message', 'Insurance cover was successfully published');
            } else {
                return Redirect::action('AdminController@view_vehicle_insurance_droped')->with('errorMessage', 'Insurance cover was not successfully published');
            }
        } elseif ($type_client == 10) {
            $updating = DB::table('vehicle_insurance_bs')
                    ->where('id', $id)
                    ->update(array('status' => 1));
            if ($updating) {

                return Redirect::action('AdminController@view_vehicle_insurance_droped')->with('message', 'Insurance cover was successfully published');
            } else {
                return Redirect::action('AdminController@view_vehicle_insurance_droped')->with('errorMessage', 'Insurance cover was not successfully published');
            }
        }
    }

    //view medical insurance cover
    public function view_medical_insurance() {
        $insurance_medical = DB::table('medical_insurance_indi')
                ->join('clients_individual', 'medical_insurance_indi.indi_client_id', '=', 'clients_individual.id')
                ->where('medical_insurance_indi.status', 1)
                ->get($columns = array('medical_insurance_indi.id',
            'medical_insurance_indi.date_from',
            'medical_insurance_indi.date_to',
            'medical_insurance_indi.insurance_number',
            'clients_individual.first_name',
            'clients_individual.second_name',
            'clients_individual.email',
            'clients_individual.phone_number'));
        $this->mainlayout = 'layouts.test';
        $this->_setupLayout();
        $this->layout->title = "Linsure Insurance";
        $this->layout->content = View::make('admin.viewMedical')->with(array('insurance_medical' => $insurance_medical));
    }

    //View droped medical insurance cover
    public function view_dropped_medical_insurance() {
        $insurance_medical = DB::table('medical_insurance_indi')
                ->join('clients_individual', 'medical_insurance_indi.indi_client_id', '=', 'clients_individual.id')
                ->where('medical_insurance_indi.status', 0)
                ->get($columns = array('medical_insurance_indi.id',
            'medical_insurance_indi.date_from',
            'medical_insurance_indi.date_to',
            'clients_individual.first_name',
            'clients_individual.second_name',
            'clients_individual.email',
            'clients_individual.phone_number'));
        $this->mainlayout = 'layouts.test';
        $this->_setupLayout();
        $this->layout->title = "Linsure Insurance";
        $this->layout->content = View::make('admin.viewDroppedMedical')->with(array('insurance_medical' => $insurance_medical));
    }

    //drop medical insurance 0 = dropped 1 = active
    public function drop_medical_insurance($id) {
        $updating = DB::table('medical_insurance_indi')
                ->where('id', $id)
                ->update(array('status' => 0));
        if ($updating) {

            return Redirect::action('AdminController@view_medical_insurance')->with('message', 'Insurance cover was successfully dropped');
        } else {
            return Redirect::action('AdminController@view_medical_insurance')->with('errorMessage', 'Insurance cover was not successfully dropped');
        }
    }

    //publish medical insurance
    public function publish_medical_insurance($id) {
        $updating = DB::table('medical_insurance_indi')
                ->where('id', $id)
                ->update(array('status' => 1));
        if ($updating) {

            return Redirect::action('AdminController@view_dropped_medical_insurance')->with('message', 'Insurance cover was successfully published');
        } else {
            return Redirect::action('AdminController@view_dropped_medical_insurance')->with('errorMessage', 'Insurance cover was not successfully dropped');
        }
    }

    //medical insurance details
    public function medical_insurance_details($id) {
        $this->mainlayout = 'layouts.test';
        $this->_setupLayout();
        $this->layout->title = "Linsure Insurance";
        $riskId = $id;


        $insurance = DB::table('medical_insurance_indi')
                ->join('clients_individual', 'medical_insurance_indi.indi_client_id', '=', 'clients_individual.id')
                ->where('medical_insurance_indi.id', $id)
                ->get();
        $this->layout->content = View::make('admin.medicaldetail')->with(array('risk_id' => $riskId, 'insurance' => $insurance));
    }

    //medical insurance details pdf
    public function medical_insurance_details_pdf($id) {
        $insurance['insurance'] = DB::table('medical_insurance_indi')
                ->join('clients_individual', 'medical_insurance_indi.indi_client_id', '=', 'clients_individual.id')
                ->where('medical_insurance_indi.id', $id)
                ->get();
        $pdf = PDF::loadView('admin.medicinedetailpdf', $insurance);

        return $pdf->stream('');
    }

    //vehicle insurance risk note
    public function risknote_View($id, $typeClient) {
        $this->mainlayout = 'layouts.test';
        $this->_setupLayout();
        $this->layout->title = "Linsure Insurance";
        $riskId = $id;
        $insurance;
        if ($typeClient == 5) {
            $insurance = DB::table('vehicle_insurance_indi')
                    ->join('clients_individual', 'vehicle_insurance_indi.indi_client_id', '=', 'clients_individual.id')
                    ->where('vehicle_insurance_indi.id', $id)
                    ->get();
            $this->layout->content = View::make('admin.risknote')->with(array('risk_id' => $riskId, 'type_client' => $typeClient, 'insurance' => $insurance));
        } elseif ($typeClient == 10) {
            $insurance = DB::table('vehicle_insurance_bs')
                    ->join('clients_business', 'vehicle_insurance_bs.bs_client_id', '=', 'clients_business.id')
                    ->where('vehicle_insurance_bs.id', $id)
                    ->get();
            $this->layout->content = View::make('admin.risknotebs')->with(array('risk_id' => $riskId, 'type_client' => $typeClient, 'insurance' => $insurance));
        }
    }

    public function risknote($id, $typeClient) {
        $insurance;
        if ($typeClient == 5) {
            $insurance['insurance'] = DB::table('vehicle_insurance_indi')
                    ->join('clients_individual', 'vehicle_insurance_indi.indi_client_id', '=', 'clients_individual.id')
                    ->where('vehicle_insurance_indi.id', $id)
                    ->get();
            $pdf = PDF::loadView('admin.risknotepdf', $insurance);
        } elseif ($typeClient == 10) {
            $insurance['insurance'] = DB::table('vehicle_insurance_bs')
                    ->join('clients_business', 'vehicle_insurance_bs.bs_client_id', '=', 'clients_business.id')
                    ->where('vehicle_insurance_bs.id', $id)
                    ->get();
            $pdf = PDF::loadView('admin.risknotepdfbs', $insurance);
        }

        return $pdf->stream('');
    }

    /**
     * Vehicle Insurance Claim 
     */
    public function claim_choose($id, $typeClient) {
        $this->mainlayout = 'layouts.test';
        $this->_setupLayout();
        $this->layout->title = "Linsure Insurance";
        $this->layout->content = View::make('admin.selectclaim')->with(array('client_id' => $id, 'client_type' => $typeClient));
    }

    //vehicle insurance partial damage claim form
    public function claim_Vehicle($id, $typeClient) {
        if ($typeClient == 5) {
            $client_id = DB::table('clients_individual')
                    ->join('vehicle_insurance_indi', 'clients_individual.id', '=', 'vehicle_insurance_indi.indi_client_id')
                    ->where('vehicle_insurance_indi.id', $id)
                    ->pluck('clients_individual.id');
        } elseif ($typeClient == 10) {
            $client_id = DB::table('clients_business')
                    ->join('vehicle_insurance_bs', 'clients_business.id', '=', 'vehicle_insurance_bs.bs_client_id')
                    ->where('vehicle_insurance_bs.id', $id)
                    ->pluck('clients_business.id');
        }
        $this->mainlayout = 'layouts.test';
        $this->_setupLayout();
        $this->layout->title = "Linsure Insurance";
        $this->layout->content = View::make('admin.claim_form')->with(array('client_id' => $client_id, 'type_client' => $typeClient, 'insurance_id' => $id));
    }

    //post partial damage to database
    public function partial_Damage() {
        $rules = array(
            'location_of_accident' => 'required',
            'police_station' => 'required',
            'time' => 'required',
            'driving_experience' => 'required',
            'date_reported' => 'required',
            'date_incident' => 'required'
        );
        $random = uniqid();
        $validator = Validator::make(Input::all(), $rules);

        if (Input::get('client_type') == 5) {
            $add_partialdamage = new PartialDamage();
        } elseif (Input::get('client_type') == 10) {
            $add_partialdamage = new PartialDamageBs();
        }

        if ($validator->passes()) {
            if (Input::get('client_type') == 5) {
                $add_partialdamage->indi_client_id = Input::get('id');
                $add_partialdamage->indi_policy_id = Input::get('insurance_id');
            } elseif (Input::get('client_type') == 10) {
                $add_partialdamage->bs_client_id = Input::get('id');
                $add_partialdamage->bs_policy_id = Input::get('insurance_id');
            }
            $add_partialdamage->location_of_accident = Input::get('location_of_accident');
            $add_partialdamage->police_station = Input::get('police_station');
            $add_partialdamage->time = Input::get('time');
            $add_partialdamage->status = 1;
            $add_partialdamage->claim_no = $random;
            $add_partialdamage->driving_experience = Input::get('driving_experience');
            $add_partialdamage->date_reported = Input::get('date_reported');
            $add_partialdamage->date_incident = Input::get('date_incident');

            $add_partialdamage->save();

            return Redirect::to('admin/claimVehicle/' . Input::get('insurance_id') . '/' . Input::get('client_type'))->with('message', 'Claim was made Successfully');
        } else {
            return Redirect::to('admin/claimVehicle/' . Input::get('insurance_id') . '/' . Input::get('client_type'))
                            ->withErrors($validator)
                            ->withInput(Input::all())
                            ->with(array('errorMessage' => 'Claim was not made Successfully'));
        }
    }

    //theft claim form page
    public function theft_Claim($id, $typeClient) {
        if ($typeClient == 5) {
            $client_id = DB::table('clients_individual')
                    ->join('vehicle_insurance_indi', 'clients_individual.id', '=', 'vehicle_insurance_indi.indi_client_id')
                    ->where('vehicle_insurance_indi.id', $id)
                    ->pluck('clients_individual.id');
        } elseif ($typeClient == 10) {
            $client_id = DB::table('clients_business')
                    ->join('vehicle_insurance_bs', 'clients_business.id', '=', 'vehicle_insurance_bs.bs_client_id')
                    ->where('vehicle_insurance_bs.id', $id)
                    ->pluck('clients_business.id');
        }
        $this->mainlayout = 'layouts.test';
        $this->_setupLayout();
        $this->layout->title = "Linsure Insurance";
        $this->layout->content = View::make('admin.theftclaim_form')->with(array('client_id' => $client_id, 'type_client' => $typeClient, 'insurance_id' => $id));
    }

    //post theft claim to database
    public function theft_claim_post() {
        $rules = array(
            'location_of_accident' => 'required',
            'police_station' => 'required',
            'time' => 'required',
            'date_reported' => 'required',
            'date_incident' => 'required'
        );
        $random = uniqid();
        $validator = Validator::make(Input::all(), $rules);

        if (Input::get('client_type') == 5) {
            $add_theftclaim = new theftClaim();
        } elseif (Input::get('client_type') == 10) {
            $add_theftclaim = new theftClaimBs();
        }

        if ($validator->passes()) {
            if (Input::get('client_type') == 5) {
                $add_theftclaim->indi_client_id = Input::get('id');
                $add_theftclaim->indi_policy_id = Input::get('insurance_id');
            } elseif (Input::get('client_type') == 10) {
                $add_theftclaim->bs_client_id = Input::get('id');
                $add_theftclaim->bs_policy_id = Input::get('insurance_id');
            }
            $add_theftclaim->location = Input::get('location_of_accident');
            $add_theftclaim->police_station = Input::get('police_station');
            $add_theftclaim->time = Input::get('time');

            $add_theftclaim->claim_no = $random;

            $add_theftclaim->date_reported = Input::get('date_reported');
            $add_theftclaim->date_incident = Input::get('date_incident');

            $add_theftclaim->save();

            return Redirect::to('admin/theftClaim/' . Input::get('insurance_id') . '/' . Input::get('client_type'))->with('message', 'Claim was made Successfully');
        } else {
            return Redirect::to('admin/theftClaim/' . Input::get('insurance_id') . '/' . Input::get('client_type'))
                            ->withErrors($validator)
                            ->withInput(Input::all())
                            ->with(array('errorMessage' => 'Claim was not made Successfully'));
        }
    }

    //windscreen claim form page
    public function windscreen_Claim($id, $typeClient) {
        if ($typeClient == 5) {
            $client_id = DB::table('clients_individual')
                    ->join('vehicle_insurance_indi', 'clients_individual.id', '=', 'vehicle_insurance_indi.indi_client_id')
                    ->where('vehicle_insurance_indi.id', $id)
                    ->pluck('clients_individual.id');
        } elseif ($typeClient == 10) {
            $client_id = DB::table('clients_business')
                    ->join('vehicle_insurance_bs', 'clients_business.id', '=', 'vehicle_insurance_bs.bs_client_id')
                    ->where('vehicle_insurance_bs.id', $id)
                    ->pluck('clients_business.id');
        }
        $this->mainlayout = 'layouts.test';
        $this->_setupLayout();
        $this->layout->title = "Linsure Insurance";
        $this->layout->content = View::make('admin.windscreenclaim_form')->with(array('client_id' => $client_id, 'type_client' => $typeClient, 'insurance_id' => $id));
    }

    //post windscreen claim to database
    public function windscreen_claim_post() {
        $rules = array(
            'location_of_accident' => 'required',
            'police_station' => 'required',
            'time' => 'required',
            'date_reported' => 'required',
            'date_incident' => 'required'
        );
        $random = uniqid();
        $validator = Validator::make(Input::all(), $rules);

        if (Input::get('client_type') == 5) {
            $add_windscreenclaim = new windscreenClaim();
        } elseif (Input::get('client_type') == 10) {
            $add_windscreenclaim = new windscreenClaimBs();
        }

        if ($validator->passes()) {
            if (Input::get('client_type') == 5) {
                $add_windscreenclaim->indi_client_id = Input::get('id');
                $add_windscreenclaim->indi_policy_id = Input::get('insurance_id');
            } elseif (Input::get('client_type') == 10) {
                $add_windscreenclaim->bs_client_id = Input::get('id');
                $add_windscreenclaim->bs_policy_id = Input::get('insurance_id');
            }
            $add_windscreenclaim->location = Input::get('location_of_accident');
            $add_windscreenclaim->police_station = Input::get('police_station');
            $add_windscreenclaim->time = Input::get('time');

            $add_windscreenclaim->claim_no = $random;

            $add_windscreenclaim->date_reported = Input::get('date_reported');
            $add_windscreenclaim->date_incident = Input::get('date_incident');

            $add_windscreenclaim->save();

            return Redirect::to('admin/windscreenClaim/' . Input::get('insurance_id') . '/' . Input::get('client_type'))->with('message', 'Claim was made Successfully');
        } else {
            return Redirect::to('admin/windscreenClaim/' . Input::get('insurance_id') . '/' . Input::get('client_type'))
                            ->withErrors($validator)
                            ->withInput(Input::all())
                            ->with(array('errorMessage' => 'Claim was not made Successfully'));
        }
    }

    //radio cassette claim form page
    public function radiocassette_Claim($id, $typeClient) {
        if ($typeClient == 5) {
            $client_id = DB::table('clients_individual')
                    ->join('vehicle_insurance_indi', 'clients_individual.id', '=', 'vehicle_insurance_indi.indi_client_id')
                    ->where('vehicle_insurance_indi.id', $id)
                    ->pluck('clients_individual.id');
        } elseif ($typeClient == 10) {
            $client_id = DB::table('clients_business')
                    ->join('vehicle_insurance_bs', 'clients_business.id', '=', 'vehicle_insurance_bs.bs_client_id')
                    ->where('vehicle_insurance_bs.id', $id)
                    ->pluck('clients_business.id');
        }
        $this->mainlayout = 'layouts.test';
        $this->_setupLayout();
        $this->layout->title = "Linsure Insurance";
        $this->layout->content = View::make('admin.radiocassetteclaim_form')->with(array('client_id' => $client_id, 'type_client' => $typeClient, 'insurance_id' => $id));
    }

    //post radio cassette claim to database
    public function radiocassette_claim_post() {
        $rules = array(
            'location_of_accident' => 'required',
            'police_station' => 'required',
            'time' => 'required',
            'date_reported' => 'required',
            'date_incident' => 'required'
        );
        $random = uniqid();
        $validator = Validator::make(Input::all(), $rules);

        if (Input::get('client_type') == 5) {
            $add_radioclaim = new radiocassetteClaim();
        } elseif (Input::get('client_type') == 10) {
            $add_radioclaim = new radiocassetteClaimBs();
        }

        if ($validator->passes()) {
            if (Input::get('client_type') == 5) {
                $add_radioclaim->indi_client_id = Input::get('id');
                $add_radioclaim->indi_policy_id = Input::get('insurance_id');
            } elseif (Input::get('client_type') == 10) {
                $add_radioclaim->bs_client_id = Input::get('id');
                $add_radioclaim->bs_policy_id = Input::get('insurance_id');
            }
            $add_radioclaim->location = Input::get('location_of_accident');
            $add_radioclaim->police_station = Input::get('police_station');
            $add_radioclaim->time = Input::get('time');

            $add_radioclaim->claim_no = $random;

            $add_radioclaim->date_reported = Input::get('date_reported');
            $add_radioclaim->date_incident = Input::get('date_incident');

            $add_radioclaim->save();

            return Redirect::to('admin/radiocasetteClaim/' . Input::get('insurance_id') . '/' . Input::get('client_type'))->with('message', 'Claim was made Successfully');
        } else {
            return Redirect::to('admin/radiocasetteClaim/' . Input::get('insurance_id') . '/' . Input::get('client_type'))
                            ->withErrors($validator)
                            ->withInput(Input::all())
                            ->with(array('errorMessage' => 'Claim was not made Successfully'));
        }
    }

    //select claim type to view page
    public function select_claim_view() {
        $this->mainlayout = 'layouts.test';
        $this->_setupLayout();
        $this->layout->title = "Linsure Insurance";
        $this->layout->content = View::make('admin.selectclaimview');
    }

    public function view_Partialdamage() {
        $partial_damage_indi = DB::table('partial_damage')
                ->join('clients_individual', 'partial_damage.indi_client_id', '=', 'clients_individual.id')
                ->where('partial_damage.status', 1)
                ->get($columns = array('partial_damage.id',
            'partial_damage.location_of_accident',
            'partial_damage.police_station',
            'partial_damage.date_reported',
            'partial_damage.date_incident',
            'clients_individual.first_name',
            'clients_individual.phone_number',
            'clients_individual.second_name'
        ));

        $partial_damage_bs = DB::table('partial_damage_bs')
                ->join('clients_business', 'partial_damage_bs.bs_client_id', '=', 'clients_business.id')
                ->where('partial_damage_bs.status', 1)
                ->get($columns = array('partial_damage_bs.id',
            'partial_damage_bs.location_of_accident',
            'partial_damage_bs.police_station',
            'partial_damage_bs.date_reported',
            'partial_damage_bs.date_incident',
            'clients_business.business_phone',
            'clients_business.business_name'
        ));

        $this->mainlayout = 'layouts.test';
        $this->_setupLayout();
        $this->layout->title = "Linsure Insurance";
        $this->layout->content = View::make('admin.viewpartialdamage')->with(array('partial_damage_indi' => $partial_damage_indi, 'partial_damage_bs' => $partial_damage_bs));
    }

    public function view_theft_claim() {
        $partial_damage_indi = DB::table('theft_indi')
                ->join('clients_individual', 'theft_indi.indi_client_id', '=', 'clients_individual.id')
                ->where('theft_indi.status', 1)
                ->get($columns = array('theft_indi.id',
            'theft_indi.location',
            'theft_indi.police_station',
            'theft_indi.date_reported',
            'theft_indi.date_incident',
            'clients_individual.first_name',
            'clients_individual.phone_number',
            'clients_individual.second_name'
        ));

        $partial_damage_bs = DB::table('theft_bs')
                ->join('clients_business', 'theft_bs.bs_client_id', '=', 'clients_business.id')
                ->where('theft_bs.status', 1)
                ->get($columns = array('theft_bs.id',
            'theft_bs.location',
            'theft_bs.police_station',
            'theft_bs.date_reported',
            'theft_bs.date_incident',
            'clients_business.business_phone',
            'clients_business.business_name'
        ));

        $this->mainlayout = 'layouts.test';
        $this->_setupLayout();
        $this->layout->title = "Linsure Insurance";
        $this->layout->content = View::make('admin.viewtheftclaim')->with(array('partial_damage_indi' => $partial_damage_indi, 'partial_damage_bs' => $partial_damage_bs));
    }

    public function view_windscreen_claim() {
        $partial_damage_indi = DB::table('windscreen_indi')
                ->join('clients_individual', 'windscreen_indi.indi_client_id', '=', 'clients_individual.id')
                ->where('windscreen_indi.status', 1)
                ->get($columns = array('windscreen_indi.id',
            'windscreen_indi.location',
            'windscreen_indi.police_station',
            'windscreen_indi.date_reported',
            'windscreen_indi.date_incident',
            'clients_individual.first_name',
            'clients_individual.phone_number',
            'clients_individual.second_name'
        ));

        $partial_damage_bs = DB::table('windscreen_bs')
                ->join('clients_business', 'windscreen_bs.bs_client_id', '=', 'clients_business.id')
                ->where('windscreen_bs.status', 1)
                ->get($columns = array('windscreen_bs.id',
            'windscreen_bs.location',
            'windscreen_bs.police_station',
            'windscreen_bs.date_reported',
            'windscreen_bs.date_incident',
            'clients_business.business_phone',
            'clients_business.business_name'
        ));

        $this->mainlayout = 'layouts.test';
        $this->_setupLayout();
        $this->layout->title = "Linsure Insurance";
        $this->layout->content = View::make('admin.viewwindscreenclaim')->with(array('partial_damage_indi' => $partial_damage_indi, 'partial_damage_bs' => $partial_damage_bs));
    }

    public function view_radio_claim() {
        $partial_damage_indi = DB::table('radiocasette_indi')
                ->join('clients_individual', 'radiocasette_indi.indi_client_id', '=', 'clients_individual.id')
                ->where('radiocasette_indi.status', 1)
                ->get($columns = array('radiocasette_indi.id',
            'radiocasette_indi.location',
            'radiocasette_indi.police_station',
            'radiocasette_indi.date_reported',
            'radiocasette_indi.date_incident',
            'clients_individual.first_name',
            'clients_individual.phone_number',
            'clients_individual.second_name'
        ));

        $partial_damage_bs = DB::table('radiocasette_bs')
                ->join('clients_business', 'radiocasette_bs.bs_client_id', '=', 'clients_business.id')
                ->where('radiocasette_bs.status', 1)
                ->get($columns = array('radiocasette_bs.id',
            'radiocasette_bs.location',
            'radiocasette_bs.police_station',
            'radiocasette_bs.date_reported',
            'radiocasette_bs.date_incident',
            'clients_business.business_phone',
            'clients_business.business_name'
        ));

        $this->mainlayout = 'layouts.test';
        $this->_setupLayout();
        $this->layout->title = "Linsure Insurance";
        $this->layout->content = View::make('admin.viewradiocassetteclaim')->with(array('partial_damage_indi' => $partial_damage_indi, 'partial_damage_bs' => $partial_damage_bs));
    }

    public function export() {
        $posts = Post::all();

        $headers = $this->getColumnNames($posts);

        $posts_array = array_merge((array) $headers, (array) $posts->toArray());
    }

    public function partial_damage_details($id, $client_type) {
        $this->mainlayout = 'layouts.test';
        $this->_setupLayout();
        $this->layout->title = "Linsure Insurance";
        $riskId = $id;

        if ($client_type == 5) {
            $partialdamage = DB::table('partial_damage')
                    ->join('clients_individual', 'partial_damage.indi_client_id', '=', 'clients_individual.id')
                    ->where('partial_damage.id', $id)
                    ->get();
            /**
             * export to excell
             * echo "<br /><br />";
             * 
             * change from object to array
             */
            for ($i = 0, $c = count($partialdamage); $i < $c; ++$i) {
                $partialdamage[$i] = (array) $partialdamage[$i];
            }
            Excel::create('Filename', function($excel) use($partialdamage) {

                $excel->sheet('Sheetname', function($sheet) use($partialdamage) {

                    $sheet->fromArray($partialdamage);
                });
            })->export('xls');
            /**
             * end of excel export code
             */
            $this->layout->content = View::make('admin.partialdamageDetails')->with(array('risk_id' => $riskId, 'type_client' => $client_type, 'partialdamage' => $partialdamage));
        } elseif ($client_type == 10) {
            $partialdamage = DB::table('partial_damage_bs')
                    ->join('clients_business', 'partial_damage_bs.bs_client_id', '=', 'clients_business.id')
                    ->where('partial_damage_bs.id', $id)
                    ->get();
            /**
             * export to excell
             * echo "<br /><br />";
             * 
             * change from object to array
             */
            for ($i = 0, $c = count($partialdamage); $i < $c; ++$i) {
                $partialdamage[$i] = (array) $partialdamage[$i];
            }
            Excel::create('Filename', function($excel) use($partialdamage) {

                $excel->sheet('Sheetname', function($sheet) use($partialdamage) {

                    $sheet->fromArray($partialdamage);
                });
            })->export('xls');
            /**
             * end of excel export code
             */
            $this->layout->content = View::make('admin.partialdamageDetailsbs')->with(array('risk_id' => $riskId, 'type_client' => $client_type, 'partialdamage' => $partialdamage));
        }
    }

    public function getColumnNames($object) {

        $rip_headers = $object->toArray();

        $keys = array_keys($rip_headers[0]);

        foreach ($keys as $value) {
            $headers[$value] = $value;
        }

        return array($headers);
    }

    public function view_theft_claim_details($id, $client_type) {
        $this->mainlayout = 'layouts.test';
        $this->_setupLayout();
        $this->layout->title = "Linsure Insurance";
        $riskId = $id;
        $claimtitle = 'Theft Claim';

        if ($client_type == 5) {
            $partialdamage = DB::table('theft_indi')
                    ->join('clients_individual', 'theft_indi.indi_client_id', '=', 'clients_individual.id')
                    ->where('theft_indi.id', $id)
                    ->get();
            $this->layout->content = View::make('admin.claimdetailsview')->with(array('risk_id' => $riskId, 'type_client' => $client_type, 'partialdamage' => $partialdamage, 'claim' => $claimtitle));
        } elseif ($client_type == 10) {
            $partialdamage = DB::table('theft_bs')
                    ->join('clients_business', 'theft_bs.bs_client_id', '=', 'clients_business.id')
                    ->where('theft_bs.id', $id)
                    ->get();
            $this->layout->content = View::make('admin.claimdetailsviewbs')->with(array('risk_id' => $riskId, 'type_client' => $client_type, 'partialdamage' => $partialdamage, 'claim' => $claimtitle));
        }
    }

    public function view_windscreen_claim_details($id, $client_type) {
        $this->mainlayout = 'layouts.test';
        $this->_setupLayout();
        $this->layout->title = "Linsure Insurance";
        $riskId = $id;
        $claimtitle = 'Windscreen Claim';

        if ($client_type == 5) {
            $partialdamage = DB::table('windscreen_indi')
                    ->join('clients_individual', 'windscreen_indi.indi_client_id', '=', 'clients_individual.id')
                    ->where('windscreen_indi.id', $id)
                    ->get();
            $this->layout->content = View::make('admin.claimdetailsview')->with(array('risk_id' => $riskId, 'type_client' => $client_type, 'partialdamage' => $partialdamage, 'claim' => $claimtitle));
        } elseif ($client_type == 10) {
            $partialdamage = DB::table('windscreen_bs')
                    ->join('clients_business', 'windscreen_bs.bs_client_id', '=', 'clients_business.id')
                    ->where('windscreen_bs.id', $id)
                    ->get();
            $this->layout->content = View::make('admin.claimdetailsviewbs')->with(array('risk_id' => $riskId, 'type_client' => $client_type, 'partialdamage' => $partialdamage, 'claim' => $claimtitle));
        }
    }

    public function view_radio_claim_details($id, $client_type) {
        $this->mainlayout = 'layouts.test';
        $this->_setupLayout();
        $this->layout->title = "Linsure Insurance";
        $riskId = $id;
        $claimtitle = 'Radio cassette claim';

        if ($client_type == 5) {
            $partialdamage = DB::table('radiocasette_indi')
                    ->join('clients_individual', 'radiocasette_indi.indi_client_id', '=', 'clients_individual.id')
                    ->where('radiocasette_indi.id', $id)
                    ->get();
            $this->layout->content = View::make('admin.claimdetailsview')->with(array('risk_id' => $riskId, 'type_client' => $client_type, 'partialdamage' => $partialdamage, 'claim' => $claimtitle));
        } elseif ($client_type == 10) {
            $partialdamage = DB::table('radiocasette_bs')
                    ->join('clients_business', 'radiocasette_bs.bs_client_id', '=', 'clients_business.id')
                    ->where('radiocasette_bs.id', $id)
                    ->get();
            $this->layout->content = View::make('admin.claimdetailsviewbs')->with(array('risk_id' => $riskId, 'type_client' => $client_type, 'partialdamage' => $partialdamage, 'claim' => $claimtitle));
        }
    }

    /**
     * Email module
     */
    public function mail_view() {
        $this->mainlayout = 'layouts.test';
        $this->_setupLayout();
        $this->layout->title = "Linsure Insurance";
        $this->layout->content = View::make('admin.mailsend');
    }

    //send emails
    public function send_email() {
        $data = Input::all();

        $rules = array(
            'email_to' => 'required',
            'subject' => 'required'
        );
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->passes()) {
//            Mail::send('admin.mail', $data, function($message) use ($data) {
//                $message->from('murimi.kenn@gmail.com', 'kennedy');
//                $message->to(Input::get('email_to'))->cc(Input::get('email_cc'))->subject(Input::get('subject'));
//                $message->attach(Input::get('attachment'));
//            });

            Mail::send('admin.mail', $data, function($message) use ($data) {
                $message->to(Input::get('email_to'))
                        ->subject(Input::get('subject'));
                if ($data['attachment']) {
                    $message->attach($data['attachment']->getRealPath(), array(
                        'as' => 'Linsure' . $data['attachment']->getClientOriginalExtension(),
                        'mime' => $data['attachment']->getMimeType())
                    );
                }
            });

            return Redirect::to('admin/mailview');
        } else {
//return contact form with errors
            return Redirect::to('admin/mailview')->withErrors($validator);
        }
    }

    /*
     * Exporting to excell module 
     */

    /*
     * exporting clients to excell view
     */

    public function export_clients_excel() {
        $this->mainlayout = 'layouts.test';
        $this->_setupLayout();
        $this->layout->title = "Linsure Insurance";
        $this->layout->content = View::make('admin.clientsexcell');
    }

    public function export_indi_clients() {
        $clients_individual = DB::table('clients_individual')
                ->where('status', 1)
                ->get($columns = array('first_name',
            'second_name',
            'email',
            'phone_number',
            'postal_address',
            'location'
        ));
        $name = "individual excel sheet";
        $heading = array(
            'first name', 'second name', 'email address', 'phone number', 'postal address', 'location'
        );
        $this->exportexcel($clients_individual, $name, $heading);
    }

    public function export_bs_clients() {
        $clients_individual = DB::table('clients_business')
                ->where('status', 1)
                ->get($columns = array('business_name',
            'contact_person',
            'business_email',
            'business_phone',
            'location',
            'postal_address'
        ));
        $name = "business excel sheet";
        $heading = array(
            'Business name', 'Contact person', 'Business email', 'Business phone number', 'location', 'postal address'
        );
        $this->exportexcel($clients_individual, $name, $heading);
    }

    /*
     * export insurance covers to excel
     */

    public function export_ins_excel_view() {
        $this->mainlayout = 'layouts.test';
        $this->_setupLayout();
        $this->layout->title = "Linsure Insurance";
        $this->layout->content = View::make('admin.insuranceexcell');
    }

    public function export_ins_excel() {
        $insurance_individual = DB::table('vehicle_insurance_indi')
                ->join('clients_individual', 'vehicle_insurance_indi.indi_client_id', '=', 'clients_individual.id')
                ->where('vehicle_insurance_indi.status', 1)
                ->get($columns = array(
            'vehicle_insurance_indi.car_registration',
            'vehicle_insurance_indi.engine_number',
            'vehicle_insurance_indi.insurance_start_date',
            'vehicle_insurance_indi.insurance_end_date',
            'vehicle_insurance_indi.courtesy_vehicle',
            'vehicle_insurance_indi.manufacturing_year',
            'vehicle_insurance_indi.cubic_capacity',
            'vehicle_insurance_indi.vehicle_value',
            'vehicle_insurance_indi.radio_cassette',
            'vehicle_insurance_indi.anti_theft',
            'vehicle_insurance_indi.windscreen',
            'vehicle_insurance_indi.chasis_number',
            'vehicle_insurance_indi.premium',
            'clients_individual.first_name',
            'clients_individual.second_name',
            'clients_individual.email',
            'clients_individual.phone_number'));
        $name = "insurance excel sheet";
        $heading = array(
            'car registration no',
            'engine number',
            'insurance start date',
            'insurance end date',
            'courtesy vehicle',
            'manufacturing year',
            'cubic capacity',
            'vehicle value',
            'radio cassette',
            'anti theft',
            'wind screen',
            'chasis number',
            'premium',
            'first name',
            'second name',
            'email',
            'phone number'
        );
        $this->exportexcel($insurance_individual, $name, $heading);
    }

    public function export_bs_excel() {
        $insurance_bs = DB::table('vehicle_insurance_bs')
                ->join('clients_business', 'vehicle_insurance_bs.bs_client_id', '=', 'clients_business.id')
                ->where('vehicle_insurance_bs.status', 1)
                ->get($columns = array(
            'vehicle_insurance_bs.car_registration',
            'vehicle_insurance_bs.engine_number',
            'vehicle_insurance_bs.insurance_start_date',
            'vehicle_insurance_bs.insurance_end_date',
            'vehicle_insurance_bs.courtesy_vehicle',
            'vehicle_insurance_bs.manufacturing_year',
            'vehicle_insurance_bs.cubic_capacity',
            'vehicle_insurance_bs.vehicle_value',
            'vehicle_insurance_bs.radio_cassette',
            'vehicle_insurance_bs.anti_theft',
            'vehicle_insurance_bs.windscreen',
            'vehicle_insurance_bs.chasis_number',
            'vehicle_insurance_bs.premium',
            'clients_business.business_name',
            'clients_business.business_phone',
            'clients_business.business_email'));
        $name = "insurance excel sheet";
        $heading = array(
            'car registration no',
            'engine number',
            'insurance start date',
            'insurance end date',
            'courtesy vehicle',
            'manufacturing year',
            'cubic capacity',
            'vehicle value',
            'radio cassette',
            'anti theft',
            'wind screen',
            'chasis number',
            'premium',
            'business name',
            'phone no',
            'email'
        );
        $this->exportexcel($insurance_bs, $name, $heading);
    }

    public function export_med_excel() {
        $insurance_medical = DB::table('medical_insurance_indi')
                ->join('clients_individual', 'medical_insurance_indi.indi_client_id', '=', 'clients_individual.id')
                ->where('medical_insurance_indi.status', 1)
                ->get($columns = array(
            'medical_insurance_indi.date_from',
            'medical_insurance_indi.date_to',
            'clients_individual.first_name',
            'clients_individual.second_name',
            'clients_individual.email',
            'clients_individual.phone_number'));
        $name = "medical excel sheet";
        $heading = array(
            'insurance start date',
            'insurance end date',
            'first name',
            'second name',
            'email',
            'phone number'
        );
        $this->exportexcel($insurance_medical, $name, $heading);
    }

    /**
     * export claims to excel
     */
    public function export_claims() {
        $this->mainlayout = 'layouts.test';
        $this->_setupLayout();
        $this->layout->title = "Linsure Insurance";
        $this->layout->content = View::make('admin.claimsexcell');
    }

    /**
     * export to excel function
     * 
     * @param type $arrayfromdb = values received from database
     * @param type $name = name of excell sheet
     * @param type $headrow = ist row heading
     * 
     */
    public function exportexcel($arrayfromdb, $name, $headrow) {


        for ($i = 0, $c = count($arrayfromdb); $i < $c; ++$i) {
            $arrayfromdb[$i] = (array) $arrayfromdb[$i];
        }
        Excel::create($name, function($excel) use($arrayfromdb, $headrow) {

            $excel->sheet('Sheetname', function($sheet) use($arrayfromdb, $headrow) {

                $sheet->fromArray($arrayfromdb);
                $sheet->row(1, $headrow);
            });
        })->download('xls');
    }

    /**
     * view covers that have passed
     */
    public function getpastdates() {
        $insurance_individual = DB::table('vehicle_insurance_indi')
                ->where('vehicle_insurance_indi.status', 1)
                ->get($columns = array(
            'vehicle_insurance_indi.id',
            'vehicle_insurance_indi.insurance_end_date'));
        $today = date('y-m-d');
        foreach ($insurance_individual as $ins_enddate) {
            $enddate = $ins_enddate->insurance_end_date;
            $itemid = $ins_enddate->id;

            if (strtotime($today) >= strtotime($enddate)) {

                echo "$itemid has expired";
            } else {
                echo "$itemid has not expired";
            }
            echo '<br> </br>';
        }
    }

    /**
     * renew ins
     */
    public function renew_ins($id) {
        $ins = DB::table('vehicle_insurance_bs')
                ->join('clients_business', 'vehicle_insurance_bs.bs_client_id', '=', 'clients_business.id')
                ->where('vehicle_insurance_bs.insurance_id', $id)
                ->get();
        $type = "vehicle_insurance_bs";
        $type1 = "vehicle insurance";

        if (empty($ins)) {
            $ins = DB::table('vehicle_insurance_indi')
                    ->join('clients_individual', 'vehicle_insurance_indi.indi_client_id', '=', 'clients_individual.id')
                    ->where('vehicle_insurance_indi.insurance_id', $id)
                    ->get();
            $type = "vehicle_insurance_indi";
            $type1 = "vehicle insurance";

            if (empty($ins)) {
                $ins = DB::table('medical_insurance_indi')
                        ->join('clients_individual', 'medical_insurance_indi.indi_client_id', '=', 'clients_individual.id')
                        ->where('medical_insurance_indi.insurance_number', $id)
                        ->get();
                $type = "medical_insurance_indi";
                $type1 = "medical insurance";
                if (empty($ins)) {
                    $ins = DB::table('medical_insurance_bs')
                            ->join('clients_business', 'medical_insurance_bs.bs_client_id', '=', 'clients_business.id')
                            ->where('medical_insurance_bs.insurance_number', $id)
                            ->get();
                    $type = "medical_insurance_bs";
                    $type1 = "medical insurance";
                }
            }
        }
        $this->mainlayout = 'layouts.test';
        $this->_setupLayout();
        $this->layout->title = "Linsure Insurance";
        $this->layout->content = View::make('admin.renewins')->with(array('ins' => $ins, 'type' => $type, 'typeclient' => $type1));
    }

    Public function renew_insurancepage($id) {
        $this->mainlayout = 'layouts.test';
        $this->_setupLayout();
        $this->layout->title = "Linsure Insurance";
        $this->layout->content = View::make('admin.renewinsurancepage');
    }

    /**
     * Accounts module
     * 
     * Display vehicle insurance invoice view
     */
    public function invoice_client() {
        $clients_individual = DB::table('clients_individual')
                ->join('vehicle_insurance_indi', 'clients_individual.id', '=', 'vehicle_insurance_indi.indi_client_id')
                ->where('clients_individual.status', 1)
                ->where('clients_individual.vehicle_status', 1)
                ->get($columns = array(
            'clients_individual.first_name',
            'clients_individual.second_name',
            'clients_individual.email',
            'clients_individual.phone_number',
            'clients_individual.location',
            'clients_individual.id',
            'vehicle_insurance_indi.insurance_id'
        ));

        $clients_bs = DB::table('clients_business')
                ->join('vehicle_insurance_bs', 'clients_business.id', '=', 'vehicle_insurance_bs.bs_client_id')
                ->where('clients_business.status', 1)
                ->where('clients_business.vehicle_status', 1)
                ->get($columns = array(
            'clients_business.business_name',
            'clients_business.contact_person',
            'clients_business.business_email',
            'clients_business.business_phone',
            'clients_business.location',
            'clients_business.id',
            'vehicle_insurance_bs.insurance_id'
        ));
        $this->mainlayout = 'layouts.test';
        $this->_setupLayout();
        $this->layout->title = "Linsure Insurance";
        $this->layout->content = View::make('admin.createinvoice')->with(array('client_individuals' => $clients_individual, 'client_business' => $clients_bs));
    }

    //Display medicle insurance to invoice
    public function invoice_medicle_client() {
        $clients_individual = DB::table('clients_individual')
                ->where('status', 1)
                ->where('med_status', 1)
                ->get();

        $clients_bs = DB::table('clients_business')
                ->where('status', 1)
                ->where('med_status', 1)
                ->get();

        $this->mainlayout = 'layouts.test';
        $this->_setupLayout();
        $this->layout->title = "Linsure Insurance";
        $this->layout->content = View::make('admin.createmedinvoice')->with(array('client_individuals' => $clients_individual, 'client_business' => $clients_bs));
    }

    //vehicle insurance invoice template
    public function invoice_template($id, $type_client, $ins_id) {

        $exists = DB::table('invoice_description')
                ->where('insurance_id_num', $ins_id)
                ->get();
        if (!$exists) {

            $InvoiceDescription = new InvoiceDescription();
            $InvoiceDescription->insurance_id_num = $ins_id;
            $InvoiceDescription->description = "Description here";
            $InvoiceDescription->save();

            $InvoiceGetDescription = DB::table('invoice_description')
                    ->where('insurance_id_num', $ins_id)
                    ->get();
            $settings = DB::table('settings')
                    ->get();
            if ($type_client == 5) {
                $name = DB::table('clients_individual')
                        ->where('clients_individual.id', $id)
                        ->join('vehicle_insurance_indi', 'clients_individual.id', '=', 'vehicle_insurance_indi.indi_client_id')
                        ->get($columns = array(
                    'clients_individual.first_name',
                    'clients_individual.second_name',
                    'clients_individual.postal_address',
                    'clients_individual.location',
                    'vehicle_insurance_indi.car_registration',
                    'vehicle_insurance_indi.vehicle_value',
                    'vehicle_insurance_indi.premium',
                    'vehicle_insurance_indi.total_premium',
                    'vehicle_insurance_indi.insurance_id',
                    'vehicle_insurance_indi.traininglevy',
                    'vehicle_insurance_indi.phcflevy'));
            } else if ($type_client == 10) {
                $name = DB::table('clients_business')
                        ->where('clients_business.id', $id)
                        ->join('vehicle_insurance_bs', 'clients_business.id', '=', 'vehicle_insurance_bs.bs_client_id')
                        ->get($columns = array(
                    'clients_business.business_name',
                    'clients_business.postal_address',
                    'clients_business.location',
                    'vehicle_insurance_bs.car_registration',
                    'vehicle_insurance_bs.vehicle_value',
                    'vehicle_insurance_bs.premium',
                    'vehicle_insurance_bs.total_premium',
                    'vehicle_insurance_bs.insurance_id',
                    'vehicle_insurance_bs.traininglevy',
                    'vehicle_insurance_bs.phcflevy'));
            }

            $this->mainlayout = 'layouts.test';
            $this->_setupLayout();
            $this->layout->title = "Linsure Insurance";
            $this->layout->content = View::make('admin.invoice_template')->with(array('name' => $name, 'client_type' => $type_client, 'settings' => $settings, 'insurance_id' => $ins_id, 'Description_invoice' => $InvoiceGetDescription, 'client_id' => $id));
        } else {
            return Redirect::to('admin/invoiceClient')
                            ->with('errorMessage', 'Insurance description for this cover already exists');
        }
    }

    //view vehicle insurance invoice template

    public function view_invoice_template($id, $type_client, $ins_id) {
        $exists = DB::table('invoice_description')
                ->where('insurance_id_num', $ins_id)
                ->get();
        if ($exists) {
            $InvoiceGetDescription = DB::table('invoice_description')
                    ->where('insurance_id_num', $ins_id)
                    ->get();
            $settings = DB::table('settings')
                    ->get();

            if ($type_client == 5) {

                $name = DB::table('clients_individual')
                        ->where('clients_individual.id', $id)
                        ->join('vehicle_insurance_indi', 'clients_individual.id', '=', 'vehicle_insurance_indi.indi_client_id')
                        ->get($columns = array(
                    'clients_individual.first_name',
                    'clients_individual.second_name',
                    'clients_individual.postal_address',
                    'clients_individual.location',
                    'vehicle_insurance_indi.car_registration',
                    'vehicle_insurance_indi.vehicle_value',
                    'vehicle_insurance_indi.premium',
                    'vehicle_insurance_indi.total_premium',
                    'vehicle_insurance_indi.insurance_id',
                    'vehicle_insurance_indi.traininglevy',
                    'vehicle_insurance_indi.phcflevy'));
            } else if ($type_client == 10) {
                $name = DB::table('clients_business')
                        ->where('clients_business.id', $id)
                        ->join('vehicle_insurance_bs', 'clients_business.id', '=', 'vehicle_insurance_bs.bs_client_id')
                        ->get($columns = array(
                    'clients_business.business_name',
                    'clients_business.postal_address',
                    'clients_business.location',
                    'vehicle_insurance_bs.car_registration',
                    'vehicle_insurance_bs.vehicle_value',
                    'vehicle_insurance_bs.premium',
                    'vehicle_insurance_bs.total_premium',
                    'vehicle_insurance_bs.insurance_id',
                    'vehicle_insurance_bs.traininglevy',
                    'vehicle_insurance_bs.phcflevy'));
            }

            $this->mainlayout = 'layouts.test';
            $this->_setupLayout();
            $this->layout->title = "Linsure Insurance";
            $this->layout->content = View::make('admin.invoice_template_view')->with(array('name' => $name, 'client_type' => $type_client, 'settings' => $settings, 'insurance_id' => $ins_id, 'Description_invoice' => $InvoiceGetDescription));
        } else {
            return Redirect::to('admin/invoiceClient')
                            ->with('errorMessage', 'Invoice for this cover doesnt exit or hasent been created yet');
        }
    }

    //update invoice description
    public function update_invoice() {
        $rules = array(
            'invoice' => 'required'
        );



        $validator = Validator::make(Input::all(), $rules);
        if ($validator->passes()) {
            $updating = DB::table('invoice_description')
                    ->where('id', Input::get('id_update'))
                    ->update(array('description' => Input::get('invoice')));
            if ($updating) {

                return Redirect::to('admin/invoiceClient')->with('message', 'Successfully Updated. Click on view invoice to see the invoice');
            } else {
                return Redirect::to('admin/invoiceClient')->with('errorMessage', 'Not Successfully Updated');
            }
        } else {
            return Redirect::to('admin/settings')
                            ->withErrors($validator)
                            ->withInput(Input::all())
                            ->with('errorMessage', 'Description cannot be empty');
        }
    }

    public function invoice_edit($id) {
        $invoice_edit = DB::table('invoice_description')
                ->where('insurance_id_num', $id)
                ->get();
        $this->mainlayout = 'layouts.test';
        $this->_setupLayout();
        $this->layout->title = "Linsure Insurance";
        $this->layout->content = View::make('admin.invoice_edit')->with(array('invoice_edit' => $invoice_edit));
    }

    /**
     * Credit note module begins here
     * @param type $id -> id of client
     * @param type $typeClient -> 5 for individual 10 for business
     */
    //credit note view for date of insurance cancel 
    public function creditnotedateissue_View($id, $typeClient) {
        $this->mainlayout = 'layouts.test';
        $this->_setupLayout();
        $this->layout->title = "Linsure Insurance";
        $this->layout->content = View::make('admin.creditnotedatepick')->with(array('credit_id' => $id, 'type_client' => $typeClient));
    }

    //credit note details post to database
    public function credit_add_post() {
        $id = Input::get('id');
        $type_client = Input::get('type_client');
        $idfromdb = null;
        if ($type_client == 5) {
            $idexist = DB::table('credit_note_details')
                    ->where('vehicle_ins_id', $id)
                    ->get();
            foreach ($idexist as $idex) {
                $idfromdb = $idex->vehicle_ins_id;
            }
            var_dump($idfromdb);
            if ($idfromdb != $id) {
                $rules = array(
                    'insurance_cancel_date' => 'required',
                    'amended_by' => 'required',
                );
                $validator = Validator::make(Input::all(), $rules);
                if ($validator->passes()) {
                    $add_credit = new CreditNoteDetails();
                    $add_credit->date_cancelled = Input::get('insurance_cancel_date');
                    $add_credit->amended_by = Input::get('amended_by');
                    $add_credit->vehicle_ins_id = $id;
                    $add_credit->save();
                    return Redirect::to('admin/creditNoteDateissue/' . $id . '/' . $type_client)->with('message', 'Details successfully added');
                } else {
                    return Redirect::to('admin/creditNoteDateissue/' . $id . '/' . $type_client)
                                    ->withErrors($validator)
                                    ->withInput(Input::all())
                                    ->with('errorMessage', 'Details not successfully added. Confirm that all fields are filled and no credit note exits to this cover');
                }
            } else {
                return Redirect::to('admin/creditNoteDateissue/' . $id . '/' . $type_client)
                                ->with('errorMessage', 'Confirm that credit note doesnt exits to this cover');
            }
        } elseif ($type_client == 10) {
            $idexist = DB::table('credit_note_details_bs')
                    ->where('vehicle_ins_id', $id)
                    ->get();
            foreach ($idexist as $idex) {
                $idfromdb = $idex->vehicle_ins_id;
            }
            if ($idfromdb != $id) {
                $ruless = array(
                    'insurance_cancel_date' => 'required',
                    'amended_by' => 'required'
                );
                $validatorr = Validator::make(Input::all(), $ruless);
                if ($validatorr->passes()) {
                    $add_creditt = new CreditNoteDetailsBs();
                    $add_creditt->date_cancelled = Input::get('insurance_cancel_date');
                    $add_creditt->amended_by = Input::get('amended_by');
                    $add_creditt->vehicle_ins_id = $id;
                    $add_creditt->save();
                    return Redirect::to('admin/creditNoteDateissue/' . $id . '/' . $type_client)->with('message', 'Details successfully added');
                } else {
                    return Redirect::to('admin/creditNoteDateissue/' . $id . '/' . $type_client)
                                    ->withErrors($validatorr)
                                    ->withInput(Input::all())
                                    ->with('errorMessage', 'Details not successfully added. Confirm that all fields are filled and no credit note exits to this cover');
                }
            } else {
                return Redirect::to('admin/creditNoteDateissue/' . $id . '/' . $type_client)
                                ->with('errorMessage', 'Confirm that credit note doesnt exits to this cover');
            }
        }
    }

    //credit note doesnt exist
    public function creditnote_doesntexist() {
        $this->mainlayout = 'layouts.test';
        $this->_setupLayout();
        $this->layout->title = "Linsure Insurance";
        $this->layout->content = View::make('admin.creditnotedoesntexist');
    }

    //credit note view before pdf generation
    public function creditnote_View($id, $typeClient) {
        $this->mainlayout = 'layouts.test';
        $this->_setupLayout();
        $this->layout->title = "Linsure Insurance";
        $creditId = $id;
        $insurance;
        $idfromdb = null;
        if ($typeClient == 5) {
            $idexist = DB::table('credit_note_details')
                    ->where('vehicle_ins_id', $id)
                    ->get();
            foreach ($idexist as $idex) {
                $idfromdb = $idex->vehicle_ins_id;
            }
            if ($idfromdb) {
                $insurance = DB::table('vehicle_insurance_indi')
                        ->join('clients_individual', 'vehicle_insurance_indi.indi_client_id', '=', 'clients_individual.id')
                        ->where('vehicle_insurance_indi.id', $id)
                        ->get();
                $creditNoteDetails = DB::table('credit_note_details')
                        ->where('vehicle_ins_id', $id)
                        ->get();
                $this->layout->content = View::make('admin.creditnote')->with(array('credit_id' => $creditId, 'type_client' => $typeClient, 'insurance' => $insurance, 'credit_note_details' => $creditNoteDetails));
            } else {
                return Redirect::to('admin/creditNoteDoesntExist')
                                ->with('errorMessage', 'Credit note doesnt exits to this cover');
            }
        } elseif ($typeClient == 10) {
            $idexist = DB::table('credit_note_details_bs')
                    ->where('vehicle_ins_id', $id)
                    ->get();
            foreach ($idexist as $idex) {
                $idfromdb = $idex->vehicle_ins_id;
            }
            if ($idfromdb) {

                $insurance = DB::table('vehicle_insurance_bs')
                        ->join('clients_business', 'vehicle_insurance_bs.bs_client_id', '=', 'clients_business.id')
                        ->where('vehicle_insurance_bs.id', $id)
                        ->get();
                $creditNoteDetails = DB::table('credit_note_details_bs')
                        ->where('vehicle_ins_id', $id)
                        ->get();
                $this->layout->content = View::make('admin.creditnote')->with(array('credit_id' => $creditId, 'type_client' => $typeClient, 'insurance' => $insurance, 'credit_note_details' => $creditNoteDetails));
            } else {
                return Redirect::to('admin/creditNoteDoesntExist')
                                ->with('errorMessage', 'Credit note doesnt exits to this cover');
            }
        }
    }

    //credit note pdf generation
    public function creditnote_pdf($id, $typeClient) {
        $insurance;
        if ($typeClient == 5) {
            $insurance['insurance'] = DB::table('vehicle_insurance_indi')
                    ->join('clients_individual', 'vehicle_insurance_indi.indi_client_id', '=', 'clients_individual.id')
                    ->where('vehicle_insurance_indi.id', $id)
                    ->get();
            $credit_note_details['credit_note_details'] = DB::table('credit_note_details')
                    ->where('vehicle_ins_id', $id)
                    ->get();

            $pdf = PDF::loadView('admin.creditnotepdf', $insurance, $credit_note_details);
        } elseif ($typeClient == 10) {
            $insurance['insurance'] = DB::table('vehicle_insurance_bs')
                    ->join('clients_business', 'vehicle_insurance_bs.bs_client_id', '=', 'clients_business.id')
                    ->where('vehicle_insurance_bs.id', $id)
                    ->get();
            $credit_note_details['credit_note_details'] = DB::table('credit_note_details_bs')
                    ->where('vehicle_ins_id', $id)
                    ->get();
            $pdf = PDF::loadView('admin.creditnotepdfbs', $insurance, $credit_note_details);
        }

        return $pdf->stream('');
    }

    /**
     * Medicine credit note module
     */
    //credit note cancel date issue medicine ins
    public function creditnotedateissue_med_View($id) {
        $this->mainlayout = 'layouts.test';
        $this->_setupLayout();
        $this->layout->title = "Linsure Insurance";
        $this->layout->content = View::make('admin.creditnotedatepickmed')->with(array('credit_id' => $id));
    }

    //credit note medicine post to db
    public function credit_add_med_post() {
        $id = Input::get('id');
        $idfromdb = null;
        $idexist = DB::table('credit_note_details_med')
                ->where('med_id', $id)
                ->get();
        foreach ($idexist as $idex) {
            $idfromdb = $idex->med_id;
        }
        var_dump($idfromdb);
        if ($idfromdb != $id) {
            $rules = array(
                'insurance_cancel_date' => 'required',
                'amended_by' => 'required',
            );
            $validator = Validator::make(Input::all(), $rules);
            if ($validator->passes()) {
                $add_credit = new CreditNoteDetailsMed();
                $add_credit->date_cancelled = Input::get('insurance_cancel_date');
                $add_credit->amended_by = Input::get('amended_by');
                $add_credit->med_id = $id;
                $add_credit->save();
                return Redirect::to('admin/creditNoteDateissueMed/' . $id)->with('message', 'Details successfully added');
            } else {
                return Redirect::to('admin/creditNoteDateissueMed/' . $id)
                                ->withErrors($validator)
                                ->withInput(Input::all())
                                ->with('errorMessage', 'Details not successfully added. Confirm that all fields are filled and no credit note exits to this cover');
            }
        } else {
            return Redirect::to('admin/creditNoteDateissueMed/' . $id)
                            ->with('errorMessage', 'Confirm that credit note doesnt exits to this cover');
        }
    }

    public function creditnote_med_View($id) {
        $this->mainlayout = 'layouts.test';
        $this->_setupLayout();
        $this->layout->title = "Linsure Insurance";
        $creditId = $id;
        $insurance;
        $idfromdb = null;
        $idexist = DB::table('credit_note_details_med')
                ->where('med_id', $id)
                ->get();
        foreach ($idexist as $idex) {
            $idfromdb = $idex->med_id;
        }
        if ($idfromdb) {
            $insurance = DB::table('medical_insurance_indi')
                    ->join('clients_individual', 'medical_insurance_indi.indi_client_id', '=', 'clients_individual.id')
                    ->where('medical_insurance_indi.id', $id)
                    ->get();
            $creditNoteDetails = DB::table('credit_note_details_med')
                    ->where('med_id', $id)
                    ->get();
            $this->layout->content = View::make('admin.creditnote')->with(array('credit_id' => $creditId, 'insurance' => $insurance, 'credit_note_details' => $creditNoteDetails));
        } else {
            return Redirect::to('admin/creditNoteDoesntExist')
                            ->with('errorMessage', 'Credit note doesnt exits to this cover');
        }
    }

}
