<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>{{ $title }} &reg; </title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- bootstrap 3.0.2 -->
        {{ HTML::style('packages/components/css/bootstrap.min.css', array('media' => 'screen')) }}

        <!-- font Awesome -->
        {{ HTML::style('packages/components/css/font-awesome.min.css', array('media' => 'screen')) }}

        <!-- Ionicons -->
        {{ HTML::style('packages/components/css/ionicons.min.css', array('media' => 'screen')) }}

        <!-- DATA TABLES -->
        {{ HTML::style('packages/components/css/datatables/dataTables.bootstrap.css', array('media' => 'screen')) }}

        {{ HTML::style('packages/components/css/fullcalendar/fullcalendar.css', array('media' => 'screen')) }}
        {{ HTML::style('packages/components/css/fullcalendar/fullcalendar.print.css', array('media' => 'screen')) }}

        <!-- Theme style -->
        {{ HTML::style('packages/components/css/AdminLTE.css', array('media' => 'screen')) }}



        <!-- Ionicons -->
        {{ HTML::style('packages/components/css/ionicons.min.css', array('media' => 'screen')) }}

        <!-- Morris chart -->
        {{ HTML::style('packages/components/css/morris/morris.css', array('media' => 'screen')) }}


        <!-- jvectormap -->
        {{ HTML::style('packages/components/css/jvectormap/jquery-jvectormap-1.2.2.css', array('media' => 'screen')) }}




        <!-- Daterange picker -->
        {{ HTML::style('packages/components/css/daterangepicker/daterangepicker-bs3.css', array('media' => 'screen')) }}


        <!-- bootstrap wysihtml5 - text editor -->
        {{ HTML::style('packages/components/css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css', array('media' => 'screen')) }}

        {{ HTML::style('packages/components/css/datepicker.min.css', array('media' => 'screen')) }}





        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->


    </head>
    <body class="skin-black">
        <!-- header logo: style can be found in header.less -->
        <header class="header">
            <a href="{{ URL::to('admin/dashboard') }}" class="logo">
                <!-- Add the class icon to your logo image or logo icon to add the margining -->
                Linsure Insurance &reg;
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-right">
                    <ul class="nav navbar-nav">

                        <?php
                        $insurance_individual = DB::table('vehicle_insurance_indi')
                                ->where('vehicle_insurance_indi.status', 1)
                                ->get($columns = array(
                            'vehicle_insurance_indi.insurance_id',
                            'vehicle_insurance_indi.insurance_end_date'));

                        $insurance_bs = DB::table('vehicle_insurance_bs')
                                ->where('vehicle_insurance_bs.status', 1)
                                ->get($columns = array(
                            'vehicle_insurance_bs.insurance_id',
                            'vehicle_insurance_bs.insurance_end_date'));

                        $med_indi = DB::table('medical_insurance_indi')
                                ->where('medical_insurance_indi.status', 1)
                                ->get($columns = array(
                            'medical_insurance_indi.insurance_number',
                            'medical_insurance_indi.date_to'));


                        $med_bs = DB::table('medical_insurance_bs')
                                ->where('medical_insurance_bs.status', 1)
                                ->get($columns = array(
                            'medical_insurance_bs.insurance_number',
                            'medical_insurance_bs.date_to'));

                        $today = date('y-m-d');
                        ?>
                        <!-- Tasks: style can be found in dropdown.less -->
                        <li class="dropdown tasks-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="fa fa-tasks"></i>
                                <?php
                                $count = 0;
                                $bscount = 0;
                                $medcountindi = 0;
                                $medcountbs = 0;
                                foreach ($insurance_individual as $ins_enddate) {
                                    $count++;
                                }
                                foreach ($insurance_bs as $ins_enddate) {
                                    $bscount++;
                                }
                                foreach ($med_indi as $ins_enddate) {
                                    $medcountindi++;
                                }
                                foreach ($med_bs as $ins_enddate) {
                                    $medcountbs++;
                                }
                                ?>
                                <span class="label label-danger">{{$count+$bscount+$medcountindi+$medcountbs}}</span>

                            </a>
                            <ul class="dropdown-menu">
                                <li class="header text-center">Expired insurance covers</li>
                                <li>
                                    <!-- inner menu: contains the actual data -->

                                    <ul class="menu">
                                        <?php
                                        foreach ($insurance_individual as $ins_enddate) {
                                            $enddate = $ins_enddate->insurance_end_date;
                                            $itemid = $ins_enddate->insurance_id;
                                            ?>
                                            <li><!-- Task item -->
                                                <a href="{{ URL::to('admin/renewins/'.$itemid) }}">
                                                    <h3>
                                                        <p  class="text-danger">
                                                            <?php if (strtotime($today) >= strtotime($enddate)) { ?>
                                                                <?php echo "Insurance no "; ?><i class="text-aqua">{{$itemid}}</i> is expired</p>
                                                            <?php } ?>
                                                    </h3>
                                                </a>
                                            </li><!-- end task item -->
                                        <?php } ?>
                                        <?php
                                        foreach ($insurance_bs as $ins_enddate) {
                                            $enddate = $ins_enddate->insurance_end_date;
                                            $itemid = $ins_enddate->insurance_id;
                                            ?>
                                            <li><!-- Task item -->
                                                <a href="{{ URL::to('admin/renewins/'.$itemid) }}">
                                                    <h3>
                                                        <p  class="text-danger">
                                                            <?php if (strtotime($today) >= strtotime($enddate)) { ?>
                                                                <?php echo "Insurance no "; ?><i class="text-aqua">{{$itemid}}</i> is expired</p>
                                                            <?php } ?>
                                                    </h3>
                                                </a>
                                            </li><!-- end task item -->
                                        <?php } ?>
                                        <?php
                                        foreach ($med_indi as $ins_enddate) {
                                            $enddate = $ins_enddate->date_to;
                                            $itemid = $ins_enddate->insurance_number;
                                            ?>
                                            <li><!-- Task item -->
                                                <a href="{{ URL::to('admin/renewins/'.$itemid) }}">
                                                    <h3>
                                                        <p  class="text-danger">
                                                            <?php if (strtotime($today) >= strtotime($enddate)) { ?>
                                                                <?php echo "Insurance no "; ?><i class="text-aqua">{{$itemid}}</i> is expired</p>
                                                            <?php } ?>
                                                    </h3>
                                                </a>
                                            </li><!-- end task item -->
                                        <?php } ?>
                                        <?php
                                        foreach ($med_bs as $ins_enddate) {
                                            $enddate = $ins_enddate->date_to;
                                            $itemid = $ins_enddate->insurance_number;
                                            ?>
                                            <li><!-- Task item -->
                                                <a href="{{ URL::to('admin/renewins/'.$itemid) }}">
                                                    <h3>
                                                        <p  class="text-danger">
                                                            <?php if (strtotime($today) >= strtotime($enddate)) { ?>
                                                                <?php echo "Insurance no "; ?><i class="text-aqua">{{$itemid}}</i> is expired</p>
                                                            <?php } ?>
                                                    </h3>
                                                </a>
                                            </li><!-- end task item -->
                                        <?php } ?>

                                    </ul>
                                </li>
                                <li class="footer">

                                </li>
                            </ul>
                        </li>
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="glyphicon glyphicon-user"></i>
                                <span>{{ Confide::user()->username ?: '' }} <i class="caret"></i></span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header bg-light-blue">
                                    <img src="{{ asset('packages/components/img/avatar3.png') }}" class="img-circle" alt="User Image" />
                                    <p>
                                        {{ Confide::user()->username ?: '' }}
                                        <small>Member since {{ Confide::user()->created_at ?: '' }}</small>
                                    </p>
                                </li>
                                <!-- Menu Body -->

                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a href="#" class="btn btn-default btn-flat">Profile</a>
                                    </div>
                                    <div class="pull-right">
                                        <a href="{{{ URL::to('users/logout') }}}" class="btn btn-default btn-flat">Sign out</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas">                
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="{{ asset('packages/components/img/avatar3.png') }}" class="img-circle" alt="User Image" />
                        </div>
                        <div class="pull-left info">
                            <p>Hello,  {{ Confide::user()->username ?: '' }}</p>

                            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>
                    <!-- search form -->
                    <!--                    <form action="#" method="get" class="sidebar-form">
                                            <div class="input-group">
                                                <input type="text" name="q" class="form-control" placeholder="Search..."/>
                                                <span class="input-group-btn">
                                                    <button type='submit' name='seach' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                                                </span>
                                            </div>
                                        </form>-->
                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <li>
                            <a href="{{ URL::to('admin/dashboard') }}">
                                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                            </a>
                        </li>

                        <li>
                            <a href="{{ URL::to('admin/settings') }}">
                                <i class="fa fa-gears"></i> <span>Settings</span>
                            </a>
                        </li>
                        <li class="active">
                            <a href="#">
                                <i class="fa a-home"></i> <span><b>Clients module</b></span>
                            </a>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-bar-chart-o"></i>
                                <span>Clients</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href="{{ URL::to('admin/clients') }}"><i class="fa fa-angle-double-right"></i> Create new client </a></li>
                                <li><a href="{{ URL::to('admin/viewClients') }}"><i class="fa fa-angle-double-right"></i> View clients</a></li>
                                <li><a href="{{ URL::to('admin/viewClientsDroped') }}"><i class="fa fa-angle-double-right"></i> View droped clients</a></li>
                                <li><a href="{{ URL::to('admin/exportclientsexcel') }}"><i class="fa fa-angle-double-right"></i> export clients to excell</a></li>

                            </ul>
                        </li>
                        <li class="active">
                            <a href="#">
                                <i class="fa a-home"></i> <span><b>Insurance module</b></span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ URL::to('admin/newInsurance') }}">
                                <i class="fa fa-umbrella"></i> <span>Create insurance</span>
                            </a>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-truck"></i>
                                <span>View insurance cover</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">

                                <li><a href="{{ URL::to('admin/viewVehicleInsurance') }}"><i class="fa fa-angle-double-right"></i>Vehicle insurance</a></li>
                                <li><a href="{{ URL::to('admin/viewVehicleInsuranceDroped') }}"><i class="fa fa-angle-double-right"></i>Dropped vehicle insurance</a></li>
                                <li><a href="{{ URL::to('admin/viewMedicleInsurance') }}"><i class="fa fa-angle-double-right"></i> medical insurance</a></li>
                                <li><a href="{{ URL::to('admin/viewDroppedMedicalInsurance') }}"><i class="fa fa-angle-double-right"></i>dropped medical insurance</a></li>
                                <li><a href="{{ URL::to('admin/exportinsexcelview') }}"><i class="fa fa-angle-double-right"></i>Export insurances to excel</a></li>
                            </ul>
                        </li>
                        <li class="active">
                            <a href="#">
                                <i class="fa a-home"></i> <span><b>Claims module</b></span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ URL::to('admin/selectclaimview') }}">
                                <i class="fa fa-sun-o"></i> <span>View claims</span>
                            </a>
                        </li>
                        <li class="active">
                            <a href="#">
                                <i class="fa a-home"></i> <span><b>Email module</b></span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ URL::to('admin/mailview') }}">
                                <i class="fa fa-envelope-o"></i> <span>Send email</span>
                            </a>
                        </li>
                        <li class="active">
                            <a href="#">
                                <i class="fa a-home"></i> <span><b>Accounts module</b></span>
                            </a>
                        </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-tint"></i>
                                <span>Invoice</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">

                                <li><a href="{{ URL::to('admin/invoiceClient') }}"><i class="fa fa-angle-double-right"></i>Vehicle insurance invoice</a></li>
                                <!--<li><a href="{{ URL::to('admin/invoiceMedClient') }}"><i class="fa fa-angle-double-right"></i>Medicle insurance invoice</a></li>-->

                            </ul>
                        </li>
                        <!--                        <li>
                                                    <a href="{{ URL::to('admin/invoiceClient') }}">
                                                        <i class="fa fa-tint"></i> <span>Compose invoice</span>
                                                    </a>
                                                </li>-->
                        </br>
                        </br>
                        </br>
                        </br>
                        </br>


                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">                
                <!-- Content Header (Page header) -->

                {{ $content }}

            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->


        <!-- jQuery 2.0.2 -->
        {{ HTML::script('packages/components/js/jquery.min.js') }}

        {{ HTML::script('packages/components/js/jquery-ui-1.10.3.min.js') }}

        <!-- Bootstrap -->
        {{ HTML::script('packages/components/js/bootstrap.min.js') }}

        {{ HTML::script('packages/components/js/bootstrap-datepicker.js') }}

        <!-- AdminLTE App -->
        {{ HTML::script('packages/components/js/AdminLTE/app.js') }}

        {{ HTML::script('packages/components/js/plugins/ckeditor/ckeditor.js') }}

        {{ HTML::script('packages/components/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}


        <!-- Morris.js charts -->
        <script src="http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
        <!--<script src="../../js/plugins/morris/morris.min.js" type="text/javascript"></script>-->
        <!-- Morris.js charts -->

<!--<script src="{{ asset('packages/components/js/raphael-min.js') }}"></script>-->
        <script src="{{ asset('packages/components/js/plugins/morris/morris.min.js') }}"></script>

        <!-- Sparkline -->
        <script src="{{ asset('packages/components/js/plugins/sparkline/jquery.sparkline.min.js') }}"></script>

        <!-- jvectormap -->
        <script src="{{ asset('packages/components/js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>

        <script src="{{ asset('packages/components/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}" type="text/javascript"></script>


        <!-- jQuery Knob Chart -->
        <script src="{{ asset('packages/components/js/plugins/jqueryKnob/jquery.knob.js') }}" type="text/javascript"></script>

        <!-- daterangepicker -->
        <script src="{{ asset('packages/components/js/plugins/daterangepicker/daterangepicker.js') }}" type="text/javascript"></script>

        <!-- Bootstrap WYSIHTML5 -->
        <script src="{{ asset('packages/components/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}" type="text/javascript"></script>

        <!-- iCheck -->
        <script src="{{ asset('packages/components/js/plugins/iCheck/icheck.min.js') }}" type="text/javascript"></script>


        <!-- DATA TABES SCRIPT -->
        {{ HTML::script('packages/components/js/plugins/datatables/jquery.dataTables.js') }}
        {{ HTML::script('packages/components/js/plugins/datatables/dataTables.bootstrap.js') }}

        {{ HTML::script('packages/components/js/plugins/fullcalendar/fullcalendar.min.js') }}

        <!-- page script -->
        <script type="text/javascript">
        $(function () {
        $("#example1").dataTable();
                $('#example2').dataTable({
        "bPaginate": true,
                "bLengthChange": true,
                "bFilter": true,
                "bSort": true,
                "bInfo": true,
                "bAutoWidth": true
        });
                $('#example3').dataTable({
        "bPaginate": true,
                "bLengthChange": true,
                "bFilter": true,
                "bSort": true,
                "bInfo": true,
                "bAutoWidth": true
        });
        });
        $('.input-group.date').datepicker({
format: "yyyy/mm/dd",
        startDate: "1990-01-01",
        endDate: "3015-01-01",
        todayBtn: "linked",
        autoclose: true,
        todayHighlight: true
        });
        $("#email_message").wysihtml5();</script>
        <script type="text/javascript">
                    $(function () {
                    "use strict";
<?php
$vehicle_insurance = DB::table('vehicle_insurance_indi')
        ->count($columns = '*');
$vehicle_insurance_bs = DB::table('vehicle_insurance_bs')
        ->count($columns = '*');
$total_motor_ins = $vehicle_insurance + $vehicle_insurance_bs;
$medical_insurance_indi = DB::table('medical_insurance_indi')
        ->count($columns = '*');
$medical_insurance_bs = DB::table('medical_insurance_bs')
        ->count($columns = '*');
$total_med_ins = $medical_insurance_indi + $medical_insurance_bs;
?>
                    //DONUT CHART
                    var donut = new Morris.Donut({
                    element: 'sales-chart',
                            resize: true,
                            colors: ["#3c8dbc", "#f56954", "#00a65a"],
                            data: [
                            {label: "Vehicle insurance", value: {{$total_motor_ins}}},
                            {label: "Medicle insurance", value: {{$total_med_ins}}}
                            ],
                            hideHover: 'auto'
                    });
                    });
        </script>
    </body>
</html>