<section class="content-header">
    <h1>
        Vehicle insurance policy:
        <small>Create vehicle insurance policy</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::to('admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Vehicle insurance</li>
    </ol>
</section>

<section class="content">
    @if(Session::has('message')) 
    <div class="alert alert-success alert-dismissable col-md-10">
        <i class="fa fa-check"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <b>Success!</b> {{ Session::get('message') }}
    </div>
    @endif 
    @if(Session::has('errorMessage'))
    <div class="alert alert-danger alert-dismissable col-md-10">
        <i class="fa fa-ban"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <b>Alert!</b> {{ Session::get('errorMessage') }}
    </div>
    @endif 
    <div class="row">
        <div class="col-md-10">
            <!-- Custom Tabs -->
            <div class="box box-warning">
                <div class="box-header">
                    


                </div><!-- /.box-header -->
                <div class="box-body">
                    {{ Form::open(array('url'=>'admin/insureadd', 'role'=>'form')) }}

                    <div class="row">
                        <div class="form-group col-md-6">
                            @foreach ($settings_stuff as $sett)

                            {{ Form::hidden('phcf_levy', $sett->phcf_levy, array('placeholder'=>'Enter phcf levies','class'=>'form-control','id'=>'levy')) }}
                            {{ Form::hidden('training_levy', $sett->training_levy, array('placeholder'=>'Enter training levys','class'=>'form-control','id'=>'train_levy')) }}
                            {{ Form::hidden('rate', $sett->rate, array('placeholder'=>'Enter rate','class'=>'form-control','id'=>'rate')) }}
                            {{ Form::hidden('stamp_duty', $sett->stamp_duty, array('placeholder'=>'Enter stamp duty','class'=>'form-control','id'=>'stampDuty')) }}
                            {{ Form::hidden('political_violence', $sett->political_violence, array('placeholder'=>'political violence','class'=>'form-control','id'=>'politicalVio')) }}
                            {{ Form::hidden('riot', $sett->riot, array('placeholder'=>'Enter riot','class'=>'form-control','id'=>'riott')) }}
                            @endforeach
                        </div>
                        <div class="form-group col-md-6">

                            {{ Form::hidden('id', $client_id, array('placeholder'=>'Enter id','class'=>'form-control')) }}
                            {{ Form::hidden('type_of_client', $client_type, array('placeholder'=>'Enter id','class'=>'form-control')) }}
                        </div>
                    </div>
                    <!-- text input -->
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label>Car registration</label>
                            {{ Form::text('car_registration', null, array('placeholder'=>'Enter car registration number e.g KBU 119Y','class'=>'form-control')) }}
                            @if ($errors->has('car_registration')) <div class="text-danger">{{ $errors->first('car_registration') }}</div> @endif
                        </div>
                        <div class="form-group col-md-6">
                            <label>Engine number</label>
                            {{ Form::text('engine_number', null, array('placeholder'=>'Enter engine number','class'=>'form-control')) }}
                            @if ($errors->has('engine_number')) <div class="text-danger">{{ $errors->first('engine_number') }}</div> @endif
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-6">
                            <label>Insurance Start date</label>
                            <div class="input-group">
                                {{ Form::text('insurance_start_date', null, array('placeholder'=>'pick insurance start date','class'=>'form-control pull-left input-group date')) }}

                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>

                            </div><!-- /.input group -->

                            @if ($errors->has('insurance_start_date')) <div class="text-danger">{{ $errors->first('insurance_start_date') }}</div> @endif
                        </div>
                        <div class="form-group col-md-6">
                            <label>Insurance End date</label>
                            <div class="input-group">
                                {{ Form::text('insurance_end_date', null, array('placeholder'=>'pick insurance End date','class'=>'form-control pull-left input-group date')) }}

                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>

                            </div><!-- /.input group -->

                            @if ($errors->has('insurance_end_date')) <div class="text-danger">{{ $errors->first('insurance_end_date') }}</div> @endif
                        </div>
                    </div>
                    <script>
$(document).ready(function () {

    //iterate through each textboxes and add keyup
    //handler to trigger sum event
    $('#check1').click(function () {
        if (this.checked) {
            calculateSum();
        }
    });
    $('#check2').click(function () {
        if (this.checked) {
            calculateSum();
        }
    });

    $("#carValue").each(function () {

        $(this).keyup(function () {
            calculateSum();
        });
    });
    $("#radio").each(function () {

        $(this).keyup(function () {
            calculateSum();
        });
    });
    $("#antitheft").each(function () {

        $(this).keyup(function () {
            calculateSum();
        });
    });
    $("#windscreen").each(function () {

        $(this).keyup(function () {
            calculateSum();
        });
    });


});

function calculateSum() {

    var carvalue = 0;
    var radio_cassette = 0;
    var anti_theft = 0;
    var wind_screen = 0;
    var levys = 0;
    var train_levy = 0;
    var rate = 0;
    var stampDUTY = 0;
    var politicalVIOL = 0;
    var riots = 0;
    var total;
    //iterate through each textboxes and add the values
    $("#carValue").each(function () {

        //add only if the value is number
        if (!isNaN(this.value) && this.value.length != 0) {
            carvalue = parseFloat(this.value);
        } else {
            carvalue = 0;
        }

    });

    $("#radio").each(function () {

        //add only if the value is number
        if (!isNaN(this.value) && this.value.length != 0) {
            radio_cassette = parseFloat(this.value);
        } else {
            radio_cassette = 0;
        }

    });
    $("#antitheft").each(function () {

        //add only if the value is number
        if (!isNaN(this.value) && this.value.length != 0) {
            anti_theft = parseFloat(this.value);
        } else {
            anti_theft = 0;
        }

    });
    $("#windscreen").each(function () {

        //add only if the value is number
        if (!isNaN(this.value) && this.value.length != 0) {
            wind_screen = parseFloat(this.value);
        } else {
            wind_screen = 0;
        }

    });
    $("#levy").each(function () {

        //add only if the value is number
        if (!isNaN(this.value) && this.value.length != 0) {
            levys = parseFloat(this.value);
        } else {
            levys = 0;
        }

    });
    $("#train_levy").each(function () {

        //add only if the value is number
        if (!isNaN(this.value) && this.value.length != 0) {
            train_levy = parseFloat(this.value);
        } else {
            train_levy = 0;
        }

    });
    $("#rate").each(function () {

        //add only if the value is number
        if (!isNaN(this.value) && this.value.length != 0) {
            rate = parseFloat(this.value);
        } else {
            rate = 0;
        }

    });
    $("#stampDuty").each(function () {

        //add only if the value is number
        if (!isNaN(this.value) && this.value.length != 0) {
            stampDUTY = parseFloat(this.value);
        } else {
            stampDUTY = 0;
        }

    });
    $("#politicalVio").each(function () {

        //add only if the value is number
        if (!isNaN(this.value) && this.value.length != 0) {
            politicalVIOL = parseFloat(this.value);
        } else {
            politicalVIOL = 0;
        }

    });
    $("#riott").each(function () {

        //add only if the value is number
        if (!isNaN(this.value) && this.value.length != 0) {
            riots = parseFloat(this.value);
        } else {
            riots = 0;
        }

    });
    var premium = (rate / 100) * carvalue;
    var political_Violencia;
    var riotia;
    if ($('#check1').is(":checked"))
    {
        political_Violencia = premium * (politicalVIOL/100);
    } else {
        political_Violencia = 0;
    }
    if ($('#check2').is(":checked")) {
        riotia = premium * (riots/100);
    } else {
        riotia = 0;
    }

    var phcf_lvytotal = (levys / 100) * premium;
    var trainin_levy = (train_levy / 100) * premium;

    var total_levies = phcf_lvytotal + trainin_levy;

    total = premium + (radio_cassette + anti_theft + wind_screen + political_Violencia + ((stampDUTY / 100) * premium) + riotia + total_levies);

    //.toFixed() method will roundoff the final sum to 2 decimal places

    $("#sum").html(total.toLocaleString());

}
                    </script>

                    <div class="row">
                        <div class="form-group col-md-6">
                            <label>courtesy car</label>
                            {{ Form::text('courtesy_vehicle', null, array('placeholder'=>'Enter courtesy car if any','class'=>'form-control')) }}
                            @if ($errors->has('courtesy_vehicle')) <div class="text-danger">{{ $errors->first('courtesy_vehicle') }}</div> @endif
                        </div>
                        <div class="form-group col-md-6">
                            <label>Manufacture date</label>
                            {{ Form::text('manufacturing_year', null, array('placeholder'=>'Enter manufacture date ','class'=>'form-control')) }}
                            @if ($errors->has('manufacturing_year')) <div class="text-danger">{{ $errors->first('manufacturing_year') }}</div> @endif
                        </div>
                    </div>
                    @foreach ($settings_stuff as $sett)
                    <div class="row">
                        <div class="form-group col-md-6">
                            <div class="checkbox">
                                <label>

                                    <input name="checkbox1" value="{{$sett->political_violence}}" id="check1" type="checkbox"/>
                                    Insure against political violence
                                </label>                                                
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <div class="checkbox">
                                <label>

                                    <input name="checkbox2" value="{{$sett->riot}}" id="check2" type="checkbox"/>
                                    Insure against riots
                                </label>                                                
                            </div>
                        </div>

                    </div>
                    @endforeach
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label>Vehicle value</label>
                            {{ Form::number('vehicle_value', null, array('placeholder'=>'Enter vehicle value','class'=>'form-control txt','id'=>'carValue')) }}
                            @if ($errors->has('vehicle_value')) <div class="text-danger">{{ $errors->first('vehicle_value') }}</div> @endif
                        </div>
                        <div class="form-group col-md-6">
                            <label>Cubic capacity</label>
                            {{ Form::number('cubic_capacity', null, array('placeholder'=>'Enter vehicle cubic capacity','class'=>'form-control')) }}
                            @if ($errors->has('cubic_capacity')) <div class="text-danger">{{ $errors->first('cubic_capacity') }}</div> @endif
                        </div>

                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label>Radio cassette</label>
                            {{ Form::number('radio_cassette', null, array('placeholder'=>'Enter radio cassette value','class'=>'form-control txt','id'=>'radio')) }}
                            @if ($errors->has('radio_cassette')) <div class="text-danger">{{ $errors->first('radio_cassette') }}</div> @endif
                        </div>
                        <div class="form-group col-md-6">
                            <label>Anti theft value</label>
                            {{ Form::number('anti_theft', null, array('placeholder'=>'anti theft value','class'=>'form-control txt','id'=>'antitheft')) }}
                            @if ($errors->has('anti_theft')) <div class="text-danger">{{ $errors->first('anti_theft') }}</div> @endif
                        </div>
                    </div>
                    <div class="row">

                        <div class="form-group col-md-6">
                            <label>Windscreen value</label>
                            {{ Form::number('windscreen', null, array('placeholder'=>'Enter windscreen value','class'=>'form-control txt','id'=>'windscreen')) }}
                            @if ($errors->has('windscreen')) <div class="text-danger">{{ $errors->first('windscreen') }}</div> @endif
                        </div>
                        <div class="form-group col-md-6">
                            <label>Vehicle chasis number</label>
                            {{ Form::text('chasis_number', null, array('placeholder'=>'Enter vehicle chasis number','class'=>'form-control')) }}
                            @if ($errors->has('chasis_number')) <div class="text-danger">{{ $errors->first('chasis_number') }}</div> @endif
                        </div>
                    </div>


                    <div class="row">
                        <div class="form-group col-md-12">
                            <div class="callout callout-info text-yellow">
                                <label>premium :</label>
                                <span class="text-green" id="sum">0</span>

                            </div>
                        </div>
                    </div>

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Create</button>
                    </div>


                    </form>


                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div>
</section>
