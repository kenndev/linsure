<section class="content-header">
    <h1>
        Medical Insurance Module:
        <small>View clients covered with medical insurance</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::to('admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Medical insurance cover</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        @if(Session::has('message')) 
        <div class="alert alert-success alert-dismissable col-md-10">
            <i class="fa fa-check"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <b>Success!</b> {{ Session::get('message') }}
        </div>
        @endif 
        @if(Session::has('errorMessage'))
        <div class="alert alert-danger alert-dismissable col-md-10">
            <i class="fa fa-ban"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <b>Alert!</b> {{ Session::get('errorMessage') }}
        </div>
        @endif 
    </div>
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#tab_1" data-toggle="tab">Medical insurance cover</a></li>
            

            
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="tab_1">

                <!-- general form elements disabled -->
                <div class="box box-warning">
                    <div class="box-header">

                    </div><!-- /.box-header -->

                    <div class="box-body">
                        
                        <div class="box-body table-responsive">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>insurance number</th>
                                        <th>First name</th>
                                        <th>Second name</th>
                                        <th>Email</th>
                                        <th>Phone number</th>                                       
                                        <th>Date from</th>
                                        <th>Date to</th>
                                        <th>Action</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($insurance_medical as $insurance_med)
                                    <tr>
                                        <td>{{$insurance_med->insurance_number}}</td>
                                        <td>{{$insurance_med->first_name}}</td>
                                        <td>{{$insurance_med->second_name}}</td>
                                        <td>{{$insurance_med->email}}</td>
                                        <td>{{$insurance_med->phone_number}}</td>
                                        
                                        <td>{{$insurance_med->date_from}}</td>
                                        <td>{{$insurance_med->date_to}}</td>
                                        <td>

                                            {{ Form::open(array('method' => 'GET', 'action' => array('AdminController@medical_insurance_details',$insurance_med->id))) }}                       
                                            {{ Form::submit('view details', array('class' => 'btn btn-primary btn-xs')) }}
                                            {{ Form::close() }}
                                        </td>
                                        <!--5 for individual clients-->
                                        <td>

                                            {{ Form::open(array('method' => 'PUT', 'action' => array('AdminController@drop_medical_insurance',$insurance_med->id))) }}                       
                                            {{ Form::submit('drop insuarance', array('class' => 'btn btn-danger btn-xs')) }}
                                            {{ Form::close() }}
                                        </td>
                                        
                                    </tr>
                                    @endforeach


                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>insurance number</th>
                                        <th>First name</th>
                                        <th>Second name</th>
                                        <th>Email</th>
                                        <th>Phone number</th>                                       
                                        <th>Date from</th>
                                        <th>Date to</th>
                                        <th>Action</th>
                                        <th>Action</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->

                </div><!-- /.box -->

            </div><!-- /.tab-pane -->
          
        </div><!-- /.tab-content -->
    </div><!-- nav-tabs-custom -->


</section>