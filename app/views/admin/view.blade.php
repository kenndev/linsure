<section class="content-header">
    <h1>
        Clients:
        <small>View clients</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::to('admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">View Clients</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        @if(Session::has('message')) 
        <div class="alert alert-success alert-dismissable col-md-10">
            <i class="fa fa-check"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <b>Success!</b> {{ Session::get('message') }}
        </div>
        @endif 
        @if(Session::has('errorMessage'))
        <div class="alert alert-danger alert-dismissable col-md-10">
            <i class="fa fa-ban"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <b>Alert!</b> {{ Session::get('errorMessage') }}
        </div>
        @endif 
    </div>
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#tab_1" data-toggle="tab">Individual Client</a></li>
            <li><a href="#tab_2" data-toggle="tab">Business Client</a></li>

            <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="tab_1">

                <!-- general form elements disabled -->
                <div class="box box-warning">
                    <div class="box-header">

                    </div><!-- /.box-header -->

                    <div class="box-body">
                        <div class="box-header">
                            <h3 class="box-title">View individual clients</h3>                                    
                        </div><!-- /.box-header -->
                        <div class="box-body table-responsive">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>First name</th>
                                        <th>Second name</th>
                                        <th>Email</th>
                                        <th>Phone number</th>
                                        <th>Postal address</th>
                                        <th>Location</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($client_individuals as $clients_indi)
                                    <tr>
                                        <td>{{$clients_indi->first_name}}</td>
                                        <td>{{$clients_indi->second_name}}</td>
                                        <td>{{$clients_indi->email}}</td>
                                        <td>{{$clients_indi->phone_number}}</td>
                                        <td>{{$clients_indi->postal_address}}</td>
                                        <td>{{$clients_indi->location}}</td>
                                        <!--5 for individual clients-->
                                        <td>

                                            {{ Form::open(array('method' => 'PUT', 'action' => array('AdminController@drop_Client',$clients_indi->id,5))) }}                       
                                            {{ Form::submit('drop client', array('class' => 'btn btn-danger btn-xs')) }}
                                            {{ Form::close() }}
                                        </td>
                                    </tr>
                                    @endforeach


                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>First name</th>
                                        <th>Second name</th>
                                        <th>Email</th>
                                        <th>Phone number</th>
                                        <th>Postal address</th>
                                        <th>Location</th>
                                        <th>Action</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->

                </div><!-- /.box -->

            </div><!-- /.tab-pane -->
            <div class="tab-pane" id="tab_2">
                <div class="box box-warning">
                    <div class="box-header">

                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div class="box-header">
                            <h3 class="box-title">View Business clients</h3>                                    
                        </div><!-- /.box-header -->
                        <div class="box-body table-responsive">
                            <table id="example3" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Business name</th>
                                        <th>Contact person</th>
                                        <th>Business email</th>
                                        <th>Business phone number</th>
                                        <th>postal_address</th>
                                        <th>Location</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <!--10 for individual clients-->
                                    @foreach ($client_business as $clients_bs)
                                    <tr>
                                        <td>{{$clients_bs->business_name}}</td>
                                        <td>{{$clients_bs->	contact_person}}</td>
                                        <td>{{$clients_bs->business_email}}</td>
                                        <td>{{$clients_bs->business_phone}}</td>
                                        <td>{{$clients_bs->postal_address}}</td>
                                        <td>{{$clients_bs->location}}</td>
                                        <td>

                                            {{ Form::open(array('method' => 'PUT', 'action' => array('AdminController@drop_Client',$clients_bs->id,10 ))) }}                       
                                            {{ Form::submit('drop client', array('class' => 'btn btn-danger btn-xs')) }}
                                            {{ Form::close() }}
                                        </td>
                                    </tr>
                                    @endforeach


                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>Business name</th>
                                        <th>Contact person</th>
                                        <th>Business email</th>
                                        <th>Business phone number</th>
                                        <th>postal_address</th>
                                        <th>Location</th>
                                        <th>Action</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </div><!-- /.box -->
            </div><!-- /.tab-pane -->
        </div><!-- /.tab-content -->
    </div><!-- nav-tabs-custom -->


</section>