<section class="content-header">
    <h1>
        View theft claims:
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::to('admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">View theft claims</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        @if(Session::has('message')) 
        <div class="alert alert-success alert-dismissable col-md-10">
            <i class="fa fa-check"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <b>Success!</b> {{ Session::get('message') }}
        </div>
        @endif 
        @if(Session::has('errorMessage'))
        <div class="alert alert-danger alert-dismissable col-md-10">
            <i class="fa fa-ban"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <b>Alert!</b> {{ Session::get('errorMessage') }}
        </div>
        @endif 
    </div>
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#tab_1" data-toggle="tab">Individual Client</a></li>
            <li><a href="#tab_2" data-toggle="tab">Business Client</a></li>

            <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="tab_1">

                <!-- general form elements disabled -->
                <div class="box box-warning">
                    <div class="box-header">

                    </div><!-- /.box-header -->

                    <div class="box-body">
                        <div class="box-header">
                            <h3 class="box-title">Theft claims</h3>                                    
                        </div><!-- /.box-header -->
                        <div class="box-body table-responsive">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>First name</th>
                                        <th>Second name</th>
                                        <th>Location of accident</th>
                                        <th>Police station</th>                                     
                                        <th>Date reported</th>
                                        <th>Date of incident</th>
                                        <th>View details</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($partial_damage_indi as $partial_damage_indi_view)
                                    <tr>
                                        <td>{{$partial_damage_indi_view->first_name}}</td>
                                        <td>{{$partial_damage_indi_view->second_name}}</td>
                                        <td>{{$partial_damage_indi_view->location}}</td>
                                        <td>{{$partial_damage_indi_view->police_station}}</td>    
                                        <td>{{$partial_damage_indi_view->date_reported}}</td>
                                        <td>{{$partial_damage_indi_view->date_incident}}</td>
                                        <!--5 for individual clients-->
                                        <td>

                                            {{ Form::open(array('method' => 'get', 'action' => array('AdminController@view_theft_claim_details',$partial_damage_indi_view->id,5))) }}                       
                                            {{ Form::submit('more details', array('class' => 'btn btn-info btn-xs')) }}
                                            {{ Form::close() }}
                                        </td>
                                        <td>

                                            {{ Form::open(array('method' => 'PUT', 'action' => array('AdminController@policyadd',$partial_damage_indi_view->id,5))) }}                       
                                            {{ Form::submit('drop claim', array('class' => 'btn btn-danger btn-xs')) }}
                                            {{ Form::close() }}
                                        </td>
                                    </tr>
                                    @endforeach


                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>First name</th>
                                        <th>Second name</th>
                                        <th>Location of accident</th>
                                        <th>Police station</th>                                      
                                        <th>Date reported</th>
                                        <th>Date of incident</th>
                                        <th>View details</th>
                                        <th>Action</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->

                </div><!-- /.box -->

            </div><!-- /.tab-pane -->
            <div class="tab-pane" id="tab_2">
                <div class="box box-warning">
                    <div class="box-header">

                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div class="box-header">
                            <h3 class="box-title">Business clients Not insured </h3>                                    
                        </div><!-- /.box-header -->
                        <div class="box-body table-responsive">
                            <table id="example3" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Business name</th>                                       
                                        <th>Location of accident</th>
                                        <th>Police station</th>                                      
                                        <th>Date reported</th>
                                        <th>Date of incident</th>
                                        <th>View details</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <!--10 for individual clients-->
                                    @foreach ($partial_damage_bs as $partial_damage_bs_view)
                                    <tr>
                                        <td>{{$partial_damage_bs_view->business_name}}</td>
                                        
                                        <td>{{$partial_damage_bs_view->location_of_accident}}</td>
                                        <td>{{$partial_damage_bs_view->police_station}}</td>    
                                        <td>{{$partial_damage_bs_view->date_reported}}</td>
                                        <td>{{$partial_damage_bs_view->date_incident}}</td>
                                        <!--5 for individual clients-->
                                        <td>

                                            {{ Form::open(array('method' => 'get', 'action' => array('AdminController@partial_damage_details',$partial_damage_bs_view->id,10))) }}                       
                                            {{ Form::submit('more details', array('class' => 'btn btn-info btn-xs')) }}
                                            {{ Form::close() }}
                                        </td>
                                        <td>

                                            {{ Form::open(array('method' => 'PUT', 'action' => array('AdminController@policyadd',$partial_damage_bs_view->id,10))) }}                       
                                            {{ Form::submit('drop claim', array('class' => 'btn btn-danger btn-xs')) }}
                                            {{ Form::close() }}
                                        </td>
                                    </tr>
                                    @endforeach


                                </tbody>
                                <tfoot>
                                    <tr>
                                        <tr>
                                        <th>Business name</th>                                       
                                        <th>Location of accident</th>
                                        <th>Police station</th>                                      
                                        <th>Date reported</th>
                                        <th>Date of incident</th>
                                         <th>View details</th>
                                        <th>Action</th>
                                    </tr>
                                    </tr>
                                </tfoot>
                            </table>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </div><!-- /.box -->
            </div><!-- /.tab-pane -->
        </div><!-- /.tab-content -->
    </div><!-- nav-tabs-custom -->


</section>