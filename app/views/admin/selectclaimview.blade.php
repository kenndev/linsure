
<section class="content-header">
    <h1>
        Choose a claim to view

    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::to('admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">select a claim</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <!-- Small boxes (Stat box) -->

    <div class="row">

        
        <div class="col-lg-3 col-xs-6 ">

            <!-- small box -->
            <div class="small-box bg-purple">
                <a href="{{ URL::to('admin/partialDamageView') }}" class="btn btn-lg">
                    <div class="inner">
                        <h3>
                            <sup style="font-size: 20px; color: #FFFFFF">Partial damage</sup>
                        </h3>
                        <p>
                            <sup style="font-size: 20px; color: #FFFFFF">Claim</sup>
                        </p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-anchor"></i>
                    </div>
                    <a href="#" class="small-box-footer">
                        Linsure insurance management system <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </a>
            </div>

        </div><!-- ./col -->

        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-blue">
                <a href="{{ URL::to('admin/theftclaimView') }}" class="btn btn-lg">
                    <div class="inner">
                        <h3>
                            <sup style="font-size: 20px; color: #FFFFFF">Theft Claim</sup>
                        </h3>
                        <p>
                            <sup style="font-size: 20px; color: #FFFFFF">.</sup>
                        </p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-code-fork"></i>
                    </div>
                    <a href="#" class="small-box-footer">
                        Linsure insurance management system <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </a>
            </div>
        </div><!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-orange">
                <a href="{{ URL::to('admin/windscreenclaimView') }}" class="btn btn-lg">
                    <div class="inner">
                        <h3>
                            <sup style="font-size: 20px; color: #FFFFFF">Windscreen replace</sup>
                        </h3>
                        <p>
                            <sup style="font-size: 20px; color: #FFFFFF">cover</sup>
                        </p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-random"></i>
                    </div>
                    <a href="#" class="small-box-footer">
                        Linsure insurance management system <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </a>
            </div>
        </div><!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-maroon">
                <a href="{{ URL::to('admin/radioclaimView') }}" class="btn btn-lg">
                    <div class="inner">
                        <h3>
                            <sup style="font-size: 20px; color: #FFFFFF">Radio cassette</sup>
                        </h3>
                        <p>
                            <sup style="font-size: 20px; color: #FFFFFF">cover</sup>
                        </p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-leaf"></i>
                    </div>
                    <a href="#" class="small-box-footer">
                        Linsure insurance management system <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </a>
            </div>
        </div><!-- ./col -->
    </div><!-- /.row -->



</section><!-- /.content -->

