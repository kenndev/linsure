
<section class="content-header">
    <h1>
        Credit note :
        <small></small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::to('admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">credit note</li>
    </ol>
</section>
<!-- Main content -->

<section class="content">                    
    <!-- title row -->


    <div class="row">
        <div class="col-md-8">
            <!-- Custom Tabs -->
            <div class="box box-solid">

                <div class="box-body clearfix">

                    <div id="div_print">
                        <div class="row">
                            <div class="col-xs-12">
                                <h2 class="page-header" style="margin: 10px 0 20px 0; height: 50px;">
                                    <i class="fa fa-globe"></i> Linsure Insurance Agency

                                    <small class="pull-right"> {{ Form::open(array('method' => 'get', 'action' => array('AdminController@creditnote_med_pdf', $credit_id))) }}                       
                                        {{ Form::submit('Generate PDF', array('class' => 'btn btn-primary')) }}
                                        {{ Form::close() }}</small>
                                </h2>

                            </div><!-- /.col -->

                        </div>

                    </div>
                    <div class="row invoice-info">
                        <div class="col-sm-4 invoice-col text-center" style="margin: 0 auto; float: none;">

                            <img src="{{ asset('packages/components/img/real-logo.gif') }}" class="img-thumbnail" alt="User Image" />
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                    <div class="row" style="margin-top: 2%">
                        <div class="col-xs-12 table-responsive">
                            <table class="table table-striped text-center">

                                <tbody>
                                    <tr style="">
                                        <th style="height: 50px;border-top-color: black;border-top-style: solid;border-top-width: 3px;border-bottom-color:black;border-bottom-style: solid;border-bottom-width: 3px; ">RETURN PREMIUM DETAILS</th>
                                    </tr>


                                </tbody>
                            </table>
                            @foreach ($insurance as $ins)
                            <div class="row col-sm-12" style="margin-bottom: 2%;">
                                <div class="col-sm-3">
                                    Policy Number
                                </div>
                                <div class="col-sm-6">
                                    {{$ins->insurance_number}}

                                </div>
                            </div>
                            <div class="row col-sm-12" style="margin-bottom: 2%;">
                                <div class="col-sm-3">
                                    MTA NUMBER
                                </div>
                                <div class="col-sm-6">
                                    {{$ins->insurance_number}}/6
                                </div>
                            </div>
                            <div class="row col-sm-12" style="margin-bottom: 2%">
                                <div class="col-sm-3">
                                    TRANSACTION REFERENCE
                                </div>
                                <div class="col-sm-6">
                                    SEC0000787943
                                </div>
                            </div>
                            <div class="row col-sm-12" style="margin-bottom: 2%;">
                                <div class="col-sm-3">
                                    PRODUCT
                                </div>
                                <div class="col-sm-6">
                                    Medical Insurance
                                </div>
                            </div>
                            <div class="row col-sm-12" style="margin-bottom: 2%;">
                                <div class="col-sm-3">
                                    INSURED
                                </div>
                                <div class="col-sm-6">
                                    {{$ins->first_name ," " , $ins->second_name}}
                                    
                                </div>
                            </div>
                            <div class="row col-sm-12" style="margin-bottom: 2%;">
                                <div class="col-sm-3">
                                    AGENCY
                                </div>
                                <div class="col-sm-6">
                                    Linsure Insurance Agency
                                </div>
                            </div>
                            <div class="row col-sm-12" style="margin-bottom: 2%;">
                                <div class="col-sm-3">
                                    RENEWAL DATE
                                </div>
                                <div class="col-sm-6">
                                    {{$ins->date_to}}
                                </div>
                            </div>
                            <div class="row col-sm-12" style="margin-bottom: 3%;">
                                <div class="col-sm-3 "style="text-decoration: underline;">
                                    CHANGE DETAILS
                                </div>

                            </div>
                            @foreach ($credit_note_details as $creditNote)
                            <div class="row col-sm-12" style="margin-bottom: 2%;">
                                It is hereby declared and agreed that with effect from {{$creditNote->date_cancelled}} the insurance cover by this

                                policy is deemed to be cancelled.

                            </div>
                            
                        </div><!-- /.col -->
                        
                        <div class="col-xs-12 table-responsive">



                            <table class="table table-responsive">

                                <tr>
                                    <td rowspan="2" style="border-left: 1px solid #ddd;">Premium(shs)</td>
                                    <td style="border-left: 1px solid #ddd;">Return</td>
                                    <td style="border-left: 1px solid #ddd;">-{{number_format($ins->premium,2)}}</td>
                                    <td style="border-left: 1px solid #ddd;border-right: 1px solid #ddd">future</td>
                                </tr>
                                <tr>
                                    <td style="border: none; border-left: 1px solid #ddd;"></td>
                                    <td style="border: none; border-left: 1px solid #ddd;"></td>
                                    <td style="border-left: 1px solid #ddd;border-right: 1px solid #ddd">{{number_format(0,2)}}</td>
                                </tr>
                                <tr>
                                    <td rowspan="3" style="border-left: 1px solid #ddd;"></td>
                                    <td style="border-left: 1px solid #ddd;">Training levy</td>
                                    <td style="border-left: 1px solid #ddd;">-{{number_format($ins->traininglevy,2)}}</td>
                                    <td style="border-left: 1px solid #ddd;border-right: 1px solid #ddd">0.00</td>
                                </tr>
                                <tr>
                                    <td style="border-left: 1px solid #ddd;">php</td>
                                    <td style="border-left: 1px solid #ddd;">-{{number_format($ins->phcflevy,2)}}</td> 
                                    <td style="border-left: 1px solid #ddd;border-right: 1px solid #ddd">0.00</td>
                                </tr>
                                <tr>
                                    <td style="border-left: 1px solid #ddd;">Total</td>
                                    <td style="border-left: 1px solid #ddd;">-{{number_format($ins->total_premium,2)}}</td>
                                    <td style="border-left: 1px solid #ddd;border-right: 1px solid #ddd">0.00</td>
                                </tr>
                                <tr>
                                    <td style="border-left: 1px solid #ddd;">
                                        MTA DATE
                                    </td>
                                    <td style="border-left: 1px solid #ddd;">
                                        {{$ins->date_from}}
                                    </td>
                                    <td >

                                    </td>
                                    <td style="border-right: 1px solid #ddd">

                                    </td>
                                </tr>
                                <tr>
                                    <td style="border-left: 1px solid #ddd;">
                                        AMENDED BY
                                    </td>
                                    <td style="border-left: 1px solid #ddd;">
                                        {{$creditNote->amended_by}}
                                    </td>
                                    @endforeach
                                    <td>

                                    </td>
                                    <td style="border-right: 1px solid #ddd"> 

                                    </td>
                                </tr>
                                <tr>
                                    <td style="border-left: 1px solid #ddd;">
                                        Signed BY :
                                    </td>
                                    <td style="border-left: 1px solid #ddd;">
                                        ____________________
                                    </td>
                                    <td>

                                    </td>
                                    <td style="border-right: 1px solid #ddd">

                                    </td>
                                </tr>
                                <tr>
                                    <td style="border-left: 1px solid #ddd;border-bottom: 1px solid #ddd">

                                    </td>
                                    <td style="border-left: 1px solid #ddd;border-bottom: 1px solid #ddd">
                                        FOR THE COMPANY
                                    </td>
                                    <td style="border-bottom: 1px solid #ddd">

                                    </td>
                                    <td style="border-bottom: 1px solid #ddd;border-right: 1px solid #ddd">

                                    </td>
                                </tr>
                            </table>

                        </div>
                        @endforeach
                    </div><!-- /.row -->
                    <div class="row">

                    </div><!-- close row table -->
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div>
</section><!-- /.content -->

