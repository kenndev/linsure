
<section class="content-header">
    <h1>
        Credit note :
        <small>credit note doesnt exist</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::to('admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">credit note</li>
    </ol>
</section>
<!-- Main content -->

<section class="content">                    
    <!-- title row -->
    @if(Session::has('message')) 
    <div class="alert alert-success alert-dismissable col-md-10">
        <i class="fa fa-check"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <b>Success!</b> {{ Session::get('message') }}
    </div>
    @endif 
    @if(Session::has('errorMessage'))
    <div class="alert alert-danger alert-dismissable col-md-10">
        <i class="fa fa-ban"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <b>Alert!</b></br> {{ Session::get('errorMessage') }}
    </div>
    @endif 
</div><!-- /.col -->
</div>
</section><!-- /.content -->
