<section class="content-header">
    <h1>
        Invoice :
        @if($client_type == 5)
        @foreach($name as $client_name)
        <small>Invoice to {{$client_name->first_name}} {{$client_name->second_name}}</small>
        @endforeach
        @elseif($client_type == 10)
        @foreach($name as $client_name)
        <small>Invoice to {{$client_name->business_name}}</small>
        @endforeach
        @endif

    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::to('admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">invoice client</li>
    </ol>
</section>

<section class="content invoice">
    <div class="row">
        <div class="col-xs-6">
            {{ Form::hidden('insurance_id', $insurance_id, array('class'=>'form-control','id'=>'insurance_id')) }}

            <a class="btn btn-block btn-success" data-toggle="modal" data-target="#compose-modal"><i class="fa fa-pencil-square"></i> Description</a>
        </div>
    </div>

</section>



<section class="content invoice">                    

    <div class=" ">
        <div class="row" class="page-header">
            <div class="col-xs-12">
                <h2 class="page-header">

                    <i class="fa fa-globe"></i> Linsure insurance agency.
                    <small class="pull-right">Date: {{date('Y-m-d')}}</small>
                </h2>
            </div>

        </div>
        <div class="row">
            <div class="col-xs-12 text-right">

                <h2><small>Invoice #001</small></h2>
            </div>
            <div class="col-xs-5">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        From: <strong>Real insurance.</strong><br>
                    </div>
                    <div class="panel-body">
                        <address>


                            Royal Ngao House, Hospital Road<br>
                            P O Box 40001 00100 Nairobi <br>
                            Tel. 2712620 Fax: 2717888/2713831<br>
                            E-mail: general@realinsurance.co.ke<br><br>
                            <strong>AGENT:</strong> Linsure Insurance Agency</br></br>
                        </address>
                    </div>
                </div>
            </div>
            <div class="col-xs-5 col-xs-offset-2 text-right">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        @if($client_type == 5)
                        @foreach($name as $client_name)
                        To : <strong>{{$client_name->first_name}} {{$client_name->second_name}}</strong></br>
                        @endforeach
                        @elseif($client_type == 10)
                        @foreach($name as $client_name)
                        <strong>{{$client_name->business_name}}</strong> </br>
                        @endforeach
                        @endif
                    </div>
                    <div class="panel-body">
                        <address>



                            @if($client_type == 5)
                            @foreach($name as $client_name)

                            P.O BOX {{$client_name->postal_address}} </br>
                            {{$client_name->location}}</br>
                            KENYA</br></br>


                            @endforeach
                            @elseif($client_type == 10)
                            @foreach($name as $client_name)

                            P.O BOX {{$client_name->postal_address}} </br>
                            {{$client_name->location}}</br>
                            KENYA</br></br>


                            @endforeach
                            @endif
                        </address>
                    </div>
                </div>
            </div>
        </div>
        <!-- / end client details section -->
        @foreach($name as $client_name)
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>
            <h4>Service</h4>
            </th>
            <th>
            <h4>Description</h4>
            </th>
            <th>
            <h4>Registration no</h4>
            </th>
            <th>
            <h4>Value</h4>
            </th>
            <th>
            <h4>Insurance cover</h4>
            </th>
            <th>
            <h4>Premium</h4>
            </th>
            <th>
            <h4>Total</h4>
            </th>
            </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Motor private</td>
                     @foreach ($Description_invoice as $invoice_desc)
                    <td>{{$invoice_desc->description}}</td>
                    @endforeach
                    <td>{{$client_name->car_registration}}</td>
                    <td>{{number_format($client_name->vehicle_value, 2)}}</td>
                    <td>Comprehensive</td>
                    <td>{{number_format($client_name->premium, 2)}}</td>
                    <td>{{number_format($client_name->total_premium, 2)}}</td>
                </tr>

            </tbody>
        </table>

        <div class="row text-right">
            <div class="col-xs-5 col-xs-offset-5">
                <p>
                    <strong>
                        Total Premium : <br>
                        Training levy : <br>
                        Stamp duty : <br>
                        Policy holders fund : <br>
                        Net amount due from you : <br>
                    </strong>
                </p>
            </div>
            <div class="col-xs-2">
                @foreach ($settings as $set)
                <strong>
                    {{number_format($client_name->premium, 2)}}<br>
                    {{number_format($client_name->traininglevy,2)}}<br>
                    {{number_format($set->stamp_duty,2)}}<br>
                    {{number_format($client_name->phcflevy,2)}} <br>
                    {{number_format($client_name->total_premium, 2) }} <br>
                </strong>
                @endforeach
            </div>
        </div>
        @endforeach
        <div class="row">
            <div class="col-xs-6">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        details
                    </div>
                    <div class="panel-body">
                        <p>DR prepared by :</p>
                        <p>Date :  </p>
                        <p>DR passed by : </p>

                    </div>
                </div>
            </div>

            <div class="col-xs-6 pull-right">
                <div class="span7">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            Details
                        </div>
                        <div class="panel-body">
                            <p>
                                Manager : </p>
                            <p>Signature :  </p>
                            <p>Date : 
                            </p>

                        </div>
                    </div>
                </div>
            </div>

        </div>
        <p> {{ DNS2D::getBarcodeHTML("4445645656", "QRCODE",5,5); }} </p>


    </div>
</section>



<div class="modal fade" id="compose-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><i class="fa fa-envelope-o"></i> Description </h4>
            </div>
            {{ Form::open(array('url'=>'admin/updateinvoice', 'role'=>'form','files' => true)) }}
            <div class="modal-body">

                <div class="form-group">
                    @foreach ($Description_invoice as $invoice_desc)
                    {{ Form::textarea('invoice', $invoice_desc->description, array('placeholder'=>'invoice description','class'=>'form-control','id'=>'invoice','style'=>'height: 120px')) }}
                    {{ Form::hidden('id_update', $invoice_desc->id, array('class'=>'form-control')) }}
                    @endforeach
                </div>


            </div>
            <div class="modal-footer clearfix">

                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> cancel</button>

                <button type="submit" class="btn btn-primary pull-left"><i class="fa fa-envelope"></i> post</button>
            </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


