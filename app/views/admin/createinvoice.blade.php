<section class="content-header text-blue">
    <h4>
        Vehicle Invoice:
        <small>Choose or search client to invoice</small>
    </h4>
    <ol class="breadcrumb">
        <li><a href="{{ URL::to('admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Invoice Clients</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        @if(Session::has('message')) 
        <div class="alert alert-success alert-dismissable col-md-11">
            <i class="fa fa-check"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <b>Success!</b> {{ Session::get('message') }}
        </div>
        @endif 
        @if(Session::has('errorMessage'))
        <div class="alert alert-danger alert-dismissable col-md-11">
            <i class="fa fa-ban"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <b>Alert!</b> {{ Session::get('errorMessage') }}
        </div>
        @endif 
    </div>

    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#tab_1" data-toggle="tab">Individual Client</a></li>
            <li><a href="#tab_2" data-toggle="tab">Business Client</a></li>

            <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="tab_1">

                <!-- general form elements disabled -->
                <div class="box box-warning">


                    <div class="box-body">


                        <div class="box-body table-responsive">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>First name</th>
                                        <th>Second name</th>
                                        <th>Email</th>
                                        <th>Phone number</th>

                                        <th>Location</th>
                                        <th>Action</th>
                                        <th>Action</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>

                                <tbody class="text-light-blue">
                                    @foreach ($client_individuals as $clients_indi)

                                    <tr>
                                        <td>{{$clients_indi->first_name}}</td>
                                        <td>{{$clients_indi->second_name}}</td>
                                        <td>{{$clients_indi->email}}</td>
                                        <td>{{$clients_indi->phone_number}}</td>

                                        <td>{{$clients_indi->location}}</td>
                                        <!--5 for individual clients-->
                                        <td>

                                            {{ Form::open(array('method' => 'PUT', 'action' => array('AdminController@invoice_template',$clients_indi->id,5,$clients_indi->insurance_id))) }}                       
                                            {{ Form::submit('create invoice', array('class' => 'btn btn-success btn-xs')) }}
                                            {{ Form::close() }}
                                        </td>
                                        <td>

                                            {{ Form::open(array('method' => 'PUT', 'action' => array('AdminController@view_invoice_template',$clients_indi->id,5,$clients_indi->insurance_id))) }}                       
                                            {{ Form::submit('view invoice', array('class' => 'btn btn-xs bg-maroon')) }}
                                            {{ Form::close() }}
                                        </td>
                                        <td>

                                            {{ Form::open(array('method' => 'PUT', 'action' => array('AdminController@invoice_edit',$clients_indi->insurance_id))) }}                       
                                            {{ Form::submit('edit invoice', array('class' => 'btn btn-xs bg-aqua')) }}
                                            {{ Form::close() }}
                                        </td>
                                    </tr>
                                    @endforeach


                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>First name</th>
                                        <th>Second name</th>
                                        <th>Email</th>
                                        <th>Phone number</th>

                                        <th>Location</th>
                                        <th>Action</th>
                                        <th>Action</th>
                                        <th>Action</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->

                </div><!-- /.box -->

            </div><!-- /.tab-pane -->
            <div class="tab-pane " id="tab_2">
                <div class="box box-warning">
                    <div class="box-header bg-">

                    </div><!-- /.box-header -->
                    <div class="box-body">

                        <div class="box-body table-responsive">
                            <table id="example3" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Business name</th>
                                        <th>Contact person</th>
                                        <th>Business email</th>
                                        <th>Business phone number</th>

                                        <th>Location</th>
                                        <th>Action</th>
                                        <th>Action</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody class="text-light-blue">
                                    <!--10 for individual clients-->
                                    @foreach ($client_business as $clients_bs)
                                    <tr>
                                        <td>{{$clients_bs->business_name}}</td>
                                        <td>{{$clients_bs->contact_person}}</td>
                                        <td>{{$clients_bs->business_email}}</td>
                                        <td>{{$clients_bs->business_phone}}</td>

                                        <td>{{$clients_bs->location}}</td>
                                         <td>

                                            {{ Form::open(array('method' => 'PUT', 'action' => array('AdminController@invoice_template',$clients_bs->id,10,$clients_bs->insurance_id))) }}                       
                                            {{ Form::submit('create invoice', array('class' => 'btn btn-success btn-xs')) }}
                                            {{ Form::close() }}
                                        </td>
                                        <td>

                                            {{ Form::open(array('method' => 'PUT', 'action' => array('AdminController@view_invoice_template',$clients_bs->id,10,$clients_bs->insurance_id))) }}                       
                                            {{ Form::submit('view invoice', array('class' => 'btn btn-xs bg-maroon')) }}
                                            {{ Form::close() }}
                                        </td>
                                        <td>

                                            {{ Form::open(array('method' => 'PUT', 'action' => array('AdminController@invoice_edit',$clients_bs->insurance_id))) }}                       
                                            {{ Form::submit('edit invoice', array('class' => 'btn btn-xs bg-aqua')) }}
                                            {{ Form::close() }}
                                        </td>
                                    </tr>
                                    @endforeach


                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>Business name</th>
                                        <th>Contact person</th>
                                        <th>Business email</th>
                                        <th>Business phone number</th>

                                        <th>Location</th>
                                        <th>Action</th>
                                        <th>Action</th>
                                        <th>Action</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </div><!-- /.box -->
            </div><!-- /.tab-pane -->
        </div><!-- /.tab-content -->
    </div><!-- nav-tabs-custom -->


</section>
