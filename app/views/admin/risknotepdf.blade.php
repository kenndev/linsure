<!DOCTYPE html>
<html>

    <head>
        <title>Linsure insurance &reg; </title>
        <style>
            body{
                font-size: 14px;
                line-height: 20px;
            }
            .wrapper_main{
                margin: 0 auto;
                width: 100%;
            }
            .top-div{
                padding: 10px;
                border-bottom: 1px solid #eee;
            }
            .logo{
                margin: 0 auto
            }
            .logo p{
                text-align: center;
                font-size: 17px;
            }

            .tabless{
                padding: 10px;
            }

            table {
                width:100%;

                clear: both;
            }
            table, th, td {
                border-collapse: collapse;            
            }
            th, td {
                padding: 5px;
                text-align: left;
                outline: none;
            }
            table#t01 tr:nth-child(even) {
                background-color: #eee;
            }
            table#t01 tr:nth-child(odd) {
                background-color:#fff;
            }
            table#t01 th  {

                color: black;
            }
        </style>
       
    </head>
    
    <body>
        <div class="wrapper_main">
            @foreach ($insurance as $insdetails)
            <div id="top" class="top-div">
                <div class="logo ">
                    <p><strong>Linsure Insurance Agency</strong></p> 
                    <p>Vehicle insurance risk note </p>
                    <p>{{$insdetails->first_name}} {{$insdetails->second_name}}</p>
                    <p>{{date('Y-m-d')}}</p> 
                </div>
            </div>
            <div class="tabless">
                <table id="t01">
                    <tr>
                        <th>Car registration</th><td>{{$insdetails->car_registration}}</td>
                    </tr>
                    <tr>
                        <th>Engine number</th> <td>{{$insdetails->engine_number}}</td>    
                    </tr>
                    <tr>
                        <th>Insurance start date</th><td>{{$insdetails->insurance_start_date}}</td>
                    </tr>
                    <tr>
                        <th>Insurance end date</th><td>{{$insdetails->insurance_end_date}}</td>
                    </tr>
                    <tr>
                        <th>Courtesy vehicle</th><td>{{$insdetails->courtesy_vehicle}}</td>
                    </tr>
                    <tr>
                        <th>Manufacturing year</th><td>{{$insdetails->manufacturing_year}}</td>
                    </tr>
                    <tr>
                        <th>Cubic capacity</th><td>{{$insdetails->cubic_capacity}}</td>
                    </tr>
                    <tr>
                        <th>Vehicle value</th><td>{{$insdetails->vehicle_value}}</td>
                    </tr>
                    <tr>
                        <th>Radio cassette</th><td>{{$insdetails->radio_cassette}}</td>
                    </tr>
                    <tr>
                        <th>Anti theft</th><td>{{$insdetails->anti_theft}}</td>
                    </tr>
                    <tr>
                        <th>Windscreen</th><td>{{$insdetails->windscreen}}</td>
                    </tr>
                    <tr>
                        <th>Chasis number</th><td>{{$insdetails->chasis_number}}</td>
                    </tr>
                    <tr>
                        <th>Premium</th><td>{{$insdetails->total_premium}}</td>
                    </tr>

                </table>
            </div>
            @endforeach
        </div>
    </body>
</html>
