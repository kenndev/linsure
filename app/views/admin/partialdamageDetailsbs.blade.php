
<section class="content-header">
    <h1>
        Partial Damage Report
        <small>Partial damage report</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::to('admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">view partial report</li>
    </ol>
</section>
<!-- Main content -->

<section class="content invoice">                    
    <!-- title row -->
    <div id="div_print">
        <div class="row">
            <div class="col-xs-12">
                <h2 class="page-header" style="margin: 10px 0 20px 0; height: 50px;">
                    <i class="fa fa-globe"></i> Linsure Insurance Agency

<!--                    <small class="pull-right"> {{ Form::open(array('method' => 'get', 'action' => array('AdminController@risknote', $risk_id,$type_client))) }}                       
                        {{ Form::submit('Generate PDF', array('class' => 'btn btn-primary')) }}
                        {{ Form::close() }}
                    </small>-->
                </h2>

            </div><!-- /.col -->

        </div>
        <!-- info row -->
        @foreach ($partialdamage as $partialDamages)
        <div class="row invoice-info">
            <div class="col-sm-4 invoice-col text-center" style="margin: 0 auto; float: none;">

                <address>
                    <strong>Linsure Insurance Agency</strong><br>
                    Partial damage details<br>
                    {{$partialDamages->business_name}}<br>
                    {{$partialDamages->business_phone}}<br>
                    {{date('Y-m-d')}}<br/>

                </address>
            </div><!-- /.col -->


        </div><!-- /.row -->

        <!-- Table row -->
        <div class="row">
            <div class="col-xs-12 table-responsive">
                <table class="table table-striped">

                    <tbody>
                        <tr>
                            <th>Location of accident</th><td>{{$partialDamages->location_of_accident}}</td>
                        </tr>
                        <tr>
                            <th>Police station</th> <td>{{$partialDamages->police_station}}</td>   
                        </tr>
                        <tr>
                            <th>Time</th><td>{{$partialDamages->time}}</td>
                        </tr>
                        <tr>
                            <th>Driving experience</th><td>{{$partialDamages->driving_experience}}</td>
                        </tr>
                        <tr>
                            <th>Date reported</th><td>{{$partialDamages->date_reported}}</td>
                        </tr>
                        <tr>
                            <th>Date of incident</th><td>{{$partialDamages->date_incident}}</td>
                        </tr>

                    </tbody>
                </table>                            
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div>
    @endforeach
    <!-- this row will not appear when printing -->


<!--    <div class="row no-print">
        <div class="col-xs-12">

            {{ Form::open(array('method' => 'get', 'action' => array('AdminController@risknote', $risk_id,$type_client))) }}                       
            {{ Form::submit('Generate PDF', array('class' => 'btn btn-primary pull-left')) }}
            {{ Form::close() }}
        </div>
    </div>-->
</section><!-- /.content -->

