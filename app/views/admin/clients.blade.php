<section class="content-header">
    <h1>
        Clients:
        <small>Create new clients</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::to('admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Create new Clients</li>
    </ol>
</section>

<section class="content">
    @if(Session::has('message')) 
    <div class="alert alert-success alert-dismissable col-md-10">
        <i class="fa fa-check"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <b>Success!</b> {{ Session::get('message') }}
    </div>
    @endif 
    @if(Session::has('errorMessage'))
    <div class="alert alert-danger alert-dismissable col-md-10">
        <i class="fa fa-ban"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <b>Alert!</b> {{ Session::get('errorMessage') }}
    </div>
    @endif 
    <div class="row">
        <div class="col-md-8">
            <!-- Custom Tabs -->
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab_1" data-toggle="tab">Individual Client</a></li>
                    <li><a href="#tab_2" data-toggle="tab">Business Client</a></li>

                    <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_1">

                        <!-- general form elements disabled -->
                        <div class="box box-warning">
                            <div class="box-header">

                            </div><!-- /.box-header -->
                            <div class="box-body">
                                {{ Form::open(array('url'=>'admin/useradd', 'role'=>'form')) }}

                                <!-- text input -->
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label>First name</label>
                                        {{ Form::text('first_name', null, array('placeholder'=>'Enter first name','class'=>'form-control')) }}
                                        @if ($errors->has('first_name')) <div class="text-danger">{{ $errors->first('first_name') }}</div> @endif
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Second name</label>
                                        {{ Form::text('second_name', null, array('placeholder'=>'Enter second name','class'=>'form-control')) }}
                                        @if ($errors->has('second_name')) <div class="text-danger">{{ $errors->first('second_name') }}</div> @endif
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label>Email</label>
                                        {{ Form::email('email', null, array('placeholder'=>'Enter email','class'=>'form-control')) }}
                                        @if ($errors->has('email')) <div class="text-danger">{{ $errors->first('email') }}</div> @endif
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Phone number</label>
                                        {{ Form::text('phone_number', null, array('placeholder'=>'Enter phone number e.g 0704103356','class'=>'form-control')) }}
                                        @if ($errors->has('phone_number')) <div class="text-danger">{{ $errors->first('phone_number') }}</div> @endif
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label>Postal address</label>
                                        {{ Form::text('postal_address', null, array('placeholder'=>'Enter postal address e.g 1010 nairobi','class'=>'form-control')) }}
                                        @if ($errors->has('postal_address')) <div class="text-danger">{{ $errors->first('postal_address') }}</div> @endif
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Location</label>
                                        {{ Form::text('location', null, array('placeholder'=>'Enter location','class'=>'form-control')) }}
                                        @if ($errors->has('location')) <div class="text-danger">{{ $errors->first('location') }}</div> @endif
                                    </div>
                                </div>

                                <div class="box-footer">
                                    <button type="submit" class="btn btn-primary">Create</button>
                                </div>


                                </form>
                            </div><!-- /.box-body -->
                        </div><!-- /.box -->

                    </div><!-- /.tab-pane -->
                    <div class="tab-pane" id="tab_2">
                        <div class="box box-warning">
                            <div class="box-header">

                            </div><!-- /.box-header -->
                            <div class="box-body">
                                {{ Form::open(array('url'=>'admin/useraddbs', 'role'=>'form')) }}

                                <!-- text input -->
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label>Business name</label>
                                        {{ Form::text('business_name', null, array('placeholder'=>'Enter business name','class'=>'form-control')) }}
                                        @if ($errors->has('business_name')) <div class="text-danger">{{ $errors->first('business_name') }}</div> @endif
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Contact person</label>
                                        {{ Form::text('contact_person', null, array('placeholder'=>'Enter contact person','class'=>'form-control')) }}
                                        @if ($errors->has('contact_person')) <div class="text-danger">{{ $errors->first('contact_person') }}</div> @endif
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label>Email</label>
                                        {{ Form::email('business_email', null, array('placeholder'=>'Enter email','class'=>'form-control')) }}
                                        @if ($errors->has('business_email')) <div class="text-danger">{{ $errors->first('business_email') }}</div> @endif
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Business phone number</label>
                                        {{ Form::text('business_phone', null, array('placeholder'=>'Enter phone number e.g 0704103356','class'=>'form-control')) }}
                                        @if ($errors->has('business_phone')) <div class="text-danger">{{ $errors->first('business_phone') }}</div> @endif
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label>Postal address</label>
                                        {{ Form::text('business_postal_address', null, array('placeholder'=>'Enter postal address e.g 1010 nairobi','class'=>'form-control')) }}
                                        @if ($errors->has('business_postal_address')) <div class="text-danger">{{ $errors->first('business_postal_address') }}</div> @endif
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Location</label>
                                        {{ Form::text('business_location', null, array('placeholder'=>'Enter location','class'=>'form-control')) }}
                                        @if ($errors->has('business_location')) <div class="text-danger">{{ $errors->first('business_location') }}</div> @endif
                                    </div>
                                </div>

                                <div class="box-footer">
                                    <button type="submit" class="btn btn-primary">Create</button>
                                </div>


                                </form>
                            </div><!-- /.box-body -->
                        </div><!-- /.box -->
                    </div><!-- /.tab-pane -->
                </div><!-- /.tab-content -->
            </div><!-- nav-tabs-custom -->
        </div><!-- /.col -->
    </div>
</section>
