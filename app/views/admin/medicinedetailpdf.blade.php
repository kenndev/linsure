<!DOCTYPE html>
<html>

    <head>
        <title>Linsure insurance &reg; </title>
        <style>
            body{
                font-size: 14px;
                line-height: 20px;
            }
            .wrapper_main{
                margin: 0 auto;
                width: 100%;
            }
            .top-div{
                padding: 10px;
                border-bottom: 1px solid #eee;
            }
            .logo{
                margin: 0 auto
            }
            .logo p{
                text-align: center;
                font-size: 17px;
            }

            .tabless{
                padding: 10px;
            }

            table {
                width:100%;

                clear: both;
            }
            table, th, td {
                border-collapse: collapse;            
            }
            th, td {
                padding: 5px;
                text-align: left;
                outline: none;
            }
            table#t01 tr:nth-child(even) {
                background-color: #eee;
            }
            table#t01 tr:nth-child(odd) {
                background-color:#fff;
            }
            table#t01 th  {

                color: black;
            }
        </style>

    </head>

    <body>
        <div class="wrapper_main">
            @foreach ($insurance as $insdetails)
            <div id="top" class="top-div">
                <div class="logo ">
                    <p><strong>Linsure Insurance Agency</strong></p> 
                    <p>Medical insurance details </p>
                    <p>{{$insdetails->first_name}} {{$insdetails->second_name}}</p>
                    <p>{{date('Y-m-d')}}</p> 
                </div>
            </div>
            <div class="tabless">
                <table id="t01">
                    <tr>
                        <th>Date of birth</th><td>{{$insdetails->date_of_birth}}</td>
                    </tr>
                    <tr>
                        <th>Company covering</th> <td>{{$insdetails->company_covering}}</td>   
                    </tr>
                    <tr>
                        <th>Impatient</th><td>{{$insdetails->impatient}}</td>
                    </tr>
                    <tr>
                        <th>Outpatient</th><td>{{$insdetails->outpatient}}</td>
                    </tr>
                    <tr>
                        <th>Dental</th><td>{{$insdetails->dental}}</td>
                    </tr>
                    <tr>
                        <th>Optica</th><td>{{$insdetails->optica}}</td>
                    </tr>
                    <tr>
                        <th>Last expense</th><td>{{$insdetails->last_expense}}</td>
                    </tr>
                    <tr>
                        <th>Personal account</th><td>{{$insdetails->personal_account}}</td>
                    </tr>
                    <tr>
                        <th>Maternity</th><td>{{$insdetails->maternity}}</td>
                    </tr>
                    <tr>
                        <th>Insurance start date</th><td>{{$insdetails->date_from}}</td>
                    </tr>
                    <tr>
                        <th>Insurance end date</th><td>{{$insdetails->date_to}}</td>
                    </tr>

                </table>
            </div>
            @endforeach
        </div>
    </body>
</html>
