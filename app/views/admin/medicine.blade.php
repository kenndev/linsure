<section class="content-header">
    <h1>
        Vehicle Claim:
        <small>Create vehicle insurance claim</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::to('admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Vehicle insurance claims</li>
    </ol>
</section>

<section class="content">
    @if(Session::has('message')) 
    <div class="alert alert-success alert-dismissable col-md-10">
        <i class="fa fa-check"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <b>Success!</b> {{ Session::get('message') }}
    </div>
    @endif 
    @if(Session::has('errorMessage'))
    <div class="alert alert-danger alert-dismissable col-md-10">
        <i class="fa fa-ban"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <b>Alert!</b> {{ Session::get('errorMessage') }}
    </div>
    @endif 
    <div class="row">
        <div class="col-md-10">
            <!-- Custom Tabs -->
            <div class="box box-warning">
                <div class="box-header">
                </div><!-- /.box-header -->
                <div class="box-body">
                     
                    {{ Form::open(array('url'=>'admin/medicinepost', 'role'=>'form')) }}

                    <div class="row">

                        <div class="form-group col-md-6">

                            {{ Form::hidden('id', $client_id, array('class'=>'form-control')) }}
                            {{ Form::hidden('client_type', $client_type, array('class'=>'form-control')) }}

                        </div>
                    </div>
                    <!-- text input -->
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label>Date of birth</label>
                            {{ Form::text('date_of_birth', null, array('placeholder'=>'Date of birth','class'=>'form-control')) }}
                            @if ($errors->has('date_of_birth')) <div class="text-danger">{{ $errors->first('date_of_birth') }}</div> @endif
                        </div>
                        <div class="form-group col-md-6">
                            <label>Company covering</label>
                            {{ Form::text('company_covering', null, array('placeholder'=>'Enter police station where incident was reported','class'=>'form-control')) }}
                            @if ($errors->has('company_covering')) <div class="text-danger">{{ $errors->first('company_covering') }}</div> @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <div class="form-group col-md-6"> 

                                <div class="radio">
                                    <label>
                                        {{ Form::radio('optionsRadios', 'impatient', array('id'=>'optionsRadios1','class'=>'form-control' ,'checked')) }}
                                        
                                        Impatient
                                    </label>
                                </div>
                            </div>
                            <div class="form-group col-md-6"> 
                                <div class="radio">
                                    <label>
                                        {{ Form::radio('optionsRadios', 'outpatient', array('id'=>'optionsRadios2','class'=>'form-control')) }}
                                        
                                        Outpatient
                                    </label>
                                </div>
                            </div>
                            @if ($errors->has('optionsRadios')) <div class="text-danger">{{ $errors->first('optionsRadios') }}</div> @endif
                        </div>
                        <div class="form-group col-md-6">
                            <div class="form-group col-md-6"> 
                                <div class="checkbox">
                                    <label>
                                        
                                        {{ Form::checkbox('dental', 'dental') }}
                                        
                                        Dental
                                    </label>
                                    @if ($errors->has('dental')) <div class="text-danger">{{ $errors->first('dental') }}</div> @endif
                                </div>
                            </div>
                            <div class="form-group col-md-6"> 
                                <div class="checkbox">
                                    <label>
                                        {{ Form::checkbox('optica', 'optica') }}
                                        
                                        Optica
                                    </label> 
                                    @if ($errors->has('optica')) <div class="text-danger">{{ $errors->first('optica') }}</div> @endif
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label>Last expense</label>
                            {{ Form::text('last_expense', null, array('placeholder'=>'last expense','class'=>'form-control')) }}
                            @if ($errors->has('last_expense')) <div class="text-danger">{{ $errors->first('last_expense') }}</div> @endif
                        </div>
                        <div class="form-group col-md-6">
                            <div class="form-group col-md-6"> 
                                <div class="checkbox">
                                    <label>
                                        {{ Form::checkbox('personal_account', 'personal_account') }}
                                        
                                        personal account
                                    </label>
                                    @if ($errors->has('personal_account')) <div class="text-danger">{{ $errors->first('personal_account') }}</div> @endif
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                
                                <div class="checkbox">
                                    <label>
                                        {{ Form::checkbox('maternity', 'maternity') }}
                                        
                                        maternity
                                    </label> 
                                    @if ($errors->has('maternity')) <div class="text-danger">{{ $errors->first('maternity') }}</div> @endif
                                </div>
                            </div>

                        </div>
                    </div>
                     <div class="row">
                        <div class="form-group col-md-6">
                            <label>Insurance Start date</label>
                            <div class="input-group">
                                {{ Form::text('date_from', null, array('class'=>'form-control pull-left input-group date')) }}

                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>

                            </div><!-- /.input group -->

                            @if ($errors->has('date_from')) <div class="text-danger">{{ $errors->first('date_from') }}</div> @endif
                        </div>
                        <div class="form-group col-md-6">
                            <label>Insurance End date</label>
                            <div class="input-group">
                                {{ Form::text('date_to', null, array('class'=>'form-control pull-left input-group date')) }}

                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>

                            </div><!-- /.input group -->

                            @if ($errors->has('date_to')) <div class="text-danger">{{ $errors->first('date_to') }}</div> @endif
                        </div>
                    </div>


                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Create</button>
                    </div>


                    </form>


                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div>
</section>
