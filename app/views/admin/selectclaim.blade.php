
<section class="content-header">
    <h1>
        Choose a claim

    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::to('admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">select a claim</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <!-- Small boxes (Stat box) -->

    <div class="row">

        {{ Form::hidden('client_id', $client_id, array('class'=>'form-control')) }}
        <div class="col-lg-3 col-xs-6 ">

            <!-- small box -->
            <div class="small-box bg-navy">
                <a href="{{ URL::to('admin/claimVehicle/'.$client_id.'/'.$client_type) }}" class="btn btn-lg">
                    <div class="inner">
                        <h3>
                            <sup style="font-size: 20px; color: #FFFFFF">Partial damage</sup>
                        </h3>
                        <p>
                            <sup style="font-size: 20px; color: #FFFFFF">Claim</sup>
                        </p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-anchor"></i>
                    </div>
                    <a href="#" class="small-box-footer">
                        Linsure insurance management system <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </a>
            </div>

        </div><!-- ./col -->

        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-light-blue">
                <a href="{{ URL::to('admin/theftClaim/'.$client_id.'/'.$client_type) }}" class="btn btn-lg">
                    <div class="inner">
                        <h3>
                            <sup style="font-size: 20px; color: #FFFFFF">Theft Claim</sup>
                        </h3>
                        <p>
                            <sup style="font-size: 20px; color: #FFFFFF">.</sup>
                        </p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-code-fork"></i>
                    </div>
                    <a href="#" class="small-box-footer">
                        Linsure insurance management system <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </a>
            </div>
        </div><!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-aqua">
                <a href="{{ URL::to('admin/windscreenClaim/'.$client_id.'/'.$client_type) }}" class="btn btn-lg">
                    <div class="inner">
                        <h3>
                            <sup style="font-size: 20px; color: #FFFFFF">Windscreen replace</sup>
                        </h3>
                        <p>
                            <sup style="font-size: 20px; color: #FFFFFF">cover</sup>
                        </p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-random"></i>
                    </div>
                    <a href="#" class="small-box-footer">
                        Linsure insurance management system <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </a>
            </div>
        </div><!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-teal">
                <a href="{{ URL::to('admin/radiocasetteClaim/'.$client_id.'/'.$client_type) }}" class="btn btn-lg">
                    <div class="inner">
                        <h3>
                            <sup style="font-size: 20px; color: #FFFFFF">Radio cassette</sup>
                        </h3>
                        <p>
                            <sup style="font-size: 20px; color: #FFFFFF">cover</sup>
                        </p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-leaf"></i>
                    </div>
                    <a href="#" class="small-box-footer">
                        Linsure insurance management system <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </a>
            </div>
        </div><!-- ./col -->
    </div><!-- /.row -->
</section><!-- /.content -->

