<section class="content-header no-margin">
    <h1>
        <i class="fa fa-inbox"></i>
        Export insurance covers to excel:
        <small>exports data from mysql to excel</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::to('admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">export to excel</li>
    </ol>
</section>
<section class="content">
    <!-- MAILBOX BEGIN -->
    <div class="mailbox row">
        <div class="col-xs-6">
            <div class="box box-solid">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">

                            <div class="panel-group" id="accordion">
                                <div class="panel panel-primary">

                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#accordionOne">
                                                Export published individual vehicle insurance covers to excel
                                            </a>
                                        </h4>
                                    </div>

                                    <div id="accordionOne" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <a href="{{ URL::to('admin/exportinsexcel') }}" class="btn btn-block bg-purple" ><i class="fa fa-pencil"></i> export to excel</a>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel panel-danger">

                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#accordionTwo">
                                                Export published business vehicle insurance covers to excel
                                            </a>
                                        </h4>
                                    </div>

                                    <div id="accordionTwo" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <a href="{{ URL::to('admin/exportbsexcel') }}" class="btn btn-block bg-maroon" ><i class="fa fa-pencil"></i> export to excel</a>
                                        </div>
                                    </div>

                                </div>

                                <div class="panel panel-success">

                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#accordionThree">
                                                Export published medical insurance covers to excel
                                            </a>
                                        </h4>
                                    </div>

                                    <div id="accordionThree" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <a href="{{ URL::to('admin/exportmedexcel') }}" class="btn btn-block bg-green" ><i class="fa fa-pencil"></i> export to excel</a>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- /.col (LEFT) -->

                        </div><!-- /.row -->
                    </div><!-- /.box-body -->

                </div><!-- /.box -->
            </div><!-- /.col (MAIN) -->
        </div>
        <!-- MAILBOX END -->

</section><!-- /.content -->


