<section class="content-header">
    <h1>
        View Published Vehicle Insurance :
        <small>View vehicle insurance</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::to('admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">View vehicle insurance</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        @if(Session::has('message')) 
        <div class="alert alert-success alert-dismissable col-md-10">
            <i class="fa fa-check"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <b>Success!</b> {{ Session::get('message') }}
        </div>
        @endif 
        @if(Session::has('errorMessage'))
        <div class="alert alert-danger alert-dismissable col-md-10">
            <i class="fa fa-ban"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <b>Alert!</b> {{ Session::get('errorMessage') }}
        </div>
        @endif 
    </div>
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#tab_1" data-toggle="tab">Individual Client</a></li>
            <li><a href="#tab_2" data-toggle="tab">Business Client</a></li>

            <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="tab_1">

                <!-- general form elements disabled -->
                <div class="box box-warning">
                    <div class="box-header">

                    </div><!-- /.box-header -->


                    <div class="box-body">
                        <div class="box-header">
                            <h3 class="box-title">Individual clients vehicle insurance</h3>                                    
                        </div><!-- /.box-header -->

                        <div class="box-body table-responsive">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Insurance number</th>
                                        <th>First name</th>
                                        <th>Second name</th>
                                        <th>Email</th>
                                        <th>Phone number</th>                                       
                                        <th>Car registration</th>
                                        <th>Risk note</th>
                                        <th>Create claim</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>


                                    @foreach ($insurance_individual as $insurance_indi)
                                    <tr>
                                        <td>{{$insurance_indi->insurance_id}}</td>
                                        <td>{{$insurance_indi->first_name}}</td>
                                        <td>{{$insurance_indi->second_name}}</td>
                                        <td>{{$insurance_indi->email}}</td>
                                        <td>{{$insurance_indi->phone_number}}</td>                                     
                                        <td>{{$insurance_indi->car_registration}}</td>

                                        <td>

                                            {{ Form::open(array('method' => 'get', 'action' => array('AdminController@risknote_View',$insurance_indi->id,5))) }}                       
                                            {{ Form::submit('risk note', array('class' => 'btn btn-info btn-xs')) }}
                                            {{ Form::close() }}
                                        </td>
                                        <td>

                                            {{ Form::open(array('method' => 'PUT', 'action' => array('AdminController@claim_choose',$insurance_indi->id,5))) }}                       
                                            {{ Form::submit('create claim', array('class' => 'btn btn-primary btn-xs')) }}
                                            {{ Form::close() }}
                                        </td>
                                        <td>

                                            {{ Form::open(array('method' => 'PUT', 'action' => array('AdminController@drop_Insurance',$insurance_indi->id,5))) }}                       
                                            {{ Form::submit('drop cover', array('class' => 'btn btn-danger btn-xs')) }}
                                            {{ Form::close() }}
                                        </td>
                                    </tr>
                                    @endforeach


                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>Insurance number</th>
                                        <th>First name</th>
                                        <th>Second name</th>
                                        <th>Email</th>
                                        <th>Phone number</th>
                                        <th>Car registration</th>
                                        <th>Risk note</th>
                                        <th>Create claim</th>
                                        <th>Action</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->

                </div><!-- /.box -->

            </div><!-- /.tab-pane -->
            <div class="tab-pane" id="tab_2">
                <div class="box box-warning">
                    <div class="box-header">

                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div class="box-header">
                            <h3 class="box-title">Business clients vehicle insurance</h3>                                    
                        </div><!-- /.box-header -->
                        <div class="box-body table-responsive ">
                            <table id="example3" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Insurance number</th>
                                        <th>Business name</th>

                                        <th>Business email</th>
                                        <th>Business phone number</th>

                                        <th>Car registration number</th>
                                        <th>Risk note</th>
                                        <th>Create claim</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <!--10 for individual clients-->
                                    @foreach ($insurance_business as $insurance_bs)
                                    <tr>
                                        <td>{{$insurance_bs->insurance_id}}</td>
                                        <td>{{$insurance_bs->business_name}}</td>

                                        <td>{{$insurance_bs->business_email}}</td>
                                        <td>{{$insurance_bs->business_phone}}</td>

                                        <td>{{$insurance_bs->car_registration}}</td>
                                        <td>

                                            {{ Form::open(array('method' => 'get', 'action' => array('AdminController@risknote_View',$insurance_bs->id,10))) }}                       
                                            {{ Form::submit('risk note', array('class' => 'btn btn-info btn-xs')) }}
                                            {{ Form::close() }}
                                        </td>
                                        <td>

                                            {{ Form::open(array('method' => 'PUT', 'action' => array('AdminController@claim_choose',$insurance_bs->id,10 ))) }}                       
                                            {{ Form::submit('create claim', array('class' => 'btn btn-primary btn-xs')) }}
                                            {{ Form::close() }}
                                        </td>
                                        <td>

                                            {{ Form::open(array('method' => 'PUT', 'action' => array('AdminController@drop_Insurance',$insurance_bs->id,10 ))) }}                       
                                            {{ Form::submit('drop insurance', array('class' => 'btn btn-danger btn-xs')) }}
                                            {{ Form::close() }}
                                        </td>
                                    </tr>
                                    @endforeach


                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>Insurance number</th>
                                        <th>Business name</th>

                                        <th>Business email</th>
                                        <th>Business phone number</th>

                                        <th>Car registration number</th>
                                        <th>Risk note</th>
                                        <th>Create claim</th>
                                        <th>Action</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </div><!-- /.box -->
            </div><!-- /.tab-pane -->
        </div><!-- /.tab-content -->
    </div><!-- nav-tabs-custom -->


</section>