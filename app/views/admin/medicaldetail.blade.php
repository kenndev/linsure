
<section class="content-header">
    <h1>
        Medical details :
        <small>View medical insurance details</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::to('admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">medical insurance details</li>
    </ol>
</section>
<!-- Main content -->

<section class="content invoice">                    
    <!-- title row -->
    <div id="div_print">
        <div class="row">
            <div class="col-xs-12">
                <h2 class="page-header" style="margin: 10px 0 20px 0; height: 50px;">
                    <i class="fa fa-globe"></i> Linsure Insurance Agency

                    <small class="pull-right"> {{ Form::open(array('method' => 'get', 'action' => array('AdminController@medical_insurance_details_pdf', $risk_id))) }}                       
                        {{ Form::submit('Generate PDF', array('class' => 'btn btn-primary')) }}
                        {{ Form::close() }}</small>
                </h2>

            </div><!-- /.col -->

        </div>
        <!-- info row -->
        @foreach ($insurance as $insdetails)
        <div class="row invoice-info">
            <div class="col-sm-4 invoice-col text-center" style="margin: 0 auto; float: none;">

                <address>
                    <strong>Linsure Insurance Agency</strong><br>
                    Medical insurance details <br>
                    {{$insdetails->first_name}} {{$insdetails->second_name}}<br>
                    {{date('Y-m-d')}}<br/>

                </address>
            </div><!-- /.col -->


        </div><!-- /.row -->

        <!-- Table row -->
        <div class="row">
            <div class="col-xs-12 table-responsive">
                <table class="table table-striped">

                    <tbody>
                        <tr>
                            <th>Date of birth</th><td>{{$insdetails->date_of_birth}}</td>
                        </tr>
                        <tr>
                            <th>Company covering</th> <td>{{$insdetails->company_covering}}</td>   
                        </tr>
                        <tr>
                            <th>Impatient</th><td>{{$insdetails->impatient}}</td>
                        </tr>
                        <tr>
                            <th>Outpatient</th><td>{{$insdetails->outpatient}}</td>
                        </tr>
                        <tr>
                            <th>Dental</th><td>{{$insdetails->dental}}</td>
                        </tr>
                        <tr>
                            <th>Optica</th><td>{{$insdetails->optica}}</td>
                        </tr>
                        <tr>
                            <th>Last expense</th><td>{{$insdetails->last_expense}}</td>
                        </tr>
                        <tr>
                            <th>Personal account</th><td>{{$insdetails->personal_account}}</td>
                        </tr>
                        <tr>
                            <th>Maternity</th><td>{{$insdetails->maternity}}</td>
                        </tr>
                        <tr>
                            <th>Insurance start date</th><td>{{$insdetails->date_from}}</td>
                        </tr>
                        <tr>
                            <th>Insurance end date</th><td>{{$insdetails->date_to}}</td>
                        </tr>
                        
                    </tbody>
                </table>                            
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div>
    @endforeach
    <!-- this row will not appear when printing -->


    <div class="row no-print">
        <div class="col-xs-12">
            
            {{ Form::open(array('method' => 'get', 'action' => array('AdminController@medical_insurance_details_pdf', $risk_id))) }}                       
            {{ Form::submit('Generate PDF', array('class' => 'btn btn-primary pull-left')) }}
            {{ Form::close() }}
        </div>
    </div>
</section><!-- /.content -->

