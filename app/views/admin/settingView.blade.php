<section class="content-header">
    <h1>
        Settings:
        <small>Set vehicle insurance application settings</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::to('admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Settings</li>
    </ol>
</section>

<section class="content">
    @if(Session::has('message')) 
    <div class="alert alert-success alert-dismissable col-md-10">
        <i class="fa fa-check"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <b>Success!</b> {{ Session::get('message') }}
    </div>
    @endif 
    @if(Session::has('errorMessage'))
    <div class="alert alert-danger alert-dismissable col-md-10">
        <i class="fa fa-ban"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <b>Alert!</b> {{ Session::get('errorMessage') }}
    </div>
    @endif 
    <div class="row">
        <div class="col-md-10">
            <!-- Custom Tabs -->
            <div class="box box-warning">
                <div class="box-header">

                </div><!-- /.box-header -->

                <div class="box-body">
                    @foreach ($settingsRow as $settings)

                    {{ Form::open(array('url'=>'admin/updatesettings', 'role'=>'form')) }}
                    {{ Form::hidden('id_update', $settings->id, array('placeholder'=>'Enter levies','class'=>'form-control')) }}
                    <!-- text input -->
                    <div class="row">
                        <div class="form-group col-md-6">

                            <label>P.H.C.F levy</label>
                            {{ Form::text('phcf_lvy', $settings->phcf_levy, array('placeholder'=>'Enter policy holders compesition fund','class'=>'form-control')) }}
                            @if ($errors->has('phcf_lvy')) <div class="text-danger">{{ $errors->first('phcf_lvy') }}</div> @endif
                        </div>
                        <div class="form-group col-md-6">
                            <label>Training levy</label>
                            {{ Form::text('training_levy', $settings->training_levy, array('placeholder'=>'Enter training levy','class'=>'form-control')) }}
                            @if ($errors->has('training_levy')) <div class="text-danger">{{ $errors->first('training_levy') }}</div> @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label>Rate</label>
                            {{ Form::text('rate', $settings->rate, array('placeholder'=>'Enter rate','class'=>'form-control')) }}
                            @if ($errors->has('rate')) <div class="text-danger">{{ $errors->first('rate') }}</div> @endif
                        </div>
                        <div class="form-group col-md-6">
                            <label>stamp duty</label>
                            {{ Form::text('stamp_duty', $settings->stamp_duty, array('placeholder'=>'Enter stamp duty','class'=>'form-control')) }}
                            @if ($errors->has('stamp_duty')) <div class="text-danger">{{ $errors->first('stamp_duty') }}</div> @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label>Political violence</label>
                            {{ Form::text('political_violence', $settings->political_violence, array('placeholder'=>'Enter political violence','class'=>'form-control')) }}
                            @if ($errors->has('political_violence')) <div class="text-danger">{{ $errors->first('political_violence') }}</div> @endif
                        </div>
                        <div class="form-group col-md-6">
                            <label>Riot's</label>
                            {{ Form::text('riot', $settings->riot, array('placeholder'=>'Enter riot','class'=>'form-control')) }}
                            @if ($errors->has('riot')) <div class="text-danger">{{ $errors->first('riot') }}</div> @endif
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Update</button>
                    </div>


                    </form>
                    @endforeach
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div>

</section>
