<section class="content-header">
    <h1>
        Theft Claim:
        <small>Create theft claim</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::to('admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Create theft claim</li>
    </ol>
</section>

<section class="content">
    @if(Session::has('message')) 
    <div class="alert alert-success alert-dismissable col-md-10">
        <i class="fa fa-check"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <b>Success!</b> {{ Session::get('message') }}
    </div>
    @endif 
    @if(Session::has('errorMessage'))
    <div class="alert alert-danger alert-dismissable col-md-10">
        <i class="fa fa-ban"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <b>Alert!</b> {{ Session::get('errorMessage') }}
    </div>
    @endif 
    <div class="row">
        <div class="col-md-10">
            <!-- Custom Tabs -->
            <div class="box box-warning">
                <div class="box-header">



                </div><!-- /.box-header -->
                <div class="box-body">
                    {{ Form::open(array('url'=>'admin/theftClaimPost', 'role'=>'form')) }}

                    <div class="row">

                        <div class="form-group col-md-6">

                            {{ Form::hidden('id', $client_id, array('class'=>'form-control')) }}
                            {{ Form::hidden('client_type', $type_client, array('class'=>'form-control')) }}
                            {{ Form::hidden('insurance_id', $insurance_id, array('class'=>'form-control')) }}
                        </div>
                    </div>
                    <!-- text input -->
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label>Location of accident</label>
                            {{ Form::text('location_of_accident', null, array('placeholder'=>'Enter location of accident','class'=>'form-control')) }}
                            @if ($errors->has('location_of_accident')) <div class="text-danger">{{ $errors->first('location_of_accident') }}</div> @endif
                        </div>
                        <div class="form-group col-md-6">
                            <label>police station</label>
                            {{ Form::text('police_station', null, array('placeholder'=>'Enter police station where incident was reported','class'=>'form-control')) }}
                            @if ($errors->has('police_station')) <div class="text-danger">{{ $errors->first('police_station') }}</div> @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label>Time of incident</label>
                            {{ Form::text('time', null, array('placeholder'=>'Enter time of incident','class'=>'form-control')) }}
                            @if ($errors->has('time')) <div class="text-danger">{{ $errors->first('time') }}</div> @endif
                        </div>
                        <div class="form-group col-md-6">
                            <label>Date reported</label>
                            <div class="input-group">
                                {{ Form::text('date_reported', null, array('placeholder'=>'choose the date incident was reported','class'=>'form-control pull-left input-group date')) }}

                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>

                            </div><!-- /.input group -->

                            @if ($errors->has('date_reported')) <div class="text-danger">{{ $errors->first('date_reported') }}</div> @endif
                        </div>
                    </div>
                    <div class="row">
                        
                        <div class="form-group col-md-6">
                            <label>Date of incident</label>
                            <div class="input-group">
                                {{ Form::text('date_incident', null, array('placeholder'=>'choose the date of incident','class'=>'form-control pull-left input-group date')) }}

                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>

                            </div><!-- /.input group -->

                            @if ($errors->has('date_incident')) <div class="text-danger">{{ $errors->first('date_incident') }}</div> @endif
                        </div>
                    </div>


                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Create</button>
                    </div>


                    </form>


                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div>
</section>
