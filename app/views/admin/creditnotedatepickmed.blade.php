
<section class="content-header">
    <h1>
        Credit note medicine insurance:
        <small>credit note insurance cancel date pick</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::to('admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">credit note</li>
    </ol>
</section>
<!-- Main content -->

<section class="content">                    
    <!-- title row -->
    @if(Session::has('message')) 
    <div class="alert alert-success alert-dismissable col-md-10">
        <i class="fa fa-check"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <b>Success!</b> {{ Session::get('message') }}
    </div>
    @endif 
    @if(Session::has('errorMessage'))
    <div class="alert alert-danger alert-dismissable col-md-10">
        <i class="fa fa-ban"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <b>Alert!</b></br> {{ Session::get('errorMessage') }}
    </div>
    @endif 

    <div class="row">
        <div class="col-md-8">
            <!-- Custom Tabs -->
            <div class="box box-solid">

                <div class="box-body clearfix">
                    {{ Form::open(array('url'=>'admin/creditaddMed', 'role'=>'form')) }}
                    {{ Form::hidden('id', $credit_id, array('class'=>'form-control')) }}
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label>Insurance Cancel date</label>
                            <div class="input-group">
                                {{ Form::text('insurance_cancel_date', null, array('placeholder'=>'pick insurance cancel date','class'=>'form-control pull-left input-group date')) }}

                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>

                            </div><!-- /.input group -->

                            @if ($errors->has('insurance_cancel_date')) <div class="text-danger">{{ $errors->first('insurance_cancel_date') }}</div> @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label>Amended by</label>
                            {{ Form::text('amended_by', null, array('placeholder'=>'Enter name','class'=>'form-control')) }}
                            @if ($errors->has('amended_by')) <div class="text-danger">{{ $errors->first('amended_by') }}</div> @endif                       
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Create</button>
                    </div>


                    </form>
                </div>

            </div>
        </div><!-- /.box-body -->
    </div><!-- /.box -->
</div><!-- /.col -->
</div>
</section><!-- /.content -->
