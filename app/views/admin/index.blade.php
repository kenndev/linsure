
<section class="content-header">
    <h1>
        Dashboard

    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::to('admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    @if(Session::has('message')) 
    <div class="alert alert-success alert-dismissable col-md-10">
        <i class="fa fa-check"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <b>Success!</b></br> {{ Session::get('message') }}
    </div>
    @endif 
    @if(Session::has('errorMessage'))
    <div class="alert alert-danger alert-dismissable col-md-10">
        <i class="fa fa-ban"></i>
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <b>Alert!</b></br> {{ Session::get('errorMessage') }}
    </div>
    @endif
    <!-- Small boxes (Stat box) -->
    <div class="row">
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3>
                        {{$clientsindi}}
                    </h3>
                    <p>
                        individual clients 
                    </p>
                </div>
                <div class="icon">
                    <i class="ion ion-person"></i>
                </div>
                <a href="#" class="small-box-footer">
                    More info <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div><!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-green">
                <div class="inner">
                    <h3>
                        {{$clientsbs}}
                    </h3>
                    <p>
                        business clients 
                    </p>
                </div>
                <div class="icon">
                    <i class="ion ion-person-stalker"></i>
                </div>
                <a href="#" class="small-box-footer">
                    More info <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div><!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3>
                        {{$totalvehicleins}}
                    </h3>
                    <p>
                        Vehicle insurance 
                    </p>
                </div>
                <div class="icon">
                    <i class="ion ion-gear-a"></i>
                </div>
                <a href="#" class="small-box-footer">
                    More info <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div><!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-red">
                <div class="inner">
                    <h3>
                        {{$totalmedicalins}}
                    </h3>
                    <p>
                        Medical insurances 
                    </p>
                </div>
                <div class="icon">
                    <i class="ion ion-medkit"></i>
                </div>
                <a href="#" class="small-box-footer">
                    More info <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div><!-- ./col -->
    </div><!-- /.row -->


    <div class="row">
        <div class="col-md-6">

            <!-- DONUT CHART -->
            <div class="box box-danger">
                <div class="box-header">
                    <h3 class="box-title">Insurance comparison donut chart</h3>
                </div>
                <div class="box-body chart-responsive">
                    <div class="chart" id="sales-chart" style="height: 300px; position: relative;"></div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->

        </div><!-- /.col (LEFT) -->
        <div class="col-md-6">
            <!-- TO DO List -->
            <div class="box box-primary">
                <div class="box-header">
                    <i class="ion ion-clipboard"></i>
                    <h3 class="box-title">To Do List</h3>
                    <div class="box-tools pull-right small-box">

                    </div>
                </div><!-- /.box-header -->
                <div class="box-body">
                    <ul class="todo-list">
                        @foreach ($todo as $do)

                        <li>
                            <!-- drag handle -->
                            <span class="handle">
                                <i class="fa fa-ellipsis-v"></i>
                                <i class="fa fa-ellipsis-v"></i>
                            </span>  
                            <!-- checkbox -->
                            <input type="checkbox" value="" name=""/>                                            
                            <!-- todo text -->
                            <span class="text">{{$do->title}}</span>
                            <!-- Emphasis label -->

                            <?php
                            $todocolors = DB::table('colors')
                                    ->orderBy(DB::raw('RAND()'))
                                    ->limit(1)
                                    ->get();
                            foreach ($todocolors as $col) {
                                ?>

                                <small class="label {{$col->color_title}}"><i class="fa fa-clock-o"></i>{{\Carbon\Carbon::createFromTimeStamp(strtotime($do->created_at))->diffForHumans() }}</small>
                                <!-- General tools such as edit or delete-->
                            <?php } ?>

                            <div class="tools">


                                <a class="text-red" href="{{ URL::to('admin/deletetodo/'.$do->id) }}"><i class="fa fa-trash-o"></i></a>


                            </div>
                        </li>

                        @endforeach

                    </ul>
                </div><!-- /.box-body -->
                <div class="box-footer clearfix no-border col-xs-12">
                    <div class="col-xs-8">
                        {{$todo->links()}}
                    </div>
                    <div class="col-xs-4">
                        <button class="btn btn-default pull-right" data-toggle="modal" data-target="#compose-modal"><i class="fa fa-plus"></i> Add item</button>
                    </div>
                </div>
            </div><!-- /.box -->
            <!-- COMPOSE MESSAGE MODAL -->
            <div class="modal fade" id="compose-modal" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title"><i class="fa fa-star"></i> New to do action</h4>
                        </div>
                        {{ Form::open(array('url'=>'admin/todo', 'role'=>'form','files' => true)) }}
                        <div class="modal-body">
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon">TO do:</span>
                                    {{ Form::text('to_do', null, array('placeholder'=>'New to do action','class'=>'form-control')) }}
                                    @if ($errors->has('to_do')) <div class="text-danger">{{ $errors->first('to_do') }}</div> @endif

                                </div>
                            </div>
                        </div>
                        <div class="modal-footer clearfix">

                            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Discard</button>

                            <button type="submit" class="btn btn-primary pull-left"><i class="fa fa-save"></i> Save</button>
                        </div>
                        </form>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->


        </div><!-- /.col (RIGHT) -->
    </div><!-- /.row -->




</section><!-- /.content -->







