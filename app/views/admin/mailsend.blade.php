<section class="content-header no-margin">
    <h1>
        Mailbox:
        <small>Send email</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::to('admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Email</li>
    </ol>
</section>
<section class="content">
    <!-- MAILBOX BEGIN -->
    <div class="mailbox row">
        <div class="col-xs-6">
            <div class="box box-solid">
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <!-- BOXES are complex enough to move the .box-header around.
                                 This is an example of having the box header within the box body -->
                            <div class="box-header">
                                <i class="fa fa-inbox"></i>
                                <h3 class="box-title">E-mail</h3>
                            </div>
                            <!-- compose message btn -->
                            <a class="btn btn-block btn-primary" data-toggle="modal" data-target="#compose-modal"><i class="fa fa-pencil"></i> Compose Message</a>
                            <!-- Navigation - folders-->

                        </div><!-- /.col (LEFT) -->

                    </div><!-- /.row -->
                </div><!-- /.box-body -->

            </div><!-- /.box -->
        </div><!-- /.col (MAIN) -->
    </div>
    <!-- MAILBOX END -->

</section><!-- /.content -->

<!-- COMPOSE MESSAGE MODAL -->
<div class="modal fade" id="compose-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><i class="fa fa-envelope-o"></i> Compose New Message</h4>
            </div>
            {{ Form::open(array('url'=>'admin/sendemail', 'role'=>'form','files' => true)) }}
            <div class="modal-body">
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon">TO:</span>
                        {{ Form::email('email_to', null, array('placeholder'=>'Email TO','class'=>'form-control')) }}
                        @if ($errors->has('email_to')) <div class="text-danger">{{ $errors->first('email_to') }}</div> @endif

                    </div>
                </div>
                
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon">Subject:</span>
                        {{ Form::text('subject', null, array('placeholder'=>'Subject','class'=>'form-control')) }}
                        


                    </div>
                </div>

                <div class="form-group">
                    {{ Form::textarea('message', null, array('placeholder'=>'Message','class'=>'form-control','id'=>'email_message','style'=>'height: 120px')) }}
<!--                        <textarea name="message" id="email_message" class="form-control" placeholder="Message" style="height: 120px;"></textarea>-->
                </div>
                <div class="form-group">                                

                    <i class="fa fa-paperclip"></i> Attachment
                    {{ Form::file('attachment', null, array('class'=>'form-control')) }}



                    <p class="help-block">Max. 32MB</p>
                </div>

            </div>
            <div class="modal-footer clearfix">

                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Discard</button>
                
                <button type="submit" class="btn btn-primary pull-left"><i class="fa fa-envelope"></i> Send Message</button>
            </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

