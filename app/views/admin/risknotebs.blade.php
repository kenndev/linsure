
<section class="content-header">
    <h1>
        Rist note :
        <small>View vehicle insurance cover details</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::to('admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">risk note</li>
    </ol>
</section>
<!-- Main content -->

<section class="content invoice">                    
    <!-- title row -->
    <div id="div_print">
        <div class="row">
            <div class="col-xs-12">
                <h2 class="page-header" style="margin: 10px 0 20px 0; height: 50px;">
                    <i class="fa fa-globe"></i> Linsure Insurance Agency

                    <small class="pull-right"> {{ Form::open(array('method' => 'get', 'action' => array('AdminController@risknote', $risk_id,$type_client))) }}                       
                        {{ Form::submit('Generate PDF', array('class' => 'btn btn-primary')) }}
                        {{ Form::close() }}</small>
                </h2>

            </div><!-- /.col -->

        </div>
        <!-- info row -->
        @foreach ($insurance as $insdetails)
        <div class="row invoice-info">
            <div class="col-sm-4 invoice-col text-center" style="margin: 0 auto; float: none;">

                <address>
                    <strong>Linsure Insurance Agency</strong><br>
                    Vehicle insurance risk note <br>
                    {{$insdetails->business_name}}<br>
                    {{date('Y-m-d')}}<br/>

                </address>
            </div><!-- /.col -->


        </div><!-- /.row -->

        <!-- Table row -->
        <div class="row">
            <div class="col-xs-12 table-responsive">
                <table class="table table-striped">

                    <tbody>
                        <tr>
                            <th>Car registration</th><td>{{$insdetails->car_registration}}</td>
                        </tr>
                        <tr>
                            <th>Engine number</th> <td>{{$insdetails->engine_number}}</td>   
                        </tr>
                        <tr>
                            <th>Insurance start date</th><td>{{$insdetails->insurance_start_date}}</td>
                        </tr>
                        <tr>
                            <th>Insurance end date</th><td>{{$insdetails->insurance_end_date}}</td>
                        </tr>
                        <tr>
                            <th>Courtesy vehicle</th><td>{{$insdetails->courtesy_vehicle}}</td>
                        </tr>
                        <tr>
                            <th>Manufacturing year</th><td>{{$insdetails->manufacturing_year}}</td>
                        </tr>
                        <tr>
                            <th>Cubic capacity</th><td>{{$insdetails->cubic_capacity}}</td>
                        </tr>
                        <tr>
                            <th>Vehicle value</th><td>{{$insdetails->vehicle_value}}</td>
                        </tr>
                        <tr>
                            <th>Radio cassette</th><td>{{$insdetails->radio_cassette}}</td>
                        </tr>
                        <tr>
                            <th>Anti theft</th><td>{{$insdetails->anti_theft}}</td>
                        </tr>
                        <tr>
                            <th>Windscreen</th><td>{{$insdetails->windscreen}}</td>
                        </tr>
                        <tr>
                            <th>Chasis number</th><td>{{$insdetails->chasis_number}}</td>
                        </tr>
                        <tr>
                            <th>Premium</th><td>{{$insdetails->total_premium}}</td>
                        </tr>
                    </tbody>
                </table>                            
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div>
    @endforeach
    <!-- this row will not appear when printing -->


    <div class="row no-print">
        <div class="col-xs-12">
            
            {{ Form::open(array('method' => 'get', 'action' => array('AdminController@risknote', $risk_id,$type_client))) }}                       
            {{ Form::submit('Generate PDF', array('class' => 'btn btn-primary pull-left')) }}
            {{ Form::close() }}
        </div>
    </div>
</section><!-- /.content -->

