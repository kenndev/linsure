<section class="content-header">
    <h1>
        Invoice edit:


        <small>edit invoice</small>


    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::to('admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">invoice client</li>
    </ol>
</section>
<section class="content">

    <div class='row'>
        <div class='col-md-6'>
            <div class='box box-info'>

                <div class='box-body pad'>
                    {{ Form::open(array('url'=>'admin/updateinvoice', 'role'=>'form')) }}
                    @foreach ($invoice_edit as $invoice_desc)
                    <div class="row">
                        <div class="form-group col-md-12">
                            
                            {{ Form::textarea('invoice', $invoice_desc->description, array('placeholder'=>'invoice description','class'=>'textarea form-control','id'=>'invoice','style'=>'height: 120px')) }}
                            {{ Form::hidden('id_update', $invoice_desc->id, array('class'=>'form-control')) }}
                        </div>
                    </div>
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Create</button>
                    </div>
                    @endforeach
                    </form>
                </div>
            </div><!-- /.box -->


        </div><!-- /.col-->
    </div><!-- ./row -->



</section><!-- /.content -->
<script type="text/javascript">
    $(function () {
        // Replace the <textarea id="editor1"> with a CKEditor
        // instance, using default configuration.
        CKEDITOR.replace('editor1');
        //bootstrap WYSIHTML5 - text editor
        $(".textarea").wysihtml5();
    });
</script>


