<section class="content-header">
    <h1>
        Renew Insurance Module:
        <small>Renew expired insurance covers</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::to('admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Renew insurance cover</li>
    </ol>
</section>

<section class="content">
    <div class="row">
        @if(Session::has('message')) 
        <div class="alert alert-success alert-dismissable col-md-10">
            <i class="fa fa-check"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <b>Success!</b> {{ Session::get('message') }}
        </div>
        @endif 
        @if(Session::has('errorMessage'))
        <div class="alert alert-danger alert-dismissable col-md-10">
            <i class="fa fa-ban"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <b>Alert!</b> {{ Session::get('errorMessage') }}
        </div>
        @endif 
    </div>
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#tab_1" data-toggle="tab">Renew insurance cover</a></li>



        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="tab_1">

                <!-- general form elements disabled -->
                <div class="box box-warning">
                    <div class="box-header">

                    </div><!-- /.box-header -->

                    <div class="box-body">
                        {{ Form::hidden('ins_type', $type, array('placeholder'=>'Enter insurance type','class'=>'form-control')) }}
                        <div class="box-body table-responsive">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Insurance number</th>
                                        <th>Email</th>
                                        <th>Phone number</th>                                       
                                        <th>Date from</th>
                                        <th>Date to</th>
                                        <th>Insurance type</th>
                                        <th>Action</th>

                                    </tr>
                                </thead>
                                <tbody>



                                    <?php if (strcmp($type, "vehicle_insurance_bs") == 0) { ?>
                                        @foreach ($ins as $ins_renew)
                                        <tr>
                                            <td>{{$ins_renew->insurance_id}}</td>
                                            <td>{{$ins_renew->business_email}}</td>
                                            <td>{{$ins_renew->business_phone}}</td>

                                            <td>{{$ins_renew->insurance_start_date}}</td>
                                            <td>{{$ins_renew->insurance_end_date}}</td>
                                            <td>{{$typeclient}}</td>
                                            <td>

                                                {{ Form::open(array('method' => 'GET', 'action' => array('AdminController@renew_insurancepage',$ins_renew->id))) }}                       
                                                {{ Form::submit('renew', array('class' => 'btn btn-primary btn-xs')) }}
                                                {{ Form::close() }}
                                            </td>
                                            <!--5 for individual clients-->


                                        </tr>
                                        @endforeach

                                    <?php } elseif (strcmp($type, "vehicle_insurance_indi") == 0) { ?>
                                        @foreach ($ins as $ins_renew)
                                        <tr>
                                            <td>{{$ins_renew->insurance_id}}</td>
                                            <td>{{$ins_renew->email}}</td>
                                            <td>{{$ins_renew->phone_number}}</td>

                                           <td>{{$ins_renew->insurance_start_date}}</td>
                                            <td>{{$ins_renew->insurance_end_date}}</td>
                                            <td>{{$typeclient}}</td>
                                            <td>

                                                {{ Form::open(array('method' => 'GET', 'action' => array('AdminController@renew_insurancepage',$ins_renew->id))) }}                       
                                                {{ Form::submit('renew', array('class' => 'btn btn-primary btn-xs')) }}
                                                {{ Form::close() }}
                                            </td>
                                            <!--5 for individual clients-->


                                        </tr>
                                        @endforeach
                                        
                                        
                                <?php } elseif(strcmp($type, "medical_insurance_indi") == 0) {?>

                    
                                        @foreach ($ins as $ins_renew)
                                        <tr>
                                            <td>{{$ins_renew->insurance_number}}</td>
                                            <td>{{$ins_renew->email}}</td>
                                            <td>{{$ins_renew->phone_number}}</td>

                                            <td>{{$ins_renew->date_from}}</td>
                                            <td>{{$ins_renew->date_to}}</td>
                                            <td>{{$typeclient}}</td>
                                            <td>

                                                {{ Form::open(array('method' => 'GET', 'action' => array('AdminController@renew_insurancepage',$ins_renew->id))) }}                       
                                                {{ Form::submit('renew', array('class' => 'btn btn-primary btn-xs')) }}
                                                {{ Form::close() }}
                                            </td>
                                            <!--5 for individual clients-->


                                        </tr>
                                        @endforeach
<?php } ?>


                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>Insurance number</th>
                                        <th>Email</th>
                                        <th>Phone number</th>                                       
                                        <th>Date from</th>
                                        <th>Date to</th>
                                        <th>Insurance type</th>
                                        <th>Action</th>

                                    </tr>
                                </tfoot>
                            </table>
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->

                </div><!-- /.box -->

            </div><!-- /.tab-pane -->

        </div><!-- /.tab-content -->
    </div><!-- nav-tabs-custom -->


</section>