<!DOCTYPE html>
<html>

    <head>
        <title>Linsure insurance &reg; </title>

        <style>
            body{
                font-size: 14px;
                line-height: 20px;
            }
            .wrapper_main{
                margin: 0 auto;
                width: 100%;
            }
            .top-div{
                padding: 10px;
                border-bottom: 1px solid #eee;
            }
            .logo{
                margin-left: auto;
                margin-right: auto;
                width: 20%;
            }
            .tableft{

                display: inline;
            }

            table {
                width:100%;
                


            }

            table, th, td {
                border-top: 1px solid #ddd;
                border-collapse: collapse;
            }
            th, td {
                padding: 5px;
                text-align: left;    
            }



        </style>

    </head>

    <body>
        <div class="wrapper_main">

            <div id="top" class="top-div">
                <div class="logo ">
                    <img src="<?php echo $_SERVER["DOCUMENT_ROOT"] . '/packages/components/img/real-logo.gif'; ?>" alt="" />
                </div>
            </div>
            <div class="mainbody col-xs-12 table-responsive">
                <table class="table table-striped text-center">

                    <tbody>
                        <tr style="">
                            <th style="height: 50px;border-top-color: black;border-top-style: solid;border-top-width: 3px;border-bottom-color:black;border-bottom-style: solid;border-bottom-width: 3px;background-color: #eee; ">RETURN PREMIUM DETAILS</th>
                        </tr>


                    </tbody>
                </table>
                @foreach ($insurance as $ins)
                <div class="row col-sm-12" style="margin-bottom: 2%; margin-top: 2%">
                    <div class="tableft col-sm-3">
                        Policy Number
                    </div>
                    <div class="tableft col-sm-6" style="margin-left: 20%;">
                        {{$ins->insurance_id}}
                    </div>
                </div>
                <div class="row col-sm-12" style="margin-bottom: 2%;">
                    <div class="tableft col-sm-3">
                        MTA NUMBER
                    </div>
                    <div class="tableft col-sm-6"style="margin-left: 19%;">
                        {{$ins->insurance_id}}/6
                    </div>
                </div>
                <div class="row col-sm-12" style="margin-bottom: 2%">
                    <div class="tableft col-sm-3">
                        TRANSACTION REFERENCE
                    </div>
                    <div class="tableft col-sm-6"style="margin-left: 6%;">
                        SEC0000787943
                    </div>
                </div>
                <div class="row col-sm-12" style="margin-bottom: 2%;">
                    <div class="tableft col-sm-3">
                        PRODUCT
                    </div>
                    <div class="tableft col-sm-6"style="margin-left: 23%;">
                        Motor Private
                    </div>
                </div>
                <div class="row col-sm-12" style="margin-bottom: 2%;">
                    <div class="tableft col-sm-3">
                        INSURED
                    </div>
                    <div class="tableft col-sm-6"style="margin-left: 24%;">
                        {{$ins->first_name ," " , $ins->second_name}}

                    </div>
                </div>
                <div class="row col-sm-12" style="margin-bottom: 2%;">
                    <div class="tableft col-sm-3">
                        AGENCY
                    </div>
                    <div class="tableft col-sm-6"style="margin-left: 24%;">
                        Linsure Insurance Agency
                    </div>
                </div>
                <div class="row col-sm-12" style="margin-bottom: 2%;">
                    <div class="tableft col-sm-3">
                        RENEWAL DATE
                    </div>
                    <div class="tableft col-sm-6"style="margin-left: 17%;">
                        {{$ins->insurance_end_date}}
                    </div>
                </div>
                <div class="row col-sm-12" style="margin-bottom: 3%;">
                    <div class="col-sm-3 "style="text-decoration: underline;">
                        CHANGE DETAILS
                    </div>

                </div>
                 @foreach ($credit_note_details as $creditNote)
                <div class="row col-sm-12" style="margin-bottom: 2%;">
                    It is hereby declared and agreed that with effect from {{$creditNote->date_cancelled}} the insurance cover by this

                    policy is deemed to be cancelled.

                </div>
            </div><!-- /.col -->

            <div class="col-xs-12 table-responsive">



                <table class="table table-responsive">

                    <tr>
                        <td rowspan="2" style="border-left: 1px solid #ddd;">Premium(shs)</td>
                        <td style="border-left: 1px solid #ddd;">Return</td>
                        <td style="border-left: 1px solid #ddd;">-{{number_format($ins->premium,2)}}</td>
                        <td style="border-left: 1px solid #ddd;border-right: 1px solid #ddd">future</td>
                    </tr>
                    <tr>
                        <td style="border: none; border-left: 1px solid #ddd;"></td>
                        <td style="border: none; border-left: 1px solid #ddd;"></td>
                        <td style="border-left: 1px solid #ddd;border-right: 1px solid #ddd">{{number_format(0,2)}}</td>
                    </tr>
                    <tr>
                        <td rowspan="3" style="border-left: 1px solid #ddd;"></td>
                        <td style="border-left: 1px solid #ddd;">Training levy</td>
                        <td style="border-left: 1px solid #ddd;">-{{number_format($ins->traininglevy,2)}}</td>
                        <td style="border-left: 1px solid #ddd;border-right: 1px solid #ddd">0.00</td>
                    </tr>
                    <tr>
                        <td style="border-left: 1px solid #ddd;">php</td>
                        <td style="border-left: 1px solid #ddd;">-{{number_format($ins->phcflevy,2)}}</td> 
                        <td style="border-left: 1px solid #ddd;border-right: 1px solid #ddd">0.00</td>
                    </tr>
                    <tr>
                        <td style="border-left: 1px solid #ddd;">Total</td>
                        <td style="border-left: 1px solid #ddd;">-{{number_format($ins->total_premium,2)}}</td>
                        <td style="border-left: 1px solid #ddd;border-right: 1px solid #ddd">0.00</td>
                    </tr>
                    <tr>
                        <td style="border-left: 1px solid #ddd;">
                            MTA DATE
                        </td>
                        <td style="border-left: 1px solid #ddd;">
                            {{$ins->insurance_start_date}}
                        </td>
                        <td >

                        </td>
                        <td style="border-right: 1px solid #ddd">

                        </td>
                    </tr>
                    <tr>
                        <td style="border-left: 1px solid #ddd;">
                            AMENDED BY
                        </td>
                        <td style="border-left: 1px solid #ddd;">
                            {{$creditNote->amended_by}}
                        </td>
                        @endforeach
                        <td>

                        </td>
                        <td style="border-right: 1px solid #ddd"> 

                        </td>
                    </tr>
                    <tr>
                        <td style="border-left: 1px solid #ddd;">
                            Signed BY :
                        </td>
                        <td style="border-left: 1px solid #ddd;">
                            ____________________
                        </td>
                        <td>

                        </td>
                        <td style="border-right: 1px solid #ddd">

                        </td>
                    </tr>
                    <tr>
                        <td style="border-left: 1px solid #ddd;border-bottom: 1px solid #ddd">

                        </td>
                        <td style="border-left: 1px solid #ddd;border-bottom: 1px solid #ddd">
                            FOR THE COMPANY
                        </td>
                        <td style="border-bottom: 1px solid #ddd">

                        </td>
                        <td style="border-bottom: 1px solid #ddd;border-right: 1px solid #ddd">

                        </td>
                    </tr>
                </table>

            </div>
            @endforeach

        </div>
    </body>
</html>
