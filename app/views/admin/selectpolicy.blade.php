
<section class="content-header">
    <h1>
        Choose an insurance policy

    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ URL::to('admin/dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">select policy</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <!-- Small boxes (Stat box) -->

    <div class="row">

        {{ Form::hidden('client_id', $client_id, array('class'=>'form-control')) }}
        <div class="col-lg-3 col-xs-6 ">

            <!-- small box -->
            <div class="small-box bg-green">
                <a href="{{ URL::to('admin/vehicleInsurance/'.$client_id.'/'.$client_type) }}" class="btn btn-lg">
                    <div class="inner">
                        <h3>
                            <sup style="font-size: 20px; color: #FFFFFF">Vehicle insurance</sup>
                        </h3>
                        <p>
                            <sup style="font-size: 20px; color: #FFFFFF">cover</sup>
                        </p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-truck"></i>
                    </div>
                    <a href="#" class="small-box-footer">
                        Linsure insurance management system <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </a>
            </div>

        </div><!-- ./col -->

        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-yellow">
                <a href="{{ URL::to('admin/medicleInsurance/'.$client_id.'/'.$client_type) }}" class="btn btn-lg">
                    <div class="inner">
                        <h3>
                            <sup style="font-size: 20px; color: #FFFFFF">Medical insurance</sup>
                        </h3>
                        <p>
                            <sup style="font-size: 20px; color: #FFFFFF">cover</sup>
                        </p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-stethoscope"></i>
                    </div>
                    <a href="#" class="small-box-footer">
                        Linsure insurance management system <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </a>
            </div>
        </div><!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-red">
                <a href="{{ URL::to('admin/fireInsurance/'.$client_id.'/'.$client_type) }}" class="btn btn-lg">
                    <div class="inner">
                        <h3>
                            <sup style="font-size: 20px; color: #FFFFFF">Fire and Bulgary</sup>
                        </h3>
                        <p>
                            <sup style="font-size: 20px; color: #FFFFFF">cover</sup>
                        </p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-tint"></i>
                    </div>
                    <a href="#" class="small-box-footer">
                        Linsure insurance management system <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </a>
            </div>
        </div><!-- ./col -->
    </div><!-- /.row -->



</section><!-- /.content -->

