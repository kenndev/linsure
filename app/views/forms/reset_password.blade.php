<!DOCTYPE html>
<html class="bg-black">
    <head>
        <meta charset="UTF-8">
        <title>Linsure | Password Reset</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- bootstrap 3.0.2 -->
        {{ HTML::style('packages/components/css/bootstrap.min.css', array('media' => 'screen')) }}


        <!-- font Awesome -->
        {{ HTML::style('packages/components/css/font-awesome.min.css', array('media' => 'screen')) }}

        <!-- Theme style -->
        {{ HTML::style('packages/components/css/AdminLTE.css', array('media' => 'screen')) }}



        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="bg-black">

        <div class="form-box" id="login-box">
            <div class="header">Sign In</div>

            {{ Form::open(array('url'=>'users/reset_password')) }}

            <div class="body bg-gray">
                @if(Session::has('error'))
                <div class="alert alert-danger">{{Session::get('error')}}</div>  

                @endif
                <div class="form-group">

                    {{ Form::password('password',array('placeholder'=>'Password', 'class'=>'form-control')) }}
                </div>
                <div class="form-group">

                    {{ Form::password('password_confirmation', array('placeholder'=>'Confirm Password','class'=>'form-control')) }}
                </div>          

            </div>
            <div class="footer"> 
                <button type="submit" class="btn bg-olive btn-block">
                    Continue
                </button>
            </div>
            {{ Form::close() }}

        </div>


        <!-- jQuery 2.0.2 -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
        <!-- Bootstrap -->

        <script src="{{ asset('packages/components/js/bootstrap.min.js') }}"></script>

    </body>
</html>