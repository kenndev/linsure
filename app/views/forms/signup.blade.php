<!DOCTYPE html>
<html lang="en-us" id="extr-page">
    <head>
        <meta charset="utf-8">

        <title> Linsure Insurance&reg; | Sign Up </title>		
        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

        <!-- #CSS Links -->
        <!-- Basic Styles -->


        {{ HTML::style('packages/components/bk/css/bootstrap.min.css', array('media' => 'screen')) }}

        {{ HTML::style('packages/components/bk/css/font-awesome.min.css', array('media' => 'screen')) }}

        <!-- SmartAdmin Styles : Please note (smartadmin-production.css) was created using LESS variables -->

        {{ HTML::style('packages/components/bk/css/smartadmin-production.min.css', array('media' => 'screen')) }}

        {{ HTML::style('packages/components/bk/css/smartadmin-skins.min.css', array('media' => 'screen')) }}

        <!-- SmartAdmin RTL Support is under construction
                 This RTL CSS will be released in version 1.5
        <link rel="stylesheet" type="text/css" media="screen" href="css/smartadmin-rtl.min.css"> -->

        <!-- We recommend you use "your_style.css" to override SmartAdmin
             specific styles this will also ensure you retrain your customization with each SmartAdmin update.
        <link rel="stylesheet" type="text/css" media="screen" href="css/your_style.css"> -->

        <!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->

        {{ HTML::style('packages/components/bk/css/demo.min.css', array('media' => 'screen')) }}

        <!-- #FAVICONS -->		

        <link rel="shortcut icon" href="{{ asset('packages/components/bk/img/favicon/favicon.ico') }}" type="image/x-icon">
        <link rel="icon" href="{{ asset('packages/components/bk/img/favicon/favicon.ico') }}" type="image/x-icon">

        <!-- #GOOGLE FONT -->

        {{ HTML::style('http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700') }}

        <!-- #APP SCREEN / ICONS -->
        <!-- Specifying a Webpage Icon for Web Clip 
                 Ref: https://developer.apple.com/library/ios/documentation/AppleApplications/Reference/SafariWebContent/ConfiguringWebApplications/ConfiguringWebApplications.html -->
        <link rel="apple-touch-icon" href="{{ asset('packages/components/bk/img/splash/sptouch-icon-iphone.png') }}">
        <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('packages/components/bk/img/splash/touch-icon-ipad.png') }}">
        <link rel="apple-touch-icon" sizes="120x120" href="{{ asset('packages/components/bk/img/splash/touch-icon-iphone-retina.png') }}">
        <link rel="apple-touch-icon" sizes="152x152" href="{{ asset('packages/components/bk/img/splash/touch-icon-ipad-retina.png') }}">

        <!-- iOS web-app metas : hides Safari UI Components and Changes Status Bar Appearance -->
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">

        <!-- Startup image for web apps -->
        <link rel="apple-touch-startup-image" href="{{ asset('packages/components/bk/img/splash/ipad-landscape.png') }}" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:landscape)">
        <link rel="apple-touch-startup-image" href="{{ asset('packages/components/bk/img/splash/ipad-portrait.png') }}" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:portrait)">
        <link rel="apple-touch-startup-image" href="{{ asset('packages/components/bk/img/splash/iphone.png') }}" media="screen and (max-device-width: 320px)">

    </head>

    <body class="animated fadeInDown">

        <header id="header">

            <div id="logo-group">
                <span id="logo"> <img src="{{ asset('packages/components/bk/img/logo.png') }}" alt="Revenue Collection"> </span>
            </div>

            <span id="extr-page-header-space"> <span class="hidden-mobile">Already having an account?</span> <a href="{{ URL::to('users/login') }}" class="btn btn-danger">Sign in</a> </span>

        </header>

        <div id="main" role="main">

            <!-- MAIN CONTENT -->
            <div id="content" class="container">

                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-7 col-lg-7 hidden-xs hidden-sm">
                        <h1 class="txt-color-red login-header-big">Revenue Collection</h1>
                        <div class="hero">

                            <div class="pull-left login-desc-box-l">
                                <h4 class="paragraph-header">It's Okay to be Smart. Experience the simplicity of Revenue Collection, everywhere you go!</h4>
                                <div class="login-app-icons">
                                    <a href="javascript:void(0);" class="btn btn-danger btn-sm">Link here </a>
                                    <a href="javascript:void(0);" class="btn btn-danger btn-sm">Another link here</a>
                                </div>
                            </div>

                            <img src="{{ asset('packages/components/bk/img/demo/iphoneview.png') }}" class="pull-right display-image" alt="" style="width:210px">

                        </div>

                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                <h5 class="about-heading">About Revenue Collection - Are you up to date?</h5>
                                <p>
                                    Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa.
                                </p>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                                <h5 class="about-heading">Not just your average template!</h5>
                                <p>
                                    Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi voluptatem accusantium!
                                </p>
                            </div>
                        </div>

                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
                        <div class="well no-padding">
                            <!--REGISTER FORM-->	


                            <!-- <form action="index.html" id="login-form" class="smart-form client-form"> -->
                            {{ Form::open(array('url'=>'users', 'class'=>'smart-form client-form', 'id'=>'smart-form-register','novalidate'=>'novalidate')) }}
                            <header>

                                @if(Session::has('error'))  
                                <div class="alert alert-danger">                                                   
                                    @foreach (Session::get('error') as  $value)                                                     
                                    {{ $value }}<br>
                                    @endforeach
                                </div> 
                                @else
                                <div> Sign In &nbsp;</div>
                                @endif

                            </header>

                            <fieldset>                                                   

                                <section>                                                        
                                    <label for="username" class="input"> <i class="icon-append fa fa-user"></i>                                                                
                                        {{ Form::text('username', null, array('placeholder'=>'Username','id'=>'username')) }}
                                        <b class="tooltip tooltip-top-right"><i class="fa fa-user txt-color-teal"></i> Please enter username</b></label>
                                </section>

                                <section>                                                        
                                    <label class="input"> <i class="icon-append fa fa-envelope"></i>                                                                
                                        {{ Form::text('email', null, array('placeholder'=>'Email Address')) }}
                                        <b class="tooltip tooltip-top-right"><i class="fa fa-envelope txt-color-teal"></i> Please enter email address</b></label>
                                </section>

                                <section>                                                        
                                    <label class="input"> <i class="icon-append fa fa-lock"></i>											
                                        {{ Form::password('password',array('placeholder'=>'Password','id'=>'password')) }}
                                        <b class="tooltip tooltip-top-right"><i class="fa fa-lock txt-color-teal"></i> Enter your password</b> </label>

                                </section> 
                                <section>                                                        
                                    <label class="input"> <i class="icon-append fa fa-lock"></i>											
                                        {{ Form::password('password_confirmation',array('placeholder'=>'Confirm Password')) }}
                                        <b class="tooltip tooltip-top-right"><i class="fa fa-lock txt-color-teal"></i>  Conf password</b> </label>

                                </section> 
                                <section>                                                  
                                    <div class="inline-group">
                                        <label class="radio">
                                            <input type="radio"   name="r1" id="e1" value="5" onchange="show2()">
                                            <i></i>Business</label>
                                        <label class="radio">
                                            <input type="radio" checked="checked" value="10"  name="r1" onchange="show(this.value)">
                                            <i></i>Individual</label>
                                    </div> 
                                </section>

                            </fieldset>
                            <fieldset>
                                <div id="sh1">
                                    <div class="row">
                                        <section class="col col-6">                                                        
                                            <label class="input"> <i class="icon-append fa fa-user"></i>											
                                                {{ Form::text('firstname',null,array('placeholder'=>'First Name',)) }}
                                                <b class="tooltip tooltip-top-right"><i class="fa fa-user txt-color-teal"></i>First Name</b> 
                                            </label>                                                       
                                        </section> 
                                        <section class="col col-6">                                                        
                                            <label class="input"> <i class="icon-append fa fa-user"></i>											
                                                {{ Form::text('lastname',null,array('placeholder'=>'Last Name',)) }}
                                                <b class="tooltip tooltip-top-right"><i class="fa fa-user txt-color-teal"></i>Last Name</b> 
                                            </label>                                                       
                                        </section>  
                                    </div>
                                </div>
                                <div id="sh2" style="display:none;">
                                    <div class="row">
                                        <section class="col col-6">                                                        
                                            <label class="input"> <i class="icon-append fa fa-user"></i>											
                                                {{ Form::text('businessname',null,array('placeholder'=>'Business Name',)) }}
                                                <b class="tooltip tooltip-top-right"><i class="fa fa-user txt-color-teal"></i>Business Name</b> 
                                            </label>                                                       
                                        </section> 
                                        <section class="col col-6">                                                        
                                            <label class="input"> <i class="icon-append fa fa-openid"></i>											
                                                {{ Form::text('kra_pin',null,array('placeholder'=>'KRA PIN',)) }}
                                                <b class="tooltip tooltip-top-right"><i class="fa fa-openid txt-color-teal"></i>kra pin</b> 
                                            </label>                                                       
                                        </section>

                                    </div>
                                </div>
                                <div class="row">
                                    <div id="sh3">
                                        <section class="col col-6">                                                        
                                            <label class="input"> <i class="icon-append fa fa-openid"></i>											
                                                {{ Form::text('national_id',null,array('placeholder'=>'National ID',)) }}
                                                <b class="tooltip tooltip-top-right"><i class="fa fa-openid txt-color-teal"></i>First Name</b> 
                                            </label>                                                       
                                        </section>
                                    </div>                                                     
                                    <section class="col col-6">                                                        
                                        <label class="input"> <i class="icon-append fa fa-mobile"></i>											
                                            {{ Form::text('phone_number',null,array('placeholder'=>'Phone Number',)) }}
                                            <b class="tooltip tooltip-top-right"><i class="fa fa-mobile txt-color-teal"></i>Last Name</b> 
                                        </label>                                                       
                                    </section>  
                                </div>
                            </fieldset>
                            <footer>
                                <button type="submit" class="btn btn-primary">
                                    Sign in
                                </button>
                            </footer>
                            <!-- </form> -->
                            {{ Form::close() }}

                            <!--END OF REGITER FORM-->	


                        </div>

                        <h5 class="text-center"> - Or sign in using -</h5>

                        <ul class="list-inline text-center">
                            <li>
                                <a href="javascript:void(0);" class="btn btn-primary btn-circle"><i class="fa fa-facebook"></i></a>
                            </li>
                            <li>
                                <a href="javascript:void(0);" class="btn btn-info btn-circle"><i class="fa fa-twitter"></i></a>
                            </li>
                            <li>
                                <a href="javascript:void(0);" class="btn btn-warning btn-circle"><i class="fa fa-linkedin"></i></a>
                            </li>
                        </ul>

                    </div>
                </div>
            </div>

        </div>

        <!--================================================== -->	

        <!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->
        <script src="{{ asset('packages/components/bk/js/plugin/pace/pace.min.js') }}"></script>

        <!-- Link to Google CDN's jQuery + jQueryUI; fall back to local -->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
        <script> if (!window.jQuery) { document.write('<script src="{{ asset('packages / components / bk / js / libs / jquery - 2.0.2.min.js') }}"><\/script>'); }</script>

        <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
        <script> if (!window.jQuery.ui) { document.write('<script src="{{ asset('packages / components / bk / js / libs / jquery - ui - 1.10.3.min.js') }}"><\/script>'); }</script>

        {{ HTML::script('packages/components/bk/js/app.config.js') }}

        <!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->

        {{ HTML::script('packages/components/bk/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js') }}


        <!-- BOOTSTRAP JS -->

        {{ HTML::script('packages/components/bk/js/bootstrap/bootstrap.min.js') }}

        <!-- CUSTOM NOTIFICATION -->

        {{ HTML::script('packages/components/bk/js/notification/SmartNotification.min.js') }}

        <!-- JARVIS WIDGETS -->

        {{ HTML::script('packages/components/bk/js/smartwidgets/jarvis.widget.min.js') }}


        <!-- EASY PIE CHARTS -->

        {{ HTML::script('packages/components/bk/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js') }}


        <!-- SPARKLINES -->

        {{ HTML::script('packages/components/bk/js/plugin/sparkline/jquery.sparkline.min.js') }}

        <!-- JQUERY VALIDATE -->

        {{ HTML::script('packages/components/bk/js/plugin/jquery-validate/jquery.validate.min.js') }}

        <!-- JQUERY MASKED INPUT -->

        {{ HTML::script('packages/components/bk/js/plugin/masked-input/jquery.maskedinput.min.js') }}

        <!-- JQUERY SELECT2 INPUT -->

        {{ HTML::script('packages/components/bk/js/plugin/select2/select2.min.js') }}

        <!-- JQUERY UI + Bootstrap Slider -->

        {{ HTML::script('packages/components/bk/js/plugin/bootstrap-slider/bootstrap-slider.min.js') }}


        <!-- browser msie issue fix -->

        {{ HTML::script('packages/components/bk/js/plugin/msie-fix/jquery.mb.browser.min.js') }}


        <!-- FastClick: For mobile devices -->

        {{ HTML::script('packages/components/bk/js/plugin/fastclick/fastclick.min.js') }}

        <!--[if IE 8]>

        <h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>

        <![endif]-->

        <!-- Demo purpose only -->



        <!-- MAIN APP JS FILE -->

        {{ HTML::script('packages/components/bk/js/app.min.js') }}

        <!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
        <!-- Voice command : plugin -->



        <!-- PAGE RELATED PLUGIN(S) -->
        {{ HTML::script('packages/components/bk/js/plugin/jquery-form/jquery-form.min.js') }}		


        <script type="text/javascript">
            function show(str){
            document.getElementById('sh2').style.display = 'none';
                    document.getElementById('sh1').style.display = 'block';
                    document.getElementById('sh3').style.display = 'block';
            }
            function show2(sign){
            document.getElementById('sh2').style.display = 'block';
                    document.getElementById('sh1').style.display = 'none';
                    document.getElementById('sh3').style.display = 'none';
            }
        </script>

        <script type="text/javascript">

            // DO NOT REMOVE : GLOBAL FUNCTIONS!

            $(document).ready(function() {

            pageSetUp();
                    var $registerForm = $("#smart-form-register").validate({

            // Rules for form validation
            rules : {
            username : {
            required : true
            },
                    email : {
                    required : true,
                            email : true
                    },
                    password : {
                    required : true,
                            minlength : 6,
                            maxlength : 20
                    },
                    password_confirmation : {
                    required : true,
                            minlength : 6,
                            maxlength : 20,
                            equalTo : '#password'
                    },
                    firstname : {
                    required : true
                    },
                    lastname : {
                    required : true
                    },
                    businessname : {
                    required : true
                    },
                    national_id : {
                    required : true,
                            minlength : 8,
                            maxlength : 8
                    },
                    phone_number : {
                    required : true
                    },
                    kra_pin : {
                    required : true,
                            minlength : 11,
                            maxlength : 11,
                    }
            },
                    // Messages for form validation
                    messages : {
                    login : {
                    required : 'Please enter your login'
                    },
                            email : {
                            required : 'Please enter your email address',
                                    email : 'Please enter a VALID email address'
                            },
                            password : {
                            required : 'Please enter your password'
                            },
                            password_confirmation : {
                            required : 'Please enter your password one more time',
                                    equalTo : 'Please enter the same password as above'
                            },
                            firstname : {
                            required : 'Please enter your first name'
                            },
                            lastname : {
                            required : 'Please enter your last name'
                            },
                            businessname : {
                            required : 'Please enter your business name'
                            },
                            national_id : {
                            required : 'Please enter your National ID Number'
                            },
                            phone_number : {
                            required : 'Please enter your Phone Number'
                            },
                            kra_pin : {
                            required : 'Please enter your KRA PIN'
                            }
                    },
                    // Do not change code below
                    errorPlacement : function(error, element) {
                    error.insertAfter(element.parent());
                    }
           });
                    var $loginForm = $("#login-form").validate({
            // Rules for form validation
            rules : {
            email : {
            required : true,
                    email : true
            },
                    password : {
                    required : true,
                            minlength : 3,
                            maxlength : 20
                    }
            },
                    // Messages for form validation
                    messages : {
                    email : {
                    required : 'Please enter your email address',
                            email : 'Please enter a VALID email address'
                    },
                            password : {
                            required : 'Please enter your password'
                            }
                    },
                    // Do not change code below
                    errorPlacement : function(error, element) {
                    error.insertAfter(element.parent());
                    }
            });
            })

        </script>




    </body>
</html>